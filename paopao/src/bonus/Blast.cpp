#include <map>
#include <algorithm>
#include "common/Random.h"
#include "bonus/Blast.h"
#include "gameplay/Level.h"
#include "gameplay/Element.h"
#include "gameplay/Point.h"
#include "notify/notify.h"

namespace bonus {

void Blast::apply(gameplay::Level& level, size_t x_, size_t y_) {
	notify::tool::bomb(static_cast<int>(x_), static_cast<int>(y_));
	std::map<gameplay::Element, std::vector<gameplay::Point>> center;
	for (int ix = -1; ix <= 1; ix++) {
		for (int iy = -1; iy <= 1; iy++) {
			gameplay::Element element{ level.get(x_ + ix, y_ + iy) };
			if (level.get(x_ + ix, y_ + iy).canAction()) {
				center[element].push_back(gameplay::Point{ x_ + ix, y_ + iy });
			}
		}
	}
	std::map<gameplay::Element, std::vector<gameplay::Point>> all;
	for (size_t x = 0; x < level.width(); x++) {
		for (size_t y = 0; y < level.height(); y++) {
			gameplay::Element element{ level.get(x, y) };
			if (element.canAction()) {
				all[element].push_back(gameplay::Point{ x, y });
			}
		}
	}
	for (auto& block : center) {
		const auto& goals{all[block.first]};
		while (block.second.size() % 2 != 0) {
			size_t id = common::Random::get() % goals.size();
			if (std::find(block.second.begin(), block.second.end(), goals[id]) == block.second.end()) {
				block.second.push_back(goals[id]);
			}
		}
	}
	for (auto& block : center) {
		for (auto& element : block.second) {
			level.blast_element(element.x, element.y);
		}
	}
}

}
