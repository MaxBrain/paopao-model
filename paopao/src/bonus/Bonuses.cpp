#include "bonus/Bonuses.h"
#include "bonus/Blast.h"
#include "common/Strings.h"
#include "common/Rules.h"
#include "gameplay/Level.h"
#include "gameplay/Step.h"
#include "ui/UIType.h"
#include "notify/notify.h"
#include "3rdParty/json.hpp"

namespace bonus {

namespace {
std::string kBonuses{ "bonuses" };
}


Bonuses::Bonuses()
	: bomb(common::Rules::bonuses_base::bomb)
	, hint(common::Rules::bonuses_base::hint)
	, mix(common::Rules::bonuses_base::mix)
{}

void Bonuses::load(const nlohmann::json& data) {
	if (!data.contains(kBonuses)) return;
	if (!data[kBonuses].is_object()) return;
	if ((data[kBonuses].contains(kBomb)) && (data[kBonuses][kBomb].is_number())) bomb = data[kBonuses][kBomb];
	if ((data[kBonuses].contains(kHint)) && (data[kBonuses][kHint].is_number())) hint = data[kBonuses][kHint];
	if ((data[kBonuses].contains(kMix)) && (data[kBonuses][kMix].is_number())) mix = data[kBonuses][kMix];
}

void Bonuses::save(nlohmann::json& data) {
	data[kBonuses][kBomb] = bomb;
	data[kBonuses][kHint] = hint;
	data[kBonuses][kMix] = mix;
}

void Bonuses::applyBomb(gameplay::Level& level, size_t x, size_t y) {
	if (bomb <= 0) return;
	bomb--;
	Blast::apply(level, x, y);
	nlohmann::json json;
	json[kBomb] = bomb;
	json["active_bomb"] = false;
	notify::gui::update::game_ui(json);
}

void Bonuses::applyHint(gameplay::Level& level) {
	if (hint <= 0) return;
	hint--;
	const gameplay::Step step = level.help();
	notify::element::hint(static_cast<int>(level.get(step.x1, step.y1).id));
	notify::element::hint(static_cast<int>(level.get(step.x2, step.y2).id));
}

void Bonuses::applyMix(gameplay::Level& level) {
	if (mix <= 0) return;
	mix--;
	level.mix();
}

}
