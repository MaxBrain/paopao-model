#include "value/Type.h"

namespace value {

namespace {
	const char* kBlast{ "Blast" };
	const char* kHint{ "Hint" };
	const char* kMix{ "Mix" };
}

const char* to_string(Type type) {
	switch (type) {
	case Type::Blast: return kBlast;
	case Type::Hint: return kHint;
	case Type::Mix: return kMix;
	}
	return 0;
}

}
