#include "levels/PageKey.h"

namespace levels {

bool operator < (const PageKey& rhs, const PageKey& lhs) {
	if (rhs.levelType == lhs.levelType) {
		return rhs.page < lhs.page;
	}
	else {
		return rhs.levelType < lhs.levelType;
	}
}

bool operator == (const PageKey& rhs, const PageKey& lhs) {
	return (rhs.page == lhs.page)
		&& (rhs.levelType == lhs.levelType)
	;
}
}
