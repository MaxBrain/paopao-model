#include "levels/Element.h"

namespace levels {

bool operator==(const levels::Element& e1, const levels::Element& e2) {
	return
		(e1.color == e2.color) &&
		(e1.image == e2.image) &&
		(e1.present == e2.present);
}

}
