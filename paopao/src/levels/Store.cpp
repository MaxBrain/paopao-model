#include <string>
#include "levels/Store.h"
#include "levels/Data.h"
#include "common/Rules.h"
#include "common/Random.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "game/State.h"
#include "generator/Generator.h"
#include "levels/PageKey.h"
#include "component/Components.h"
#include "generator/Generator.h"

namespace levels {

namespace {
	Page getPage(levels::LevelType levelType, size_t page) {
		return {
			"00112233440011223344",/*color*/
			component::generator->getRules(PageKey{levelType, page}),/*rules*/
			"00000000010000000001",/*hard*/
		};
	}

	Pages getRegularPages(size_t page) {
		Pages pages;
		pages.background = 0;
		if (page > 0) pages.left = getPage(levels::LevelType::Regular, page - 1);
		pages.active = getPage(levels::LevelType::Regular, page);
		pages.right = getPage(levels::LevelType::Regular, page + 1);
		return pages;
	}

	Pages getAdventurePages(size_t page) {
		Pages pages;
		pages.background = 1;
		if (page == 0) {
			pages.active = getPage(levels::LevelType::Adventure, page);
			pages.right = getPage(levels::LevelType::Adventure, page + 1);
		}
		else if (page < common::Rules::PagesInAdventure - 1) {
			pages.left = getPage(levels::LevelType::Adventure, page - 1);
			pages.active = getPage(levels::LevelType::Adventure, page);
			pages.right = getPage(levels::LevelType::Adventure, page + 1);
		}
		else if (page == common::Rules::PagesInAdventure - 1) {
			pages.left = getPage(levels::LevelType::Adventure, page - 1);
			pages.active = getPage(levels::LevelType::Adventure, page);
		}
		else if (page == common::Rules::PagesInAdventure) {
			pages.left = getPage(levels::LevelType::Adventure, page - 1);
		}
		return pages;
	}
}

Level Store::getLevel(LevelType levelType, size_t number) const {
	if ((levelType == LevelType::Regular) && (number == 1)) {
		auto level = Level(Data::baseLevel);
		level.time = 180;
		return level;
	}
	common::Random::init(static_cast<unsigned>(levelType) + 1, static_cast<unsigned>(number));
	component::generator->generate(6);
	Level level{ component::generator->result };
	level.rules = component::generator->getRulesForLevel(levelType, number);
	return level;
}

Pages Store::getPages(LevelType levelType, size_t page) const {
	switch (levelType) {
	case LevelType::Regular:
		return getRegularPages(page);
	case LevelType::Adventure:
		return getAdventurePages(page);
	}
	LOG_ERROR("Wrong page type");
	return Pages{};
}

Level Store::getCurrentLevel() const {
	return getLevel(component::state->levelType, component::state->currentLevel);
}

}
