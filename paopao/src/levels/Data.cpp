#include "levels/Data.h"

namespace levels {

const std::vector<std::string> Data::baseLevel = {
	"      55",
	"        ",
	"        ",
	"   12   ",
	"        ",
	"   13   ",
	"   24   ",
	"        ",
	"   43   ",
	"        ",
	"        ",
	"66      ",
};

}
