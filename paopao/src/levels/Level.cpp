#include <cctype>
#include <vector>
#include "levels/Level.h"
#include "mediator/Logger.h"
#include "common/Rules.h"

namespace levels {

const Element Level::emptyElement;

Level::Level() {
	area.resize(width());
	for (size_t x = 0; x < width(); x++) {
		area.at(x).resize(height());
	}
}

Level::Level(const std::vector<std::string>& data) {
	area.resize(width());
	for (size_t x = 0; x < width(); x++) {
		area.at(x).resize(height());
		for (size_t y = 0; y < height(); y++) {
			const char symbol{data[height() - y - 1][x]};
			area.at(x).at(y).present = symbol != ' ';
			if (std::isdigit(symbol)) {
				area.at(x).at(y).image = symbol - '0';
			}
		}
	}
}

Level::Level(const generator::Block& block) {
	area.resize(width());
	for (size_t x = 0; x < width(); x++) {
		area.at(x).resize(height());
		for (size_t y = 0; y < height(); y++) {
			const auto& cell{ block.getArea()[x][y + 2] };
			if (cell == 0) {
				area.at(x).at(y).present = false;
			}
			else {
				area.at(x).at(y).present = true;
				area.at(x).at(y).image = cell;
			}
		}
	}
}

const Element& Level::get(size_t x, size_t y) const {
	if (x >= width()) return Level::emptyElement;
	if (y >= height()) return Level::emptyElement;
	if (x >= area.size()) {
		LOG_ERROR("Exeception while get element from levels::Level");
		return Level::emptyElement;
	}
	if (y >= area[x].size()) {
		LOG_ERROR("Exeception while get element from levels::Level");
		return Level::emptyElement;
	}
	if (!area[x][y].present) return Level::emptyElement;
	return area[x][y];
}

void Level::set(size_t x, size_t y, const Element& element) {
	if (x >= width()) {
		LOG_ERROR("x out of area levels::Levels");
		return;
	}
	if (y >= height()) {
		LOG_ERROR("y out of area levels::Levels");
		return;
	}
	if (x >= area.size()) {
		LOG_ERROR("Error while set element to levels::Level");
		return;
	}
	if (y >= area[x].size()) {
		LOG_ERROR("Error while set element to levels::Level");
		return;
	}
	area[x][y] = element;
}

size_t Level::width() const {
	return common::Rules::Width;
}

size_t Level::height() const {
	return common::Rules::Height;
}

bool operator==(const Level& lhs, const Level& rhs) {
	for (size_t x = 0; x < lhs.width(); x++) {
		for (size_t y = 0; y < lhs.width(); y++) {
			if (!(lhs.get(x, y) == rhs.get(x, y))) return false;
		}
	}
	if (lhs.time != rhs.time) return false;
	if (lhs.rules != rhs.rules) return false;
	if (lhs.hard != rhs.hard) return false;
	return true;
}

}
