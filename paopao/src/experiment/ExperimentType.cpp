#include "experiment/ExperimentType.h"

namespace experiment {

std::string GetDescription(ExperimentType experimentType) {
    switch (experimentType) {
        case ExperimentType::TasksSize: return "The size of tasks. Short, middle or long.";
        case ExperimentType::TasksFrequency: return "Tasks frequency 24h, 12h, 6h, 3h";
        case ExperimentType::InterscenialADSTimer: return "Time between interscenial ADS. 5m/10m";
        case ExperimentType::Learn: return "Only rules. Only learn. Learn + rules.";
        case ExperimentType::Level1: return "Simple or middle first level.";
        case ExperimentType::Density: return "Density for levels.";
        case ExperimentType::DensityDelta: return "Density delta for levels.";
		case ExperimentType::AddTimeAfterStep: return "Add extra time after step. In milliseconds.";
    }
	return "";
}

}
