#include <map>
#include "experiment/Experiment.h"

//Цели экспериментов:
// - Время игры в сутки
// - Количество запусков в сутки
// - Количество ADS
// - Retention

// - Выполняющие все квесты
// - Частота запуска по дням

namespace experiment {

namespace {

std::map<ExperimentType, int> experimentValues {
	{experiment::ExperimentType::TasksSize, TasksSizeShort},
	{experiment::ExperimentType::TasksFrequency, TasksFrequency24h},
	{experiment::ExperimentType::InterscenialADSTimer, InterscenialADSTimer5m},
	{experiment::ExperimentType::Learn, LearnRulesShow},
	{experiment::ExperimentType::Level1, Level1Simple},
	{experiment::ExperimentType::Density, DensityMini},
	{experiment::ExperimentType::DensityDelta, DensityDeltaMini},
	{experiment::ExperimentType::AddTimeAfterStep, AddTimeAfterStepNone},
};

}

void Experiment::Set(ExperimentType experimentType, int value) {
	switch (experimentType)
	{
	case ExperimentType::TasksSize:
		if (value < TasksSizeMin) return;
		if (value > TasksSizeMax) return;
		break;
	case ExperimentType::TasksFrequency:
		if (value < TasksFrequencyMin) return;
		if (value > TasksFrequencyMax) return;
		break;
	case ExperimentType::InterscenialADSTimer:
		if (value < InterscenialADSTimerMin) return;
		if (value > InterscenialADSTimerMax) return;
		break;
	case ExperimentType::Learn:
		if (value < LearnMin) return;
		if (value > LearnMax) return;
		break;
	case ExperimentType::Level1:
		if (value < Level1Min) return;
		if (value > Level1Max) return;
		break;
	case ExperimentType::Density:
		if (value < DensityMin) return;
		if (value > DensityMax) return;
		break;
	case ExperimentType::DensityDelta:
		if (value < DensityDeltaMin) return;
		if (value > DensityDeltaMax) return;
		break;
	case ExperimentType::AddTimeAfterStep:
		if (value < AddTimeAfterStepMin) return;
		if (value > AddTimeAfterStepMax) return;
		break;
	}
    experimentValues[experimentType] = value;
}

int Experiment::Get(ExperimentType experimentType) {
    return experimentValues[experimentType];
}

}
