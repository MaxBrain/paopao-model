#include "config/Rewarded.h"
#include "mediator/Logger.h"

namespace config {

Rewarded parseRewarded(const std::string& string) {
    if (string == "None") return Rewarded::None;
    if (string == "AdMob") return Rewarded::AdMob;
    if (string == "Yandex") return Rewarded::Yandex;
    LOG_ERROR("Can not parse config.rewarded [%s]", string.c_str());
    return Rewarded::None;
}

}
