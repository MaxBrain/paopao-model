#include "config/Achievements.h"
#include "mediator/Logger.h"

namespace config {

Achievements parseAchievements(const std::string& string) {
	if (string == "None") return Achievements::None;
	if (string == "GP") return Achievements::GP;
	if (string == "IOS") return Achievements::IOS;
	LOG_ERROR("Can not parse config.achievements [%s]", string.c_str());
	return Achievements::None;
}

}
