#include "config/Config.h"
#include "3rdParty/json.hpp"
#include "mediator/Logger.h"

namespace config {

void Config::load(const std::string& config) {
	nlohmann::json json{ nlohmann::json::parse(config, nullptr, false) };
	if (json.is_discarded()) {
		LOG_ERROR("Error while parse saved data: [%s]", config.c_str());
		return;
	}
	if (!json.is_object()) {
		LOG_ERROR("Error while parse saved data - not object: [%s]", config.c_str());
		return;
	}
    if (json["achievements"].is_string()) {
        achievements = parseAchievements(json["achievements"].get<std::string>());
    }
    if (json["ads"].is_string()) {
        ads = parseAds(json["ads"].get<std::string>());
    }
    if (json["rewarded"].is_string()) {
        rewarded = parseRewarded(json["rewarded"].get<std::string>());
    }
    if (json["shop"].is_string()) {
        shop = parseShop(json["shop"].get<std::string>());
    }
    if (json["statictics"].is_string()) {
        statictics = parseStatictics(json["statictics"].get<std::string>());
    }
}


}
