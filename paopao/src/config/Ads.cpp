#include "config/Ads.h"
#include "mediator/Logger.h"

namespace config {

Ads parseAds(const std::string& string) {
    if (string == "None") return Ads::None;
    if (string == "AdMob") return Ads::AdMob;
    if (string == "Yandex") return Ads::Yandex;
    LOG_ERROR("Can not parse config.ads [%s]", string.c_str());
    return Ads::None;
}

}
