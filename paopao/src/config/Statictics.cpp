#include "config/Statictics.h"
#include "mediator/Logger.h"
#include "mediator/Logger.h"

namespace config {

Statictics parseStatictics(const std::string& string) {
    if (string == "None") return Statictics::None;
    if (string == "Google") return Statictics::Google;
    if (string == "Yandex") return Statictics::Yandex;
    LOG_ERROR("Can not parse config.statictics [%s]", string.c_str());
    return Statictics::None;
}

}
