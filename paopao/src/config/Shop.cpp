#include "config/Shop.h"
#include "mediator/Logger.h"

namespace config {

Shop parseShop(const std::string& string) {
    if (string == "None") return Shop::None;
    if (string == "Base") return Shop::Base;
    if (string == "Yandex") return Shop::Yandex;
    LOG_ERROR("Can not parse config.shop [%s]", string.c_str());
    return Shop::None;
}

}
