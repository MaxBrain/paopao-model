#include "ui/NewElement.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void NewElement::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::NewElement");
        return;
    }
    notify::gui::popup::new_element();
    is_showed = true;
}

void NewElement::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::NewElement");
        return;
    }
    notify::gui::close::new_element();
    is_showed = false;
}

}
