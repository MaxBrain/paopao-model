#include "ui/TermOfConditions.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void TermOfConditions::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::TermOfConditions");
        return;
    }
    notify::gui::popup::term_of_conditions();
    is_showed = true;
}

void TermOfConditions::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::TermOfConditions");
        return;
    }
    notify::gui::close::term_of_conditions();
    is_showed = false;
}

}
