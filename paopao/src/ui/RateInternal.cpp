#include "ui/RateInternal.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void RateInternal::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::RateInternal");
        return;
    }
    notify::gui::popup::rate_internal();
    is_showed = true;
}

void RateInternal::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::RateInternal");
        return;
    }
    notify::gui::close::rate_internal();
    is_showed = false;
}

}
