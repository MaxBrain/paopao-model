#include "ui/TheRules.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"
#include "gameplay/Controller.h"

namespace ui {

void TheRules::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::TermOfConditions");
        return;
    }
    nlohmann::json json;
    json["title"] = component::l10n->get("The Rules");
    json["OK"] = component::l10n->get("It's clear");
    notify::gui::popup::the_rules(json);
    component::controller->pause();
    is_showed = true;
}

void TheRules::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::TermOfConditions");
        return;
    }
    notify::gui::close::the_rules();
    component::controller->unpause();
    is_showed = false;
}

}
