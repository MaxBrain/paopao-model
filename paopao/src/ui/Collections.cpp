#include "ui/Collections.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void Collections::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::Collections");
        return;
    }
    notify::gui::popup::collections();
    is_showed = true;
}

void Collections::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::Collections");
        return;
    }
    notify::gui::close::collections();
    is_showed = false;
}

}
