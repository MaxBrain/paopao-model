#include "ui/Shop.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"
#include "shop/Shop.h"

namespace ui {

void Shop::show() {
	if (is_showed) {
		LOG_ERROR("Try to show already showed ui::Shop");
		return;
	}
    if (!component::shop->isActive()) {
        LOG_ERROR("Try to show window then Shop is not active");
        return;
    }
    nlohmann::json json;
    json["goods"][0]["buyed"] = component::shop->noAdsBought();
    json["goods"][1]["buyed"] = false;
    json["goods"][2]["buyed"] = false;
    json["goods"][3]["buyed"] = false;
    json["goods"][1]["count"] = shop::HINT_PACK_COUNT;
    json["goods"][2]["count"] = shop::BOMB_PACK_COUNT;
    json["goods"][3]["count"] = shop::MIX_PACK_COUNT;
    json["goods"][0]["type"] = "no_ads";
    json["goods"][1]["type"] = "hint";
    json["goods"][2]["type"] = "bomb";
    json["goods"][3]["type"] = "mix";
    json["goods"][0]["cost"] = component::shop->getCost(shop::ItemType::NO_ADS);
    json["goods"][1]["cost"] = component::shop->getCost(shop::ItemType::HINT_PACK);
    json["goods"][2]["cost"] = component::shop->getCost(shop::ItemType::BOMB_PACK);
    json["goods"][3]["cost"] = component::shop->getCost(shop::ItemType::MIX_PACK);
    notify::gui::popup::shop(json);
    is_showed = true;
}

void Shop::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::Shop");
        return;
    }
    notify::gui::close::shop();
    is_showed = false;
}

void Shop::updateNoAdsButton() {
    if (!is_showed) {
        LOG_INFO("Try to hide buy no_ads button on closed Shop");
        return;
    }
    nlohmann::json json;
    json["buyed_goods"][0] = component::shop->noAdsBought();
    json["buyed_goods"][1] = false;
    json["buyed_goods"][2] = false;
    json["buyed_goods"][3] = false;
    notify::gui::update::shop(json);
}

}
