#include "ui/DailyTasks.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void DailyTasks::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::DailyTasks");
        return;
    }
    notify::gui::popup::daily_tasks();
    is_showed = true;
}

void DailyTasks::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::DailyTasks");
        return;
    }
    notify::gui::close::daily_tasks();
    is_showed = false;
}

}
