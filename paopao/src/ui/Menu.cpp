#include "ui/Menu.h"
#include "3rdParty/json.hpp"
#include "game/Game.h"
#include "game/State.h"
#include "levels/Store.h"
#include "shop/Shop.h"
#include "common/Strings.h"
#include "common/Rules.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "levels/Page.h"

namespace ui {

void Menu::show() {
	if (active) {
		LOG_ERROR("Try to show already active menu");
		return;
	}
	nlohmann::json json{};
	json["background"] = static_cast<int>(component::state->levelType) + 1;
	json["page_size"]["width"] = common::Rules::PageWidth;
	json["page_size"]["height"] = common::Rules::PageHeight;
	json["active_level"] = component::state->page * common::Rules::PageSize + 1;
	json["current_level"] = component::state->activeLevel();
	levels::Pages pages{ component::store->getPages(component::state->levelType, component::state->page) };
	if (pages.left) {
		json["pages"]["left"]["color"] = pages.left.get().color;
		json["pages"]["left"]["rules"] = pages.left.get().rules;
		json["pages"]["left"]["hard"] = pages.left.get().hard;
		json["pages"]["left"]["stars"] = component::state->getStars(component::state->levelType, component::state->page - 1);
	}
	if (pages.active) {
		json["pages"]["active"]["color"] = pages.active.get().color;
		json["pages"]["active"]["rules"] = pages.active.get().rules;
		json["pages"]["active"]["hard"] = pages.active.get().hard;
		json["pages"]["active"]["stars"] = component::state->getStars(component::state->levelType, component::state->page);
	}
	if (pages.right) {
		json["pages"]["right"]["color"] = pages.right.get().color;
		json["pages"]["right"]["rules"] = pages.right.get().rules;
		json["pages"]["right"]["hard"] = pages.right.get().hard;
		json["pages"]["right"]["stars"] = component::state->getStars(component::state->levelType, component::state->page + 1);
	}
	json["no_ads_present"] = component::shop->buttonNoAdsShow();
	json["shop_activator"] = component::shop->isActive();
	/*
	    "background": 1,
		"page_size": {"width": 5, "height": 4},
		"active_level": 41,
		"current_level": 54,
		"pages": {
		"left": {
			"color": "00000000000000000000",
			"rules": "01234567890123456789",
			"stars": "23121232123211123231",
			"hard" : "00000000010000000001"
		},
		"active": {
			"color": "00112233440011223344",
			"rules": "99887766554433221100",
			"stars": "23121232123210000000",
			"hard" : "00000000010000000001"
		},
		"right": {
			"color": "11111222223333344444",
			"rules": "01234567890123456789",
			"stars": "00000000000000000000",
			"hard" : "00000000010000000001"
		}
		},
		"no_ads_present": true
	}
	*/
	active = true;
	notify::gui::show::menu(json.dump(2));
}

void Menu::hide() {
	if (!active) {
		LOG_ERROR("Try to hide already hidden menu");
		return;
	}
	active = false;
	notify::gui::hide::menu();
}

void Menu::updateNoAdsButton() {
	if (active) {
		nlohmann::json json;
		json["no_ads_present"] = component::shop->buttonNoAdsShow();
		notify::gui::update::menu(json);
	}
	else {
		LOG_INFO("Try to hide buy no_ads button on closed Menu");
	}
}

void Menu::updateShopButton() {
	if (active) {
		nlohmann::json json;
		json["shop_activator"] = component::shop->isActive();
		notify::gui::update::menu(json);
	}
	else {
		LOG_INFO("Try to show Shop button on closed Menu");
	}
}

}
