#include "ui/DailyBonus.h"
#include <vector>
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"
#include "gameplay/Controller.h"

namespace ui {
namespace {
    std::vector<std::string> BONUS_TYPES{ "bomb", "mix", "hint", "no_ads" };
}

void DailyBonus::show(const std::string& title, const size_t bonus, bool unpause_on_close) {
    if (bonus >= BONUS_TYPES.size()) {
        LOG_ERROR("Try to show DailyBonus window with unknown bonus [%zd]", bonus);
        return;
    }
    nlohmann::json json;
    json["title"] = title;
    json["type"] = BONUS_TYPES[bonus];
    notify::gui::popup::daily_bonus(json);
    this->unpause_on_close = unpause_on_close;
}

void DailyBonus::hide() {
    notify::gui::close::daily_bonus();
    if (unpause_on_close) component::controller->unpause();
}

}
