#include "ui/LoseWindow.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "i18n/L10n.h"
#include "3rdParty/json-builder.h"

namespace ui {

void LoseWindow::show(bool continue_button) {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::LoseWindow");
        return;
    }
    json_value* json = json_object_new(0);
    json_object_push(json, "to_menu", json_string_new(component::l10n->get("To menu").c_str()));
    json_object_push(json, "add_time", json_string_new(component::l10n->get("+30 sec").c_str()));
    json_object_push(json, "replay", json_string_new(component::l10n->get("Replay").c_str()));
    json_object_push(json, "hide_continue", json_boolean_new(!continue_button));
    char* buf = new char[json_measure(json)];
    json_serialize(buf, json);
    notify::gui::popup::lose_window(buf);
    delete[] buf;
    json_value_free(json);
    is_showed = true;
}

void LoseWindow::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::LoseWindow");
        return;
    }
    notify::gui::close::lose_window();
    is_showed = false;
}

void LoseWindow::hide_continue() {
    if (!is_showed) {
        LOG_ERROR("Try to call LoseWindow::remove_ads_button on non showed ads");
        return;
    }
    json_value* json = json_object_new(0);
    json_object_push(json, "hide_continue", json_boolean_new(false));
    char* buf = new char[json_measure(json)];
    json_serialize(buf, json);
    notify::gui::update::lose_window(buf);
    delete[] buf;
    json_value_free(json);
}

}
