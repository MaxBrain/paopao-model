#include "ui/LevelRules.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "gameplay/Level.h"
#include "gameplay/Controller.h"
#include "notify/notify.h"
#include "i18n/L10n.h"
#include "3rdParty/json.hpp"

namespace ui {


namespace {
	std::string rules_line{ "0123456789a" };
}

void LevelRules::show(int level) {
	if (is_showed) {
		LOG_ERROR("Try to show already showed ui::LevelRules");
		return;
	}
	component::gameplay->pause();
	nlohmann::json json;
	json["title"] = component::l10n->get("Level")+" " + std::to_string(level);
	json["hard_level"] = component::level->hard?component::l10n->get("Hard level"):"";
	json["task"] = component::l10n->get("Remove all tiles");
	json["rules_label"] = component::l10n->get("Rules") + ":";
	json["rules"] = std::string{ component::level->rules };
	json["time"] = component::l10n->get("Time") + ":";
	json["timer"] = component::level->baseTime;
	json["OK_label"] = component::l10n->get("OK");
	notify::gui::popup::level_rules(json);
	component::controller->pause();
	is_showed = true;
}

void LevelRules::hide() {
	if (!is_showed) {
		LOG_ERROR("Try to hide already hidden ui::LevelRules");
		return;
	}
	notify::gui::close::level_rules();
	component::gameplay->unpause();
	is_showed = false;
}

}
