#include "ui/Settings.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "game/Game.h"
#include "game/State.h"
#include "i18n/L10n.h"
#include "3rdParty/json.hpp"

namespace ui {

std::string str_toupper(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(),
        [](unsigned char c) { return std::toupper(c); } // correct
    );
    return s;
}

std::string gen_lang(std::string s) {
    std::string lang = str_toupper(s);
    if (lang == "EN") lang = "US";
    if (lang == "ZH-CN") lang = "CN";
    if (lang == "JA") lang = "JP";
    if (lang == "KO") lang = "KR";
    return lang;
}

void Settings::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::Settings");
        return;
    }
    nlohmann::json json;
    json["music"] = component::state->music;
    json["sound"] = component::state->sound;
    json["lang"] = gen_lang(component::state->lang);
    std::vector<std::string> langs;
    for (auto lang : component::l10n->langs_list()) {
        langs.push_back(gen_lang(lang));
    }
    json["langs"] = langs;
    notify::gui::popup::settings(json);
    is_showed = true;
}

void Settings::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::Settings");
        return;
    }
    notify::gui::close::settings();
    is_showed = false;
}

void Settings::update_lang() {
    if (!is_showed) {
        LOG_ERROR("Try to update non-showed ui::Settings");
        return;
    }
    nlohmann::json json;
    json["lang"] = gen_lang(component::state->lang);
    notify::gui::update::settings(json);
}

void Settings::update_sound() {
    if (!is_showed) {
        LOG_ERROR("Try to update non-showed ui::Settings");
        return;
    }
    nlohmann::json json;
    json["sound"] = component::state->sound;
    notify::gui::update::settings(json);
}

void Settings::update_music() {
    if (!is_showed) {
        LOG_ERROR("Try to update non-showed ui::Settings");
        return;
    }
    nlohmann::json json;
    json["music"] = component::state->music;
    notify::gui::update::settings(json);
}

}
