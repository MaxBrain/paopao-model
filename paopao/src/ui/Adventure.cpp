#include "ui/Adventure.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void Adventure::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::Adventure");
        return;
    }
    notify::gui::popup::adventure();
    is_showed = true;
}

void Adventure::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::Adventure");
        return;
    }
    notify::gui::close::adventure();
    is_showed = false;
}

}
