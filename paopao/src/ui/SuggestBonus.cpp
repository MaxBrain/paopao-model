#include "ui/SuggestBonus.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"
#include "shop/Shop.h"
#include "advertising/Ads.h"
#include "ui/UI.h"
#include "common/Rules.h"
#include "bonus/Bonuses.h"

namespace ui {

void SuggestBonus::rewarded_video_complete() {
	component::ui->suggest_bonus.hide();
	switch (component::ui->suggest_bonus.getBonusType()) {
	case bonus::BonusType::Bomb:
		component::ui->daily_bonus.show("", ui::DailyBonus::Bomb, true);
		component::bonuses->bomb += common::Rules::bombForAds;
		component::ui->game_ui.updateBomb();
		break;
	case bonus::BonusType::Hint:
		component::ui->daily_bonus.show("", ui::DailyBonus::Hint, true);
		component::bonuses->hint += common::Rules::hintForAds;
		component::ui->game_ui.updateHint();
		break;
	case bonus::BonusType::Mix:
		component::ui->daily_bonus.show("", ui::DailyBonus::Mix, true);
		component::bonuses->mix += common::Rules::mixForAds;
		component::ui->game_ui.updateMix();
		break;
	}
}

void SuggestBonus::rewarded_video_canceled() {
	if (!component::ui->suggest_bonus.is_showed) {
		component::gameplay->unpause();
	}
}

void SuggestBonus::show(bonus::BonusType bonusType) {
	if (is_showed) {
		LOG_ERROR("Try to show already showed ui::SuggestBonus");
		return;
	}
	const bool inapp_available{ component::shop->isActive() };
	const bool ads_available{ component::ads->canShowRewardedAds() };

	if (!inapp_available && !ads_available) {
		LOG_INFO("Can not suggest variants to get bonuses");
		return;
	}

	nlohmann::json json;
	json["title"] = component::l10n->get("No bonuses?") + " " + component::l10n->get("No problems!");
	json["inapp_available"] = inapp_available;
	json["ads_available"] = ads_available;
	switch (bonusType)
	{
	case bonus::BonusType::Bomb:
		json["bonus"] = "bomb";
		json["count_little"] = shop::BOMB_LITTLE_COUNT;
		json["count_big"] = shop::BOMB_BIG_COUNT;
		json["little_cost"] = component::shop->getCost(shop::ItemType::BOMB_LITTLE);
		json["big_cost"] = component::shop->getCost(shop::ItemType::BOMB_BIG);
		break;
	case bonus::BonusType::Hint:
		json["bonus"] = "hint";
		json["count_little"] = shop::HINT_LITTLE_COUNT;
		json["count_big"] = shop::HINT_BIG_COUNT;
		json["little_cost"] = component::shop->getCost(shop::ItemType::HINT_LITTLE);
		json["big_cost"] = component::shop->getCost(shop::ItemType::HINT_BIG);
		break;
	case bonus::BonusType::Mix:
		json["count_little"] = shop::MIX_LITTLE_COUNT;
		json["count_big"] = shop::MIX_BIG_COUNT;
		json["little_cost"] = component::shop->getCost(shop::ItemType::MIX_LITTLE);
		json["big_cost"] = component::shop->getCost(shop::ItemType::MIX_BIG);
		json["bonus"] = "mix";
		break;
	}
	notify::gui::popup::suggest_bonus(json);

	is_showed = true;
	this->bonusType = bonusType;
}

void SuggestBonus::hide() {
	if (!is_showed) {
		LOG_ERROR("Try to hide already hidden ui::SuggestBonus");
		return;
	}
	notify::gui::close::suggest_bonus();
	is_showed = false;
}

bonus::BonusType SuggestBonus::getBonusType() const {
	return bonusType;
}

void SuggestBonus::click_little_inapp() {
	switch (bonusType)
	{
	case bonus::BonusType::Bomb:
		component::shop->buy(shop::ItemType::BOMB_LITTLE);
		return;
	case bonus::BonusType::Hint:
		component::shop->buy(shop::ItemType::HINT_LITTLE);
		return;
	case bonus::BonusType::Mix:
		component::shop->buy(shop::ItemType::MIX_LITTLE);
		return;
	}
	LOG_ERROR("SuggestBonus::click_little_inapp with unknown bonusType [%d]", static_cast<int>(bonusType));
}

void SuggestBonus::click_big_inapp() {
	switch (bonusType)
	{
	case bonus::BonusType::Bomb:
		component::shop->buy(shop::ItemType::BOMB_BIG);
		return;
	case bonus::BonusType::Hint:
		component::shop->buy(shop::ItemType::HINT_BIG);
		return;
	case bonus::BonusType::Mix:
		component::shop->buy(shop::ItemType::MIX_BIG);
		return;
	}
	LOG_ERROR("SuggestBonus::click_big_inapp with unknown bonusType [%d]", static_cast<int>(bonusType));
}

void SuggestBonus::click_view_ads() {
	updateRewardedVideoState();
	if (component::ads->canShowRewardedAds()) {
		component::ads->showRewardedAds(rewarded_video_complete, rewarded_video_canceled);
	}
	else {
		LOG_ERROR("Can not show rewarded video");
	}
}

void SuggestBonus::updateRewardedVideoState() {
	if (component::shop->isActive()) {
		nlohmann::json json;
		json["ads_available"] = false;
		notify::gui::update::suggest_bonus(json);
	}
	else {
		hide();
	}
}

}
