#include "ui/RateExternal.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void RateExternal::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::RateExternal");
        return;
    }
    notify::gui::popup::rate_external();
    is_showed = true;
}

void RateExternal::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::RateExternal");
        return;
    }
    notify::gui::close::rate_external();
    is_showed = false;
}

}
