#include "ui/UIType.h"

namespace ui {

UIType ConverToUIType(const std::string& str) {
	if (str == kAbout) return UIType::About;
	if (str == kAdsForHint) return UIType::AdsForHint;
	if (str == kAdventure) return UIType::Adventure;
	if (str == kCollectionComplete) return UIType::CollectionComplete;
	if (str == kCollections) return UIType::Collections;
	if (str == kDailyBonus) return UIType::DailyBonus;
	if (str == kDailyTasks) return UIType::DailyTasks;
	if (str == kGameUi) return UIType::GameUi;
	if (str == kLoseWindow) return UIType::LoseWindow;
	if (str == kMenu) return UIType::Menu;
	if (str == kNewElement) return UIType::NewElement;
	if (str == kPauseWindow) return UIType::PauseWindow;
	if (str == kRateExternal) return UIType::RateExternal;
	if (str == kRateInternal) return UIType::RateInternal;
	if (str == kSettings) return UIType::Settings;
	if (str == kShop) return UIType::Shop;
	if (str == kTermOfConditions) return UIType::TermOfConditions;
	if (str == kWinWindow) return UIType::WinWindow;
	return UIType::None;
}

}
