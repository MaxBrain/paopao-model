#include "ui/GameUi.h"
#include "3rdParty/json.hpp"
#include "component/Components.h"
#include "common/Strings.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "bonus/Bonuses.h"
#include "gameplay/Level.h"
#include "game/State.h"

namespace ui {

namespace {
static const std::string kTime{ "time" };
static const std::string kTotal{ "total" };
static const std::string kSound{ "sound" };
static const std::string kBonuses{ "bonuses" };
}

void GameUi::show() {
    if (is_showed) {
        LOG_INFO("Try to show already showed ui::GameUi. Update Total time");
        nlohmann::json json;
        json[kTotal] = component::level->baseTime;
        json[kTime] = component::level->time;
        json[kBonuses] = !component::state->firstRun;
        notify::gui::update::game_ui(json.dump());
        return;
    }
    nlohmann::json json;
    json[kBomb] = component::bonuses->bomb;
    json[kHint] = component::bonuses->hint;
    json[kMix] = component::bonuses->mix;
    json[kTime] = component::level->time;
    json[kTotal] = component::level->baseTime;
    json[kSound] = component::state->game_ui_sound();
    json[kBonuses] = !component::state->firstRun;
    notify::gui::show::game_ui(json);
    is_showed = true;
}

void GameUi::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::GameUi");
        return;
    }
    notify::gui::hide::game_ui();
    is_showed = false;
}

void GameUi::updateTimer(float time) {
    int t = static_cast<int>(floor(time + 1));
    if (previous_level_time == t) return;
    previous_level_time = t;
    nlohmann::json json;
    json[kTime] = t;
    notify::gui::update::game_ui(json);
}

void GameUi::updateSound() {
    nlohmann::json json;
    json[kSound] = component::state->game_ui_sound();
    notify::gui::update::game_ui(json);
}

void GameUi::updateBomb() {
    nlohmann::json json;
    json[kBomb] = component::bonuses->bomb;
    notify::gui::update::game_ui(json);
}

void GameUi::updateHint() {
    nlohmann::json json;
    json[kHint] = component::bonuses->hint;
    notify::gui::update::game_ui(json);
}

void GameUi::updateMix() {
    nlohmann::json json;
    json[kMix] = component::bonuses->mix;
    notify::gui::update::game_ui(json);
}


}
