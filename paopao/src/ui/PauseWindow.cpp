#include "ui/PauseWindow.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void PauseWindow::show() {
	if (is_showed) {
		LOG_ERROR("Try to show already showed ui::PauseWindow");
		return;
	}
	nlohmann::json json;
	json["resume"] = component::l10n->get("Resume");
	json["replay"] = component::l10n->get("Replay");
	json["to_menu"] = component::l10n->get("To menu");
	notify::gui::popup::pause_window(json.dump());
	component::gameplay->pause();
	is_showed = true;
}

void PauseWindow::hide() {
	if (!is_showed) {
		LOG_ERROR("Try to hide already hidden ui::PauseWindow");
		return;
	}
	notify::gui::close::pause_window();
	component::gameplay->unpause();
	is_showed = false;
}

}
