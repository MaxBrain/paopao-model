#include "ui/AdsForHint.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void AdsForHint::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::AdsForHint");
        return;
    }
    notify::gui::popup::ads_for_hint();
    is_showed = true;
}

void AdsForHint::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::AdsForHint");
        return;
    }
    notify::gui::close::ads_for_hint();
    is_showed = false;
}

}
