#include "ui/CollectionComplete.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

void CollectionComplete::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::CollectionComplete");
        return;
    }
    notify::gui::popup::collection_complete();
    is_showed = true;
}

void CollectionComplete::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::CollectionComplete");
        return;
    }
    notify::gui::close::collection_complete();
    is_showed = false;
}

}
