#include "ui/WinWindow.h"
#include "3rdParty/json.hpp"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "i18n/L10n.h"

namespace ui {

void WinWindow::show(int stars, bool hide_next) {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::WinWindow");
        return;
    }
    if (stars < 1) {
        LOG_ERROR("Less than 1 star on ui::WinWindow");
        stars = 1;
    }
    if (stars > 3) {
        LOG_ERROR("More than 3 star on ui::WinWindow");
        stars = 3;
    }
    nlohmann::json json;
    json["title"] = component::l10n->get("You win!");
    json["to_menu"] = component::l10n->get("To menu");
    json["next"] = component::l10n->get("Next");
    json["replay"] = component::l10n->get("Replay");
    json["stars"] = stars;
    json["hide_next"] = hide_next;
    notify::gui::popup::win_window(json.dump());
    is_showed = true;
}

void WinWindow::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::WinWindow");
        return;
    }
    notify::gui::close::win_window();
    is_showed = false;
}

}
