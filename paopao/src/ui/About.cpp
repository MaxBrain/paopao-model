#include "ui/About.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "3rdParty/json.hpp"
#include "i18n/L10n.h"

namespace ui {

namespace {
    std::string kAboutData{ "{}" };
}

void About::show() {
    if (is_showed) {
        LOG_ERROR("Try to show already showed ui::About");
        return;
    }
    notify::gui::popup::about(kAboutData);
    is_showed = true;
}

void About::hide() {
    if (!is_showed) {
        LOG_ERROR("Try to hide already hidden ui::About");
        return;
    }
    notify::gui::close::about();
    is_showed = false;
}

}
