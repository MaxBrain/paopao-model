#include "player/Player.h"
#include "common/Utils.h"
#include "mediator/Observer.h"
#include "mediator/ConsoleOutput.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "game/Game.h"
#include "i18n/L10n.h"
#include "config/Config.h"
#include "input/input.h"
#include "mediator/Logger.h"

namespace player {

class PlayChecker : public Observer<Parameters> {
public:
    Player& player;
    PlayChecker(Player& player) : player(player) {}
    void process(const Parameters& parameters) override {
        player.output.push_back(parameters);
    }
};

std::vector<PlayRecord> Player::parseTrack(const std::string& track) {
    std::vector<std::string> lines = common::utils::split(track, '\n');
    std::vector<std::string> fixedLines;
    for (const auto& line : lines) {
        if ((line.find(delimeter) != std::string::npos) && (std::isdigit(line[0])))
            fixedLines.push_back(line);
        else
            fixedLines.back() += "\n" + line;
    }
    std::vector<PlayRecord> result;
    for (const auto& line : fixedLines) {
        result.push_back(parsePlayRecord(line));
    }
    return result;
}

void Player::checkOutput() {
    size_t t = component::game->current_tick();
    for (const auto parameters : output) {
        auto it = std::find_if(trackOutput[t].begin(), trackOutput[t].end(),
            [&parameters]
        (const Parameters& p)->bool {
            return ParametersComparator{}(parameters, p);
        }
        );
        if (it == trackOutput[t].end()) {
            LOG_INFO("Tick: %zd\n", component::game->current_tick());
            LOG_INFO("=== CAN NOT FIND ======================\n");
            ConsoleOutput::print(parameters);
            LOG_INFO("====     IN     =========================\n");
            for(const auto& p :trackOutput[t]) {
                ConsoleOutput::print(p);
            }
            LOG_INFO("=======================================\n");
#ifdef __EXCEPTIONS
            throw std::exception{ "wrong output message or output message in wrong place" };
#endif // __EXCEPTIONS
        }
        else
        {
            trackOutput[t].erase(it);
        }
    }
    output.clear();
    if (!trackOutput[t].empty()) {
        LOG_INFO("=== EXTRA MESSAGES IN LOG =============\n");
        LOG_INFO("Tick: %zd\n", component::game->current_tick());
        for (const auto& p : trackOutput[t]) {
            ConsoleOutput::print(p);
        }
        LOG_INFO("=======================================\n");
#ifdef __EXCEPTIONS
        throw std::exception{ "Extra messges in log" };
#endif // __EXCEPTIONS
    }
}

void Player::applyMessage(const PlayRecord& playRecord) {
    switch (playRecord.type) {
    case PlayRecordType::i18n:
        component::l10n->load(playRecord.parameters[0].get_unchecked<std::string>());
        break;
    case PlayRecordType::config:
        component::config->load(playRecord.parameters[0].get_unchecked<std::string>());
        break;
    case PlayRecordType::load:
        component::game->load(playRecord.parameters[0].get_unchecked<std::string>());
        break;
    case PlayRecordType::input:
        input::input(playRecord.parameters);
        break;
    case PlayRecordType::output:
        break;
    case PlayRecordType::unknown:
        break;
    }
}

void Player::applyMessages(size_t tick) {
    while (current_record<records.size()) {
        const PlayRecord& playRecord{ records[current_record] };
        if (playRecord.tick > tick) return;
        applyMessage(playRecord);
        current_record++;
    }
}

void Player::fillOutput() {
    for (auto record : records) {
        if (record.type == PlayRecordType::output) {
            trackOutput[record.tick].push_back(record.parameters);
        }
    }
}

void Player::playFile(const std::string& filename) {
    std::string track{ common::utils::get_file_contents(filename) };
    records = parseTrack(track);
    fillOutput();

    PlayChecker playChecker{ *this };
    notifier.addObserver(&playChecker);

    current_record = 0;

    applyMessages(0);
    checkOutput();

    while (current_record < records.size()) {
        applyMessages(component::game->current_tick());
        checkOutput();
        component::game->tick(1.0f / 60);
    }

    notifier.removeObserver(&playChecker);
}

}
