#include "player/PlayRecordType.h"

namespace player {

PlayRecordType toPlayRecordType(const std::string& string) {
    if (string == "i18n") return PlayRecordType::i18n;
    if (string == "config") return PlayRecordType::config;
    if (string == "load") return PlayRecordType::load;
    if (string == "input") return PlayRecordType::input;
    if (string == "output") return PlayRecordType::output;
    return PlayRecordType::unknown;
}

}
