#include "player/PlayRecord.h"
#include <sstream>
#include "common/Utils.h"

namespace player {

namespace {
bool isFloat(std::string str) {
    /*
    if (!str.empty() && str[str.length() - 1] == '\r') {
        str.pop_back();
    }
    */
    std::istringstream iss(str);
    float f;
    iss >> std::noskipws >> f;
    return iss.eof() && !iss.fail();
}

Parameter parseParameter(const std::string& p) {
    if (isFloat(p)) {
        std::istringstream iss(p);
        float f;
        iss >> f;
        return f;
    }
    else {
        return p;
    }
}
}

PlayRecord parsePlayRecord(std::string string) {
    while (!string.empty() && string.back() == '\r') string.pop_back();
    std::vector<std::string> parts = common::utils::split(string, delimeter);
    PlayRecord playRecord;
    playRecord.tick = std::stoul(parts[0]);
    playRecord.type = toPlayRecordType(parts[1]);
    for (size_t i = 2; i < parts.size(); i++) {
        if (parts[i] == "nil") break;
        playRecord.parameters.push_back(parseParameter(parts[i]));
    }
    return playRecord;
}

}
