#include "mediator/ConsoleOutput.h"
#include "mapbox/variant.hpp"
#include "mediator/Logger.h"

std::string line;

struct ResponseVisitor {
	void operator()(int r) const {
		line += std::to_string(r);
	}

	void operator()(float e) const {
		line += std::to_string(e);
	}

	void operator()(const std::string& s) const {
		line += s;
	}
};

void ConsoleOutput::print(Parameters parameters) {
	ResponseVisitor visitor;
	line = "";
	for (auto parameter : parameters) {
		line += "[";
		mapbox::util::apply_visitor(visitor, parameter);
		line += "]";
	}
	if (!parameters.empty()) LOG_INFO("%s\n", line.c_str());
};
