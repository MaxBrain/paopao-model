#include <algorithm>
#include "mediator/ControllerTypes.h"
#include "3rdParty/json.hpp"

bool ParameterComparator::operator()(const Parameter& lhs, const Parameter& rhs) const {
	if ((lhs.is<std::string>() && rhs.is<std::string>())) {
		auto l = lhs.get_unchecked<std::string>();
		auto r = rhs.get_unchecked<std::string>();
		if (l == r) return true;
		auto l_json{nlohmann::json::parse(l, nullptr, false)};
		auto r_json{ nlohmann::json::parse(r, nullptr, false) };
		if (!l_json.is_discarded() && !r_json.is_discarded()) {
			return l_json == r_json;
		}
	}
	if (lhs.which() == rhs.which()) {
		if (lhs.is<std::string>()) return lhs.get_unchecked<std::string>() == rhs.get_unchecked<std::string>();
		if (lhs.is<int>()) return lhs.get_unchecked<int>() == rhs.get_unchecked<int>();
		if (lhs.is<float>()) return lhs.get_unchecked<float>() == rhs.get_unchecked<float>();
	}
	if ((lhs.is<float>() && rhs.is<int>())) {
		float l = lhs.get_unchecked<float>();
		float r = static_cast<float>(rhs.get_unchecked<int>());
		return l == r;
	}
	if ((lhs.is<int>() && rhs.is<float>())) {
		float l = static_cast<float>(lhs.get_unchecked<int>());
		float r = rhs.get_unchecked<float>();
		return l == r;
	}
	return false;
}

bool ParametersComparator::operator()(const Parameters& lhs, const Parameters& rhs) const {
	if (lhs.size() != rhs.size()) return false;
	return std::equal(lhs.begin(), lhs.end(), rhs.begin(), ParameterComparator{});
}
