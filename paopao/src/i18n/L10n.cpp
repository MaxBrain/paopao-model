#include <algorithm>
#include "i18n/L10n.h"
#include "mediator/Logger.h"
#include "3rdParty/json.hpp"

namespace i18n {

void L10n::load(const std::string& file) {
	auto json{ nlohmann::json::parse(file, nullptr, false) };
	if (json.is_discarded()) {
		LOG_ERROR("Can not load localization. Parse error");
		return;
	}
	if (!json.is_object()) {
		LOG_ERROR("Can not load localization. Not object");
		return;
	}
	auto json_langs_ = json["langs"];
	auto json_data = json["data"];
	langs.clear();
	for (auto l : json_langs_) {
		langs.push_back(l);
	}
	data.clear();
	for (auto& record : json_data.items()) {
		for (auto& str : record.value()) {
			data[record.key()].push_back(str);
		}
	}
}

const std::string& L10n::get(const std::string& key) const {
	auto it = data.find(key);
	if (it == data.end()) {
		LOG_ERROR("can not localize key [%s]", key.c_str());
		return key;
	}
	if (it->second.size()<=lang) {
		LOG_ERROR("can not find lang [%zd] for key [%s]", lang, key.c_str());
		return key;
	}
	return it->second[lang];
}

void L10n::translate(const std::string& lang_) {
	auto it = std::find(std::begin(langs), std::end(langs), lang_);
	if (it == std::end(langs)) {
		LOG_ERROR("No localization for [%s]", lang_.c_str());
		return;
	}
	this->lang = it - std::begin(langs);
}

const std::vector<std::string>& L10n::langs_list() const {
	return langs;
}

}
