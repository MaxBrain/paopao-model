#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace rewarded_ads {
void complete(const Parameters& parameters) {
	component::ads->call_rewarded_complete();
}
}
}
