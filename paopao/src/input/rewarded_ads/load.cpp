#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "advertising/Ads.h"

namespace input {
namespace rewarded_ads {
void load(const Parameters& parameters) {
	component::ads->setRewardedAdsLoaded();
}
}
}
