#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
void block_input(const Parameters& parameters) {
	if (parameters.size() < 2) {
		LOG_ERROR("Less than 2 parameters on input::block_input");
		return;
	}
	if (!parameters[1].is<float>()) {
		LOG_ERROR("2 parameters on input::block_input is not float");
		return;
	}
	component::controller->block_input(parameters[1].get_unchecked<float>());
}
}
