#include "input/handlers.h"
#include "mediator/Logger.h"

namespace input{
void input(const Parameters& parameters) {
  if (parameters.size() <= 0) return;
  if (!parameters[0].is<std::string>()) return;
  if (parameters[0].get_unchecked<std::string>() == "gui") {
    if (parameters.size() <= 1) return;
    if (!parameters[1].is<std::string>()) return;
    if (parameters[1].get_unchecked<std::string>() == "about") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::about::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "music") {
        input::gui::about::music(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "OK") {
        input::gui::about::OK(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::about::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "ads_for_hint") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::ads_for_hint::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "watch") {
        input::gui::ads_for_hint::watch(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::ads_for_hint::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "adventure") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::adventure::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "active_challenge") {
        input::gui::adventure::active_challenge(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::adventure::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "collection_complete") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::collection_complete::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::collection_complete::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "collections") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::collections::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::collections::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "daily_bonus") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::daily_bonus::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::daily_bonus::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "daily_tasks") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::daily_tasks::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "goto_1") {
        input::gui::daily_tasks::goto_1(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "goto_2") {
        input::gui::daily_tasks::goto_2(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "goto_3") {
        input::gui::daily_tasks::goto_3(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "goto_4") {
        input::gui::daily_tasks::goto_4(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "goto_5") {
        input::gui::daily_tasks::goto_5(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::daily_tasks::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "game_ui") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "sound") {
        input::gui::game_ui::sound(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "hint") {
        input::gui::game_ui::hint(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "bomb") {
        input::gui::game_ui::bomb(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "mix") {
        input::gui::game_ui::mix(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "pause") {
        input::gui::game_ui::pause(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::game_ui::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "level_rules") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "OK") {
        input::gui::level_rules::OK(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::level_rules::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "lose_window") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "to_menu") {
        input::gui::lose_window::to_menu(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "add_time") {
        input::gui::lose_window::add_time(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "replay") {
        input::gui::lose_window::replay(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::lose_window::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "menu") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "achievements") {
        input::gui::menu::achievements(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "settings") {
        input::gui::menu::settings(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "challenge") {
        input::gui::menu::challenge(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "no_ads") {
        input::gui::menu::no_ads(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "like") {
        input::gui::menu::like(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "collections") {
        input::gui::menu::collections(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "tasks") {
        input::gui::menu::tasks(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "prev") {
        input::gui::menu::prev(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "next") {
        input::gui::menu::next(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "about") {
        input::gui::menu::about(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "shop") {
        input::gui::menu::shop(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "level") {
        input::gui::menu::level(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::menu::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "new_element") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::new_element::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::new_element::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "pause_window") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::pause_window::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "resume") {
        input::gui::pause_window::resume(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "replay") {
        input::gui::pause_window::replay(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "to_menu") {
        input::gui::pause_window::to_menu(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::pause_window::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "rate_external") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::rate_external::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "not_now") {
        input::gui::rate_external::not_now(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "rate") {
        input::gui::rate_external::rate(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::rate_external::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "rate_internal") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::rate_internal::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "bad") {
        input::gui::rate_internal::bad(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "later") {
        input::gui::rate_internal::later(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "good") {
        input::gui::rate_internal::good(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::rate_internal::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "settings") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::settings::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "switch_sound") {
        input::gui::settings::switch_sound(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "switch_music") {
        input::gui::settings::switch_music(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_1") {
        input::gui::settings::lang_1(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_2") {
        input::gui::settings::lang_2(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_3") {
        input::gui::settings::lang_3(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_4") {
        input::gui::settings::lang_4(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_5") {
        input::gui::settings::lang_5(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_6") {
        input::gui::settings::lang_6(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_7") {
        input::gui::settings::lang_7(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_8") {
        input::gui::settings::lang_8(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_9") {
        input::gui::settings::lang_9(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_10") {
        input::gui::settings::lang_10(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_11") {
        input::gui::settings::lang_11(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "lang_12") {
        input::gui::settings::lang_12(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::settings::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "shop") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::shop::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "buy_1") {
        input::gui::shop::buy_1(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "buy_2") {
        input::gui::shop::buy_2(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "buy_3") {
        input::gui::shop::buy_3(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "buy_4") {
        input::gui::shop::buy_4(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::shop::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "suggest_bonus") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::suggest_bonus::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "ads") {
        input::gui::suggest_bonus::ads(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "little_inapp") {
        input::gui::suggest_bonus::little_inapp(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "big_inapp") {
        input::gui::suggest_bonus::big_inapp(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::suggest_bonus::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "term_of_conditions") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "close") {
        input::gui::term_of_conditions::close(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "policy") {
        input::gui::term_of_conditions::policy(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "agree") {
        input::gui::term_of_conditions::agree(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::term_of_conditions::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "win_window") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "to_menu") {
        input::gui::win_window::to_menu(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "next") {
        input::gui::win_window::next(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      if (parameters[2].get_unchecked<std::string>() == "replay") {
        input::gui::win_window::replay(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::win_window::*");
    } else
    if (parameters[1].get_unchecked<std::string>() == "the_rules") {
      if (parameters.size() <= 2) return;
      if (!parameters[2].is<std::string>()) return;
      if (parameters[2].get_unchecked<std::string>() == "OK") {
        input::gui::the_rules::OK(Parameters{parameters.begin() + 3, parameters.end()});
      } else
      LOG_ERROR("Bad call with gui::the_rules::*");
    } else
    LOG_ERROR("Bad call with gui::*");
  } else
  if (parameters[0].get_unchecked<std::string>() == "block_input") {
    input::block_input(Parameters{parameters.begin() + 1, parameters.end()});
  } else
  if (parameters[0].get_unchecked<std::string>() == "click") {
    input::click(Parameters{parameters.begin() + 1, parameters.end()});
  } else
  if (parameters[0].get_unchecked<std::string>() == "drag") {
    input::drag(Parameters{parameters.begin() + 1, parameters.end()});
  } else
  if (parameters[0].get_unchecked<std::string>() == "move") {
    input::move(Parameters{parameters.begin() + 1, parameters.end()});
  } else
  if (parameters[0].get_unchecked<std::string>() == "drop") {
    input::drop(Parameters{parameters.begin() + 1, parameters.end()});
  } else
  if (parameters[0].get_unchecked<std::string>() == "interscenial_ads") {
    if (parameters.size() <= 1) return;
    if (!parameters[1].is<std::string>()) return;
    if (parameters[1].get_unchecked<std::string>() == "load") {
      input::interscenial_ads::load(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    LOG_ERROR("Bad call with interscenial_ads::*");
  } else
  if (parameters[0].get_unchecked<std::string>() == "rewarded_ads") {
    if (parameters.size() <= 1) return;
    if (!parameters[1].is<std::string>()) return;
    if (parameters[1].get_unchecked<std::string>() == "load") {
      input::rewarded_ads::load(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    if (parameters[1].get_unchecked<std::string>() == "complete") {
      input::rewarded_ads::complete(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    if (parameters[1].get_unchecked<std::string>() == "cancel") {
      input::rewarded_ads::cancel(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    LOG_ERROR("Bad call with rewarded_ads::*");
  } else
  if (parameters[0].get_unchecked<std::string>() == "inapp") {
    if (parameters.size() <= 1) return;
    if (!parameters[1].is<std::string>()) return;
    if (parameters[1].get_unchecked<std::string>() == "init") {
      input::inapp::init(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    if (parameters[1].get_unchecked<std::string>() == "bought") {
      input::inapp::bought(Parameters{parameters.begin() + 2, parameters.end()});
    } else
    LOG_ERROR("Bad call with inapp::*");
  } else
  LOG_ERROR("Bad call with empty parameters list");
}
}
