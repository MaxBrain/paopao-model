#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
void drag(const Parameters& parameters) {
    if (parameters.size() < 2) {
        LOG_ERROR("Less than 2 parameters on input::drag");
        return;
    }
    if (!parameters[0].is<float>()) {
        LOG_ERROR("1 parameters on input::drag is not float");
        return;
    }
    if (!parameters[1].is<float>()) {
        LOG_ERROR("2 parameters on input::drag is not float");
        return;
    }
    component::controller->process3parameters(
        "drag",
        parameters[0].get_unchecked<float>(),
        parameters[1].get_unchecked<float>()
    );
}
}
