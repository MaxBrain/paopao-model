#include "mediator/ControllerTypes.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace rate_external {
void close(const Parameters& parameters) {
	component::ui->rate_external.hide();
}
}
}
}
