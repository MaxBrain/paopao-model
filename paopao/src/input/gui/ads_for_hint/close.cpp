#include "mediator/ControllerTypes.h"
#include "component/Components.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace ads_for_hint {
void close(const Parameters& parameters) {
	component::ui->ads_for_hint.hide();
}
}
}
}
