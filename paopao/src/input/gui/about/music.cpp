#include <string>
#include "mediator/ControllerTypes.h"
#include "notify/notify.h"

namespace input {
namespace gui {
namespace about {

const std::string kBenSoundSite{ "https://www.bensound.com/" };

void music(const Parameters& parameters) {
	notify::sys::navigate(kBenSoundSite);
}
}
}
}
