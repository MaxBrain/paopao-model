#include "mediator/ControllerTypes.h"
#include "component/Components.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace about {
void close(const Parameters& parameters) {
	component::ui->about.hide();
}
}
}
}
