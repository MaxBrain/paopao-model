#include "mediator/ControllerTypes.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace daily_tasks {
void close(const Parameters& parameters) {
	component::ui->daily_tasks.hide();
}
}
}
}
