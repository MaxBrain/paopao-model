#include "mediator/ControllerTypes.h"
#include "notify/notify.h"

namespace input {
namespace gui {
namespace rate_internal {
void close(const Parameters& parameters) {
	notify::gui::close::rate_internal();
}
}
}
}
