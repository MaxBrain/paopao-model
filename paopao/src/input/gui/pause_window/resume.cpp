#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "notify/notify.h"
#include "gameplay/Gameplay.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace pause_window {
void resume(const Parameters& parameters) {
	component::ui->pause_window.hide();
	component::gameplay->unpause();
}
}
}
}
