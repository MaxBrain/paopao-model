#include "mediator/ControllerTypes.h"
#include "component/Components.h"
#include "notify/notify.h"
#include "gameplay/Gameplay.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace pause_window {
void replay(const Parameters& parameters) {
	notify::state::clear_arrays();
	component::ui->pause_window.hide();
	component::gameplay->replay();
}
}
}
}
