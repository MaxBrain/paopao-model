#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace pause_window {
void close(const Parameters& parameters) {
	component::ui->pause_window.hide();
	component::gameplay->unpause();
}
}
}
}
