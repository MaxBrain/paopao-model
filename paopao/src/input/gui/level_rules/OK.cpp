#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace level_rules {
void OK(const Parameters&) {
	component::ui->level_rules.hide();
}
}
}
}
