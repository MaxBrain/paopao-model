#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace menu {
void shop(const Parameters&) {
	component::ui->shop.show();
}
}
}
}
