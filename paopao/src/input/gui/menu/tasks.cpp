#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace menu {
void tasks(const Parameters& parameters) {
	component::ui->daily_tasks.show();
}
}
}
}
