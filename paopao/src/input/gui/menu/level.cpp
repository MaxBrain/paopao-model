#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace menu {
void level(const Parameters& parameters) {
	if (parameters.size() != 1) {
		LOG_ERROR("Not one parameter for gui::menu::level");
		return;
	}
	if (!parameters[0].is<float>()) {
		LOG_ERROR("Not number as parameter for gui::menu::level");
	}
	size_t level_number = static_cast<size_t>((parameters[0].get_unchecked<float>()));
	levels::LevelType level_type = component::state->levelType;
	if (!component::state->canPlayLevel(level_type, level_number)) return;
	component::ui->menu.hide();
	component::gameplay->play_level(level_type, level_number);
}
}
}
}
