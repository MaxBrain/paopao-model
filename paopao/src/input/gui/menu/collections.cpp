#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace menu {
void collections(const Parameters& parameters) {
	component::ui->collections.show();
}
}
}
}
