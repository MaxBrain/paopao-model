#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace menu {
void settings(const Parameters& parameters) {
	component::ui->settings.show();
}
}
}
}
