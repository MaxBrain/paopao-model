#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace shop {
void close(const Parameters& parameters) {
	component::ui->shop.hide();
}
}
}
}
