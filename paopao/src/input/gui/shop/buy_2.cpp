#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"
#include "notify/notify.h"

namespace input {
namespace gui {
namespace shop {
void buy_2(const Parameters& parameters) {
	notify::inapp::buy(std::string{ "HINT_PACK" });
}
}
}
}
