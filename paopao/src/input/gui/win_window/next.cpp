#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace win_window {
void next(const Parameters& parameters) {
	component::ui->win_window.hide();
	component::gameplay->play_curr_level();
}
}
}
}
