#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace game_ui {
void pause(const Parameters& parameters) {
    component::ui->pause_window.show();
}
}
}
}
