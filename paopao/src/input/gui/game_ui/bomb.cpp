#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "ui/UI.h"
#include "bonus/Bonuses.h"
#include "gameplay/Controller.h"
#include "notify/notify.h"
#include "3rdParty/json.hpp"

namespace input {
namespace gui {
namespace game_ui {
void bomb(const Parameters& parameters) {
    if (component::bonuses->bomb != 0) {
        nlohmann::json json;
        if (component::controller->isBomb()) {
            component::controller->cancelBomb();
            json["active_bomb"] = false;
        }
        else {
            component::controller->useBomb();
            json["active_bomb"] = true;
        }
        notify::gui::update::game_ui(json);
    }
    else {
        component::ui->suggest_bonus.show(bonus::BonusType::Bomb);
    }
}
}
}
}
