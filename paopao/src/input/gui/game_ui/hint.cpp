#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "bonus/Bonuses.h"
#include "ui/UI.h"
#include "notify/notify.h"
#include "common/Strings.h"
#include "3rdParty/json.hpp"

namespace input {
namespace gui {
namespace game_ui {
void hint(const Parameters& parameters) {
    if (component::bonuses->hint != 0) {
        component::bonuses->applyHint(*component::level);
        nlohmann::json json;
        json[kHint] = component::bonuses->hint;
        notify::gui::update::game_ui(json);
    }
    else {
        component::ui->suggest_bonus.show(bonus::BonusType::Hint);
    }
}
}
}
}
