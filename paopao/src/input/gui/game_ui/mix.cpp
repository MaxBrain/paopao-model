#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "bonus/Bonuses.h"
#include "ui/UI.h"
#include "notify/notify.h"
#include "common/Strings.h"
#include "3rdParty/json.hpp"

namespace input {
namespace gui {
namespace game_ui {
void mix(const Parameters& parameters) {
    if (component::bonuses->mix != 0) {
        component::bonuses->applyMix(*component::level);
        nlohmann::json json;
        json[kMix] = component::bonuses->mix;
        notify::gui::update::game_ui(json);
    }
    else {
        component::ui->suggest_bonus.show(bonus::BonusType::Mix);
    }
}
}
}
}
