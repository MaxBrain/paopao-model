#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "game/State.h"
#include "ui/UI.h"
#include "notify/notify.h"

namespace input {
namespace gui {
namespace game_ui {
void sound(const Parameters& parameters) {
	if (component::state->game_ui_sound()) {
		component::state->sound = false;
		component::state->music = false;
		notify::sound::sound_off();
		notify::sound::music_off();
	}
	else {
		component::state->sound = true;
		component::state->music = true;
		notify::sound::sound_on();
		notify::sound::music_on();
	}
	component::ui->game_ui.updateSound();
}
}
}
}
