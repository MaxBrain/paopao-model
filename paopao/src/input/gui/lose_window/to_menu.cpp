#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace lose_window {
void to_menu(const Parameters& parameters) {
	component::gameplay->stop();
	component::ui->lose_window.hide();
	component::ui->game_ui.hide();
	component::ui->menu.show();
}
}
}
}
