#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace lose_window {
namespace {
void add_time_complete() {
	component::ui->lose_window.hide();
	component::gameplay->appendTime();
}
void add_time_cancel() {
}
}

void add_time(const Parameters& parameters) {
	component::ui->lose_window.hide_continue();
	component::ads->showRewardedAds(add_time_complete, add_time_cancel);
}
}
}
}
