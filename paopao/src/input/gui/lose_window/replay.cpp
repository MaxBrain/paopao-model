#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace lose_window {
void replay(const Parameters& parameters) {
	component::ui->lose_window.hide();
	component::gameplay->replay();
}
}
}
}
