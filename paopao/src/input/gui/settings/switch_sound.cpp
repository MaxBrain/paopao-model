#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "notify/notify.h"
#include "game/State.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace settings {
void switch_sound(const Parameters& parameters) {
	component::state->sound = !component::state->sound;
	component::ui->settings.update_sound();
	if (component::state->sound)
		notify::sound::sound_on();
	else
		notify::sound::sound_off();
}
}
}
}
