#include "mediator/ControllerTypes.h"
#include "component/Components.h"
#include "ui/UI.h"
#include "game/State.h"
#include "i18n/L10n.h"

namespace input {
namespace gui {
namespace settings {
void lang_7(const Parameters& parameters) {
	component::state->lang = component::l10n->langs_list()[6];
	component::ui->settings.update_lang();
}
}
}
}
