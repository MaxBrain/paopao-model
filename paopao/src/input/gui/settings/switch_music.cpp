#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "game/State.h"
#include "ui/UI.h"
#include "notify/notify.h"

namespace input {
namespace gui {
namespace settings {
void switch_music(const Parameters& parameters) {
	component::state->music = !component::state->music;
	component::ui->settings.update_music();
	if (component::state->music)
		notify::sound::music_on();
	else
		notify::sound::music_off();
}
}
}
}
