#include "mediator/ControllerTypes.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "ui/UI.h"

namespace input {
namespace gui {
namespace term_of_conditions {
void close(const Parameters& parameters) {
	component::ui->term_of_conditions.hide();
}
}
}
}
