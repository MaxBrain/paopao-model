#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace gui {
namespace suggest_bonus {
void big_inapp(const Parameters& parameters) {
	component::ui->suggest_bonus.click_big_inapp();
}
}
}
}
