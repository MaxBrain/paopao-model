#include "mediator/ControllerTypes.h"
#include "mediator/Logger.h"
#include "component/Components.h"
#include "component/detail.h"

namespace input {
namespace inapp {
void init(const Parameters& parameters) {
	if ((parameters.size() == 1) && parameters[0].is<std::string>()) {
		component::shop->init(parameters[0].get_unchecked<std::string>());
	}
	else {
		LOG_ERROR("Can not parse parameters for input::inapp::init");
	}
}
}
}
