#include "game/StarStore.h"
#include "common/Rules.h"

namespace game {

std::string kEmptyPage{ "00000000000000000000" };
const char base{ '0' };

void StarStore::setStar(levels::LevelType levelType, size_t level, size_t stars) {
	if ((levelType == levels::LevelType::Adventure) &&
		(level >= common::Rules::LevelsInAdventure)) {
		return;
	}
	char realStars = stars > 3 ? 3 : static_cast<char>(stars);
	size_t id{ level - 1 };
	size_t symbol{ id / 3 };
	std::string& dt{ data[static_cast<size_t>(levelType)] };
	while (symbol >= dt.size()) dt+=base;
	size_t diff{ (id % 3) * 2 };
	char mask = 3 << diff;
	char ds = dt[symbol] - base;
	char ds_nolevel = ds & ~mask;
	char fix = ds_nolevel & 63;
	char fixed_data = fix | (realStars << diff);
	char c = fixed_data + base;

	dt[symbol] = c;
}

size_t StarStore::getStar(levels::LevelType levelType, size_t level) const {
	size_t id{ level - 1 };
	size_t symbol{ id / 3 };
	const std::string& dt{data[static_cast<size_t>(levelType)]};
	if (symbol >= dt.size()) return 0;
	size_t diff{ (id % 3) * 2 };
	char mask = 3 << diff;
	return ((dt[symbol] - base) & mask) >> diff;
}

std::string StarStore::getStars(levels::LevelType levelType, size_t page) const {
	switch (levelType)
	{
	case levels::LevelType::Regular:
		break;
	case levels::LevelType::Adventure:
		if (page >= common::Rules::PagesInAdventure) return kEmptyPage;
		break;
	default:
		break;
	}
	std::string result;
	for (size_t i = page * common::Rules::PageSize + 1; i <= (page + 1) * common::Rules::PageSize; i++) {
		result += static_cast<char>(getStar(levelType, i)) + '0';
	}
	return result;
}

}
