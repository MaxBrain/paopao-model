#include "game/State.h"
#include "3rdParty/uuidxx.h"
#include "component/Components.h"
#include "levels/Store.h"
#include "common/Rules.h"
#include "3rdParty/json.hpp"

namespace game {

State::State() {
	session = uuidxx::uuid::Generate().ToString();
}

std::string State::getSession() const {
	return session;
}

size_t State::activeLevel() const {
	return activeLevels[static_cast<size_t>(levelType)];
}

void State::load(const nlohmann::json& data) {
	if (!data.contains(kState)) return;
	auto state{ data[kState] };
	if (state.contains(kFirstRun)) firstRun = state[kFirstRun];
	if (state.contains("music")) music = state["music"];
	if (state.contains("sound")) sound = state["sound"];
	if (state.contains("lang")) lang = state["lang"];
	if (state.contains("system_language")) system_language = state["system_language"];
}

void State::save(nlohmann::json& data) {
	data[kState][kFirstRun] = firstRun;
	data[kState]["music"] = music;
	data[kState]["sound"] = sound;
	data[kState]["lang"] = lang;
	data[kState]["system_language"] = system_language;
}

std::string State::getStars(levels::LevelType levelType_, size_t page_) const {
	return starStore.getStars(levelType_, page_);
}

bool State::haveNextLevel() const {
	switch (levelType)
	{
	case levels::LevelType::Regular:
		return true;
	case levels::LevelType::Adventure:
		return currentLevel < common::Rules::LevelsInAdventure;
	}
	return false;
}

bool State::switchToNextLevel() {
	bool result{haveNextLevel()};
	currentLevel++;
	if (activeLevel() < currentLevel) activeLevels[static_cast<size_t>(levelType)] = currentLevel;
	return result;
}

void State::switchLevelType() {
	switch (levelType)
	{
	case levels::LevelType::Regular:
		levelType = levels::LevelType::Adventure;
		break;
	case levels::LevelType::Adventure:
		levelType = levels::LevelType::Regular;
		break;
	}
	currentLevel = activeLevels[static_cast<size_t>(levelType)];
	page = (currentLevel - 1) / common::Rules::PageSize + 1;
}

bool State::canPlayLevel(levels::LevelType levelType_, size_t level_) const {
	switch (levelType_)
	{
		case levels::LevelType::Regular:
			return level_ <= activeLevels[0];
		case levels::LevelType::Adventure:
			if (level_ > common::Rules::LevelsInAdventure) return false;
			return level_ <= activeLevels[1];
	}
	return false;
}

bool State::game_ui_sound() const {
	return sound || music;
}

}
