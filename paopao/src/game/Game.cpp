#include "component/Components.h"
#include "game/Game.h"
#include "3rdParty/json.hpp"
#include "component/detail.h"
#include "mediator/Logger.h"

namespace game {

void Game::load(std::string data) {
	nlohmann::json json{ nlohmann::json::parse(data, nullptr, false) };
	if (json.is_discarded()) {
		LOG_ERROR("Error while parse saved data: [%s]", data.c_str());
		return;
	}
	component::state->load(json);
	component::adventure->load(json);
	component::shop->load(json);
	component::gameplay->load(json);
	if (component::state->firstRun) {
		component::gameplay->play_level(levels::LevelType::Regular, 1);
		component::state->firstRun = false;
	} else {
		component::ui->menu.show();
	}
	component::daily_bonus->load(json);
	component::daily_quest->load(json);
}

std::string Game::save() {
	nlohmann::json json;
	component::state->save(json);
	component::adventure->save(json);
	component::shop->save(json);
	component::gameplay->save(json);
	component::daily_bonus->save(json);
	component::daily_quest->save(json);
	return json.dump(2);
}

void Game::tick(float dt) {
	ticker++;
	component::gameplay->tick(dt);
}

size_t Game::current_tick() {
	return ticker;
}

}
