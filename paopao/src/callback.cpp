#include "callback.h"
#include "LuaUtils.h"
#include <stdlib.h>

static dmScript::LuaCallbackInfo* defUtoLua = 0x0;
static std::vector<Parameters> m_callbacksQueue;
static dmMutex::HMutex m_mutex;

static void DestroyCallback() {
    if (defUtoLua != 0x0) {
        dmScript::DestroyCallback(defUtoLua);
        defUtoLua = 0x0;
    }
}

static void InvokeCallback(Parameters parameters) {
    if (!dmScript::IsCallbackValid(defUtoLua)) {
        dmLogError("Paopao model callback is invalid. Set new callback unsing `model.setCallback()` funciton.");
        return;
    }

    lua_State* L = dmScript::GetCallbackLuaContext(defUtoLua);
    int top = lua_gettop(L);

    if (!dmScript::SetupCallback(defUtoLua)) {
        return;
    }

    for (size_t i = 0; i<parameters.size(); i++) {
        parameters[i].match(
            [&L](int r) {lua_pushnumber(L, r);},
            [&L](float e) {lua_pushnumber(L, e);},
            [&L](std::string& s) {lua_pushstring(L, s.c_str());}
        );
    }

    int ret = dmScript::PCall(L, parameters.size() + 1, 0);
    (void)ret;
    dmScript::TeardownCallback(defUtoLua);

    assert(top == lua_gettop(L));
}

void Initialize() {
    m_mutex = dmMutex::New();
}

void Finalize() {
    dmMutex::Delete(m_mutex);
    DestroyCallback();
}

void SetLuaCallback(lua_State* L, int pos) {
    int type = lua_type(L, pos);
    if (type == LUA_TNONE || type == LUA_TNIL) {
        DestroyCallback();
    } else {
        defUtoLua = dmScript::CreateCallback(L, pos);
    }
}

void AddToQueue(const Parameters& parameters) {
    m_callbacksQueue.push_back(parameters);
}

void CallbackUpdate() {
    if (m_callbacksQueue.empty()) {
        return;
    }
    DM_MUTEX_SCOPED_LOCK(m_mutex);
    for(uint32_t i = 0; i != m_callbacksQueue.size(); ++i) {
        InvokeCallback(m_callbacksQueue[i]);
    }
    m_callbacksQueue.resize(0);
}
