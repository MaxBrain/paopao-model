#include <cmath>
#include "generator/Generator.h"
#include "common/Rules.h"
#include "common/Random.h"

//#define ERROR throw std::exception{};
#define ERROR abort();

namespace generator {

namespace {
	const size_t START_RULES = 5;
	const size_t START_RULES_5 = 21;
	const std::vector<size_t> RULES_56{ 18,19 };
	const size_t START_RULES_7 = 31;
	const std::vector<size_t> RULES_78{ 28,29 };
	const size_t START_RULES_9 = 41;
	const std::vector<size_t> RULES_9A{ 38,39 };
}

const Directions directions2 {
	{Dir::forward, Dir::forward},
	{Dir::forward, Dir::backward},
};

const Directions directions3 {
	{Dir::forward, Dir::forward, Dir::forward},
	{Dir::forward, Dir::forward, Dir::backward},
	{Dir::forward, Dir::backward, Dir::forward},
	{Dir::backward, Dir::forward, Dir::forward},
};

const Directions directions4 {
	{Dir::forward, Dir::forward, Dir::forward, Dir::forward},
	{Dir::backward, Dir::forward, Dir::forward, Dir::backward},
	{Dir::backward, Dir::forward, Dir::backward, Dir::forward},
	{Dir::forward, Dir::forward, Dir::forward, Dir::forward},
	{Dir::forward, Dir::backward, Dir::forward, Dir::backward},
};

Direction getRandomDirections(unsigned size) {
	Directions ds[]{ directions2, directions3, directions4 };
	Directions& d{ ds[size - 2] };
	return d[Block::rnd() % d.size()];
}

void VerticalForm::generate() {
	style = Block::rnd() % 3;
	switch (style) {
	case 0: generate0(); break;
	case 1: generate1(); break;
	case 2: generate2(); break;
	}
}

void VerticalForm::generate0() {
	unsigned p{ Block::rnd() % 3 };
	p0 = 8 - p;
	p1 = Block::rnd() % (p + 1);
	p2 = 0;
}

void VerticalForm::generate1() {
	unsigned p{ Block::rnd() % 6 };
	switch (p)
	{
	case 0: p0 = 4; p1 = 6; p2 = 0; break;
	case 1: p0 = 6; p1 = 5; p2 = 0; break;
	case 2: p0 = 4; p1 = 5; p2 = 0; break;
	case 3: p0 = 6; p1 = 5; p2 = 0; break;
	case 4: p0 = 4; p1 = 4; p2 = 1; break;
	case 5: p0 = 4; p1 = 5; p2 = 1; break;
	}
}

void VerticalForm::generate2() {
	unsigned p{ Block::rnd() % 3 };
	p2 = 0;
	p3 = 0;
	switch (p) {
	case 0: p0 = 5; p1 = 3; break;
	case 1: p0 = 3; p1 = 5; break;
	case 2: p0 = 4; p1 = 4; break;
	}
	unsigned c = 0;
	if (Block::rnd() % 2) {
		p0--;
		c++;
	}
	if (Block::rnd() % 2) {
		p1--;
		c++;
	}
	if (c == 0) {
		return;
	}
	if (c == 1) {
		unsigned v{ Block::rnd() % 3 };
		switch (v)
		{
		case 0: break;
		case 1: p2 = 1; break;
		case 2: p3 = 1; break;
		}
	}
	if (c == 2) {
		unsigned v{ Block::rnd() % 6 };
		switch (v)
		{
		case 0: p2 = 0; p3 = 0; break;
		case 1: p2 = 0; p3 = 1; break;
		case 2: p2 = 0; p3 = 2; break;
		case 3: p2 = 1; p3 = 0; break;
		case 4: p2 = 1; p3 = 1; break;
		case 5: p2 = 2; p3 = 0; break;
		}
	}
}

bool VerticalForm::empty() const {
	return ((p0 == 0) && (p1 == 0) && (p2 == 0) && (p3 == 0));
}

void HorizotalForm::generate() {
	p1 = 0;
	p2 = 0;
	unsigned p{ Block::rnd() % 2 };
	if (p == 0) {
		p0 = 4;
		return;
	}
	p0 = 3;
	p1 = Block::rnd() % 2;
	if (p1 == 1) {
		p2 = Block::rnd() % 2;
	}
}

unsigned VerticalForm::nBlocks() const {
	return style + 2;
}

void VerticalForm::generateDirection(unsigned columns) {
	directions.clear();
	if (columns == 1) {
		directions.push_back(getRandomDirections(nBlocks()));
		directions.push_back(getRandomDirections(nBlocks()));
	} else {
		unsigned p{ Block::rnd() % 2 == 0 ? 1 : columns };
		directions.clear();
		for (unsigned i = 0; i < p; i++) {
			directions.push_back(getRandomDirections(nBlocks()));
		}
	}
}

void VerticalForm::generateBlockIDs() {
	hs.resize(2);
	IDs.resize(nBlocks());
	blockTypes = Block::rnd() % 2 + 1;
	switch (style) {
	case 0:
		IDs[0] = 0;
		IDs[1] = blockTypes - 1;
		hs[0] = p0;
		hs[1] = p0;
		break;
	case 1:
		IDs[0] = 0;
		IDs[2] = 0;
		if ((p0 != p1) || (blockTypes == 2)) {
			IDs[1] = 1;
			blockTypes = 2;
			hs[0] = p1;
			hs[1] = p0;
		} else {
			blockTypes = 1;
			IDs[1] = 0;
			hs[0] = p0;
		}
		break;
	case 2:
		if (p0 == p1) {
			if (blockTypes == 2) {
				unsigned p{Block::rnd()%3};
				switch (p) {
				case 0:
					IDs[0] = 0;
					IDs[1] = 1;
					IDs[2] = 1;
					IDs[3] = 0;
					break;
				case 1:
					IDs[0] = 0;
					IDs[1] = 1;
					IDs[2] = 0;
					IDs[3] = 1;
					break;
				case 2:
					IDs[0] = 0;
					IDs[1] = 0;
					IDs[2] = 1;
					IDs[3] = 1;
					break;
				}
				hs[0] = p0;
				hs[1] = p1;
			} else {
				IDs[0] = 0;
				IDs[1] = 0;
				IDs[2] = 0;
				IDs[3] = 0;
				hs[0] = p0;
			}
		} else {
			blockTypes = 2;
			IDs[0] = 0;
			IDs[1] = 1;
			IDs[2] = 1;
			IDs[3] = 0;
			hs[0] = p1;
			hs[1] = p0;
		}
		break;
	}
	hs.resize(blockTypes);
}

void VerticalForm::appendIDs(unsigned add) {
	for (size_t i = 0; i < IDs.size(); i++) {
		IDs[i] += add;
	}
}

unsigned VerticalForm::getX0(unsigned block) {
	unsigned p{ 0 };
	switch (style)
	{
	case 0:
		if (block == 0) return 8 - p0 - p1;
		if (block == 1) return 8 + p1;
		ERROR;
		break;
	case 1:
		p = p0 + p1 * 2 + p2 * 2;
		p = (16 - p) / 2;
		if (block == 0) return p;
		if (block == 1) return p + p1 + p2;
		if (block == 2) return p + p1 + p0 + p2 * 2;
		ERROR;
		break;
	case 2:
		p = (p0 + p1) * 2 + (p2 + p3) * 2;
		p = (16 - p) / 2;
		if (block == 0) return p;
		if (block == 1) return p + p1 + p2;
		if (block == 2) return p + p1 + p2 + p0 + p3 + p3;
		if (block == 3) return p + p1 + p2 + p0 + p3 + p3 + p0 + p2;
		ERROR;
		break;
	}
	return 0;
}

std::vector<unsigned> VerticalForm::getX() {
	std::vector<unsigned> result;
	for (unsigned i = 0; i < nBlocks(); i++) {
		result.push_back(getX0(i));
	}
	return result;
}

bool HorizotalForm::needAdditionalVertical() const {
	return p2 == 1;
}

bool HorizotalForm::empty() const {
	return ((p0 == 0) && (p1 == 0) && (p2 == 0));
}

unsigned HorizotalForm::nBlocks() const {
	return 2;
}

void HorizotalForm::generateDirection(unsigned rows) {
	unsigned p{ Block::rnd() % 2 == 0 ? 1 : nBlocks() };
	directions.clear();
	for (unsigned i = 0; i < p; i++) {
		directions.push_back(getRandomDirections(rows));
	}
}

unsigned HorizotalForm::ws() {
	return p0;
}

unsigned HorizotalForm::getY0(unsigned block) {
	unsigned p{(8 - (p0 + p1) * 2) / 2};
	if (block == 0) return p;
	if (block == 1) return p + p0 + p1 + p1;
	ERROR;
}

std::vector<unsigned> HorizotalForm::getY() {
	std::vector<unsigned> yVector;
	for (unsigned i = 0; i < nBlocks(); i++) {
		yVector.push_back(getY0(i));
	}
	return yVector;
}

Generator::Generator()
	: result{8,16}
	, rules_cache{ [this](const levels::PageKey& key) {return this->generateRules(key); } }
{
}

void Generator::generate(float colors) {
	do {
		makeVerticalForm();
		makeHorisontalForm();
		makeAdditionalVerticalForm();
		generateVerticalDirection();
		generateHorizontalDirection();
		generateAdditionalVerticalDirection();
		generateBlocks();
		fillBlocks();
		fillTiles(colors);
	} while (result.fail());
}

const Block& Generator::getResult() const {
	return result;
}

void Generator::makeVerticalForm() {
	verticalForm.generate();
}

void Generator::makeHorisontalForm() {
	horizonalForm.generate();
}

void Generator::makeAdditionalVerticalForm() {
	if (horizonalForm.needAdditionalVertical()) {
		additionalVerticalForm.generate();
	}
	else {
		additionalVerticalForm = VerticalForm{};
	}
}

void Generator::generateVerticalDirection() {
	verticalForm.generateDirection(horizonalForm.nBlocks());
}

void Generator::generateHorizontalDirection() {
	horizonalForm.generateDirection(verticalForm.nBlocks());
}

void Generator::generateAdditionalVerticalDirection() {
	if (!additionalVerticalForm.empty()) {
		additionalVerticalForm.generateDirection(1);
	}
}

void Generator::generateBlocks() {
	verticalForm.generateBlockIDs();
	blockRecords.clear();
	for (unsigned iy = 0; iy < verticalForm.nBlocks(); iy++) {
		for (unsigned ix = 0; ix < horizonalForm.nBlocks(); ix++) {
			blockRecords.push_back(BlockRecord{
				verticalForm.IDs[iy],
				horizonalForm.getY0(ix),
				verticalForm.getX0(iy),
				horizonalForm.directions[ix % horizonalForm.directions.size()][iy],
				verticalForm.directions[iy % verticalForm.directions.size()][ix]});
		}
	}
	blockSizes.clear();
	for (unsigned i = 0; i < verticalForm.blockTypes; i++) {
		blockSizes.push_back(BlockSize{ horizonalForm.ws(), verticalForm.hs[i] });
	}
	if (additionalVerticalForm.empty()) return;
	additionalVerticalForm.generateBlockIDs();
	additionalVerticalForm.appendIDs(verticalForm.blockTypes);
	for (unsigned iy = 0; iy < additionalVerticalForm.nBlocks(); iy++) {
		blockRecords.push_back(BlockRecord{
			additionalVerticalForm.IDs[iy],
			3,
			additionalVerticalForm.getX0(iy),
			additionalVerticalForm.directions[0][iy],
			additionalVerticalForm.directions[1][iy] });
	}
	for (unsigned i = 0; i < additionalVerticalForm.blockTypes; i++) {
		blockSizes.push_back(BlockSize{ 2, additionalVerticalForm.hs[i] });
	}
}

void Generator::fillBlocks() {
	std::vector<Block> blocks;
	for (size_t i = 0; i < blockSizes.size(); i++) {
		blocks.push_back(Block{ blockSizes[i].w, blockSizes[i].h });
		blocks[i].fill(Block::rnd()%101);
	}
	result.clear();
	for (auto b : blockRecords) {
		result.put(blocks[b.id], b.x, b.y, b.horizontal, b.vertical);
	}
}

void Generator::fillTiles(float colors) {
	std::vector<Point> points;
	for (unsigned ix = 0; ix < result.w(); ix++) {
		for (unsigned iy = 2; iy < result.h() - 2; iy++) {
			if (result.getArea()[ix][iy] != 0) {
				points.push_back(Point{ ix, iy });
			}
		}
	}
	const unsigned stepSize = (unsigned)floor(((float)points.size() / 2) / colors);
	unsigned currentColor = 1;
	unsigned currentStep = 0;
	if (points.size() % 2) points.pop_back();
	result.clear();
	while (!points.empty()) {
		unsigned id1 = Block::rnd() % points.size();
		result.putColor(points[id1], currentColor);
		points.erase(points.begin() + id1);

		unsigned id2 = Block::rnd() % points.size();
		result.putColor(points[id2], currentColor);
		points.erase(points.begin() + id2);

		currentStep++;
		if (currentStep >= stepSize) {
			currentColor++;
			currentStep = 0;
		}
	}
}

char unsigned_to_rules(size_t r) {
	if (r < 10)
		return static_cast<char>(r) + '0';
	else
		return static_cast<char>(r) + 'a' - 10;
}

std::string Generator::getRules(const levels::PageKey& key) const {
	return rules_cache.get(key);
}

void Generator::clear_5(std::string& levels, const levels::PageKey& key, size_t n) const {
	size_t seed{ getDefinedRandom(key) };
	for (size_t i = 0; i < n; i++) {
		size_t base_id = seed % (levels.size() - i);
		seed /= (levels.size() - i);
		while ((levels[base_id] == '0')&&(base_id<levels.size())) base_id++;
		if (base_id < levels.size()) levels[base_id] = '0';
	}
}

std::string Generator::generateRules(const levels::PageKey& key) const {
	std::string rules(static_cast<size_t>(common::Rules::PageSize), '0');

	if ((key.levelType == levels::LevelType::Regular) && (key.page == 0)) {
		for (size_t i = START_RULES-1; i < common::Rules::PageSize; i++) {
			const auto seed{ getDefinedRandom({ key.levelType, i }) };
			rules[i] = AVAILABLE_RULES_5[1+(seed%(AVAILABLE_RULES_5.size()-1))];
		}
		clear_5(rules, key, 3);
		std::string S56{ "56" };
		for (size_t i = 0; i < RULES_56.size(); i++) {
			rules[RULES_56[i] - 1] = S56[i % S56.size()];
		}
	}
	else
	if ((key.levelType == levels::LevelType::Regular) && (key.page == 1)) {
		for (size_t i = START_RULES_5 - 1; i < START_RULES_7; i++) {
			const auto seed{ getDefinedRandom({ key.levelType, i }) };
			rules[i - common::Rules::PageSize] = AVAILABLE_RULES_7[1 + (seed % (AVAILABLE_RULES_7.size() - 1))];
		}
		for (size_t i = START_RULES_7 - 1; i < START_RULES_9 - 1; i++) {
			const auto seed{ getDefinedRandom({ key.levelType, i }) };
			rules[i - common::Rules::PageSize] = AVAILABLE_RULES_9[1 + (seed % (AVAILABLE_RULES_9.size() - 1))];
		}
		clear_5(rules, key, 6);
		std::string S78{ "78" };
		for (size_t i = 0; i < RULES_78.size(); i++) {
			rules[RULES_78[i] - common::Rules::PageSize - 1] = S78[i % S78.size()];
		}
		std::string S9A{ "9a" };
		for (size_t i = 0; i < RULES_9A.size(); i++) {
			rules[RULES_9A[i] - common::Rules::PageSize - 1] = S9A[i % S9A.size()];
		}
	}
	else
	{
		for (size_t i = 0; i < common::Rules::PageSize; i++) {
			const auto seed{ getDefinedRandom({ key.levelType, key.page * common::Rules::PageSize + i }) };
				rules[i] = AVAILABLE_RULES[1 + (seed % (AVAILABLE_RULES.size() - 1))];
		}
		clear_5(rules, key);
	}
	return rules;
}

size_t Generator::getDefinedRandom(const levels::PageKey& key) const {
	common::Random::init(static_cast<unsigned>(key.page), static_cast<unsigned>(key.levelType) + 4);
	return common::Random::get();
}

char Generator::getRulesForLevel(levels::LevelType levelType, size_t level) {
	level--;
	size_t page = level / common::Rules::PageSize;
	size_t id = level % common::Rules::PageSize;
	std::string rules = getRules(levels::PageKey{ levelType , page });
	return rules[id];
}

}
