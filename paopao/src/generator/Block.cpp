#include "generator/Block.h"
#include "common/Random.h"

namespace generator {

void Block::drop_rnd() {
	rnd = common::Random::get;
}

std::function<unsigned()> Block::rnd{ common::Random::get };

Block::Block(unsigned w, unsigned h) {
	resize(w, h);
}

Block::Block(const BlockSize& blockSize) {
	resize(blockSize.w, blockSize.h);
}

void Block::resize(unsigned w, unsigned h) {
	area.resize(w);
	for (size_t i = 0; i < w; i++) {
		area[i].resize(h);
	}
}

void Block::clear() {
	for (size_t i = 0; i < area.size(); i++) {
		for (size_t j = 0; j < area[i].size(); j++) {
			area[i][j] = 0;
		}
	}
}

void Block::putColor(const Point & point, unsigned color) {
	area[point.x][point.y] = color;
}

bool Block::fail() const {
	if (w() != 8) return false;
	if (h() != 16) return false;
	unsigned top{ 0 }, bottom{ 0 };
	for (unsigned x = 0; x < w(); x++) {
		for (unsigned y = 0; y < h()/2; y++) {
			if (area[x][y] != 0) top++;
			if (area[x][y+8] != 0) bottom++;
		}
	}
	if (top < 8) return true;
	if (bottom < 8) return true;
	if ((top + bottom) < 20) return true;
	return false;
}

unsigned Block::points() const {
	unsigned count{ 0 };
	for (unsigned x = 0; x < w(); x++) {
		for (unsigned y = 0; y < h(); y++) {
			if (area[x][y] != 0) count++;
		}
	}
	return count;
}

void Block::fill(unsigned p_i) {
	float p = ((float)p_i) / 100.0f;
	constexpr unsigned T = 256;
	p -= 0.2f;
	p /= 0.7f;
	unsigned counter{ 0 };
	if (p > 1) p = 1;
	for (size_t ix = 0; ix < w(); ix++) {
		for (size_t iy = 0; iy < h(); iy++) {
			if ((rnd() % T) < (p*T)) {
				area[ix][iy] = 1;
				counter++;
			} else {
				area[ix][iy] = 0;
			}
		}
	}
	if (counter % 2 == 1) {
		unsigned ix;
		unsigned iy;
		do {
			ix = Block::rnd() % w();
			iy = Block::rnd() % h();
		} while (area[ix][iy] == 0);
		area[ix][iy] = 0;
	}
}

void Block::put(const Block& block, unsigned x, unsigned y, Dir horizontal, Dir vertical) {
	for (unsigned ix = 0; ix < block.w(); ix++) {
		for (unsigned iy = 0; iy < block.h(); iy++) {
			auto px = ((horizontal == Dir::forward) ? (x + ix) : (x + block.w() - ix - 1));
			auto py = ((vertical == Dir::forward) ? (y + iy) : (y + block.h() - iy - 1));
			area[px][py] = block.area[ix][iy];
		}
	}
}

unsigned Block::w() const {
	return static_cast<unsigned>(area.size());
}

unsigned Block::h() const {
	if (w() == 0) return 0;
	return static_cast<unsigned>(area[0].size());
}


const Area& Block::getArea() const {
	return area;
}

}
