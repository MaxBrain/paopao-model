// PaopaoModel.cpp
// Extension lib defines
#define MODULE_NAME_PAOPAO "paopaomodel"

// include the Defold SDK
#include <dmsdk/sdk.h>
#include <string>
#include "mediator/ConsoleOutput.h"
#include "component/Components.h"
#include "game/Game.h"
#include "game/State.h"
#include "i18n/L10n.h"
#include "config/Config.h"
#include "input/input.h"
#include "notify/notify.h"
#include "callback.h"

dmScript::LuaCallbackInfo* defUtoLua = 0x0;
std::vector<Parameters> parametersPack;

static int SetCallback(lua_State* L) {
    SetLuaCallback(L, 1);
    return 0;
}

static int UserAction(lua_State* L) {
    Parameters parameters;
    int n = lua_gettop(L);
    for (int i=1; i<=n; i++) {
        switch (lua_type(L, i)) {
            case LUA_TNUMBER:
                parameters.push_back(static_cast<float>(luaL_checknumber(L, i)));
                break;
            case LUA_TSTRING:
                parameters.push_back(std::string{luaL_checkstring(L, i)});
                break;
        }
    }
    input::input(parameters);
    ConsoleOutput::print(parameters);
    return 0;
}

class DefoldEventSender : public Observer<Parameters> {
public:
    void process(const Parameters& parameters) {
        AddToQueue(parameters);
    }
};

DefoldEventSender defoldEventSender;

static void startGame(const std::string& load_data) {
    printf("Start game\n");
    notifier.addObserver(&defoldEventSender);
    component::game->load(load_data);
}

static int Load(lua_State* L) {
    const std::string load_data{luaL_checkstring(L, 1)};
    startGame(load_data);
    return 0;
}

static int Save(lua_State* L) {
    std::string data = component::game->save();
    lua_pushstring(L, data.c_str());
    return 1;
}

static int LoadI18N(lua_State* L) {
    const std::string file{luaL_checkstring(L, 1)};
    component::l10n->load(file);
    return 0;
}

static int LoadConfig(lua_State* L) {
    const std::string config{luaL_checkstring(L, 1)};
    component::config->load(config);
    return 0;
}

static int Session(lua_State* L) {
    std::string session = component::state->getSession();
    lua_pushstring(L, session.c_str());
    return 1;
}

static int CurrentTick(lua_State* L) {
    size_t current_tick = component::game->current_tick();
    lua_pushnumber(L, current_tick);
    return 1;
}

// Functions exposed to Lua
static const luaL_reg Module_methods[] =
{
    {"user_action", UserAction},
    {"load", Load},
    {"save", Save},
    {"setCallback", SetCallback},
    {"loadI18N", LoadI18N},
    {"loadConfig", LoadConfig},
    {"session", Session},
    {"current_tick", CurrentTick},
    {0, 0}
};

static void LuaInit(lua_State* L) {
    int top = lua_gettop(L);

    luaL_register(L, MODULE_NAME_PAOPAO, Module_methods);

    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}

dmExtension::Result AppInitializePaopaoModel(dmExtension::AppParams* params) {
    return dmExtension::RESULT_OK;
}

dmExtension::Result InitializePaopaoModel(dmExtension::Params* params) {
    // Init Lua
    LuaInit(params->m_L);
    //dmLogInfo("Registered %s Extension\n", MODULE_NAME_PAOPAO);
    Initialize();
    return dmExtension::RESULT_OK;
}

dmExtension::Result AppFinalizePaopaoModel(dmExtension::AppParams* params) {
    return dmExtension::RESULT_OK;
}

dmExtension::Result FinalizePaopaoModel(dmExtension::Params* params) {
    Finalize();
    return dmExtension::RESULT_OK;
}

dmExtension::Result UpdatePaopaoModel(dmExtension::Params* params) {
    CallbackUpdate();
    component::game->tick(1.0f/60);
    return dmExtension::RESULT_OK;
}


// Defold SDK uses a macro for setting up extension entry points:
//
// DM_DECLARE_EXTENSION(symbol, name, app_init, app_final, init, update, on_event, final)

DM_DECLARE_EXTENSION(PaopaoModel, "PaopaoModel", AppInitializePaopaoModel, AppFinalizePaopaoModel, InitializePaopaoModel, UpdatePaopaoModel, 0, FinalizePaopaoModel)

