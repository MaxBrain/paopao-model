#include "common/Random.h"

namespace common {

struct seed_t
{
    unsigned x = 1;
    unsigned y = 11111;
    unsigned z = 22222;
    unsigned w = 33333;
};

static seed_t seed;

void Random::init(unsigned v1, unsigned v2, unsigned v3, unsigned v4) {
    seed.x = v1;
    seed.y = v3;
    seed.z = v4;
    seed.w = v2;
}

unsigned Random::get()
{
    unsigned t = seed.x ^ (seed.x << 11);
    seed.x = seed.y;
    seed.y = seed.z;
    seed.z = seed.w;
    seed.w = (seed.w ^ (seed.w >> 19)) ^ (t ^ (t >> 8));
    return seed.w;
}

}
