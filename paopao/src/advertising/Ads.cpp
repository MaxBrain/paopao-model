#include "advertising/Ads.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "config/Config.h"

namespace advertising {

bool Ads::isEnabled() const {
	return enabled;
}

void Ads::setEnabled(bool enabled_) {
	enabled = enabled_;
}

void Ads::setRewardedAdsLoaded() {
	rewardedAdsLoaded = true;
}

void Ads::setInterscenialAdsLoaded() {
	interscenialAdsLoaded = true;
}

void Ads::showRewardedAds(std::function<void()> complete_, std::function<void()> cancel_) {
	if (component::config->ads == config::Ads::None) {
		LOG_ERROR("Call Ads::showRewardedAds with disabled ads in config");
		return;
	}
	if (!rewardedAdsLoaded) {
		LOG_ERROR("Try to show non loaded rewarded Ads");
		return;
	}
	if (!enabled) {
		LOG_ERROR("showRewardedAds: Ads not enabled");
		return;
	}
	complete = complete_;
	cancel = cancel_;
	rewardedAdsLoaded = false;
	notify::ads::rewarded();
}

void Ads::showInterscenialAds() {
	if (component::config->ads == config::Ads::None) {
		LOG_ERROR("Call Ads::showInterscenialAds with disabled ads in config");
		return;
	}
	if (!interscenialAdsLoaded) {
		LOG_ERROR("Try to show non loaded interscenial Ads");
		return;
	}
	if (!enabled) {
		LOG_ERROR("showInterscenialAds: Ads not enabled");
		return;
	}
	interscenialAdsLoaded = false;
	notify::ads::interscenial();
}

void Ads::call_rewarded_complete() {
	if (complete) complete();
	complete = nullptr;
	cancel = nullptr;
}

void Ads::call_rewarded_cancel() {
	if (cancel) cancel();
	complete = nullptr;
	cancel = nullptr;
}

bool Ads::canShowInterscenialAds() const {
	if (!isEnabled()) return false;
	if (component::config->ads == config::Ads::None) return false;
	return interscenialAdsLoaded;
}

bool Ads::canShowRewardedAds() const {
	if (!isEnabled()) return false;
	if (component::config->ads == config::Ads::None) return false;
	return rewardedAdsLoaded;
}

}
