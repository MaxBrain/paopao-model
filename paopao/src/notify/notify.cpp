#include "notify/notify.h"
#include "3rdParty/json.hpp"

Subject<Parameters> notifier;

namespace notify {
namespace controller {

void activate() {
    notifier.notify({"controller", "activate"});
}

void activate(int value) {
    notifier.notify({"controller", "activate", value});
}

void activate(int value1, int value2) {
    notifier.notify({"controller", "activate", value1, value2});
}

void activate(float value) {
    notifier.notify({"controller", "activate", value});
}

void activate(const std::string& value) {
    notifier.notify({"controller", "activate", value});
}

void activate(const nlohmann::json& value) {
    notifier.notify({"controller", "activate", value.dump()});
}


void deactivate() {
    notifier.notify({"controller", "deactivate"});
}

void deactivate(int value) {
    notifier.notify({"controller", "deactivate", value});
}

void deactivate(int value1, int value2) {
    notifier.notify({"controller", "deactivate", value1, value2});
}

void deactivate(float value) {
    notifier.notify({"controller", "deactivate", value});
}

void deactivate(const std::string& value) {
    notifier.notify({"controller", "deactivate", value});
}

void deactivate(const nlohmann::json& value) {
    notifier.notify({"controller", "deactivate", value.dump()});
}

}
namespace state {

void newlevel() {
    notifier.notify({"state", "newlevel"});
}

void newlevel(int value) {
    notifier.notify({"state", "newlevel", value});
}

void newlevel(int value1, int value2) {
    notifier.notify({"state", "newlevel", value1, value2});
}

void newlevel(float value) {
    notifier.notify({"state", "newlevel", value});
}

void newlevel(const std::string& value) {
    notifier.notify({"state", "newlevel", value});
}

void newlevel(const nlohmann::json& value) {
    notifier.notify({"state", "newlevel", value.dump()});
}


void clear_arrays() {
    notifier.notify({"state", "clear_arrays"});
}

void clear_arrays(int value) {
    notifier.notify({"state", "clear_arrays", value});
}

void clear_arrays(int value1, int value2) {
    notifier.notify({"state", "clear_arrays", value1, value2});
}

void clear_arrays(float value) {
    notifier.notify({"state", "clear_arrays", value});
}

void clear_arrays(const std::string& value) {
    notifier.notify({"state", "clear_arrays", value});
}

void clear_arrays(const nlohmann::json& value) {
    notifier.notify({"state", "clear_arrays", value.dump()});
}

}
namespace window {

void win() {
    notifier.notify({"window", "win"});
}

void win(int value) {
    notifier.notify({"window", "win", value});
}

void win(int value1, int value2) {
    notifier.notify({"window", "win", value1, value2});
}

void win(float value) {
    notifier.notify({"window", "win", value});
}

void win(const std::string& value) {
    notifier.notify({"window", "win", value});
}

void win(const nlohmann::json& value) {
    notifier.notify({"window", "win", value.dump()});
}

}
namespace element {

void new_() {
    notifier.notify({"element", "new"});
}

void new_(int value) {
    notifier.notify({"element", "new", value});
}

void new_(int value1, int value2) {
    notifier.notify({"element", "new", value1, value2});
}

void new_(float value) {
    notifier.notify({"element", "new", value});
}

void new_(const std::string& value) {
    notifier.notify({"element", "new", value});
}

void new_(const nlohmann::json& value) {
    notifier.notify({"element", "new", value.dump()});
}


void activate() {
    notifier.notify({"element", "activate"});
}

void activate(int value) {
    notifier.notify({"element", "activate", value});
}

void activate(int value1, int value2) {
    notifier.notify({"element", "activate", value1, value2});
}

void activate(float value) {
    notifier.notify({"element", "activate", value});
}

void activate(const std::string& value) {
    notifier.notify({"element", "activate", value});
}

void activate(const nlohmann::json& value) {
    notifier.notify({"element", "activate", value.dump()});
}


void deactivate() {
    notifier.notify({"element", "deactivate"});
}

void deactivate(int value) {
    notifier.notify({"element", "deactivate", value});
}

void deactivate(int value1, int value2) {
    notifier.notify({"element", "deactivate", value1, value2});
}

void deactivate(float value) {
    notifier.notify({"element", "deactivate", value});
}

void deactivate(const std::string& value) {
    notifier.notify({"element", "deactivate", value});
}

void deactivate(const nlohmann::json& value) {
    notifier.notify({"element", "deactivate", value.dump()});
}


void move() {
    notifier.notify({"element", "move"});
}

void move(int value) {
    notifier.notify({"element", "move", value});
}

void move(int value1, int value2) {
    notifier.notify({"element", "move", value1, value2});
}

void move(float value) {
    notifier.notify({"element", "move", value});
}

void move(const std::string& value) {
    notifier.notify({"element", "move", value});
}

void move(const nlohmann::json& value) {
    notifier.notify({"element", "move", value.dump()});
}


void randommove() {
    notifier.notify({"element", "randommove"});
}

void randommove(int value) {
    notifier.notify({"element", "randommove", value});
}

void randommove(int value1, int value2) {
    notifier.notify({"element", "randommove", value1, value2});
}

void randommove(float value) {
    notifier.notify({"element", "randommove", value});
}

void randommove(const std::string& value) {
    notifier.notify({"element", "randommove", value});
}

void randommove(const nlohmann::json& value) {
    notifier.notify({"element", "randommove", value.dump()});
}


void drop() {
    notifier.notify({"element", "drop"});
}

void drop(int value) {
    notifier.notify({"element", "drop", value});
}

void drop(int value1, int value2) {
    notifier.notify({"element", "drop", value1, value2});
}

void drop(float value) {
    notifier.notify({"element", "drop", value});
}

void drop(const std::string& value) {
    notifier.notify({"element", "drop", value});
}

void drop(const nlohmann::json& value) {
    notifier.notify({"element", "drop", value.dump()});
}


void putweb() {
    notifier.notify({"element", "putweb"});
}

void putweb(int value) {
    notifier.notify({"element", "putweb", value});
}

void putweb(int value1, int value2) {
    notifier.notify({"element", "putweb", value1, value2});
}

void putweb(float value) {
    notifier.notify({"element", "putweb", value});
}

void putweb(const std::string& value) {
    notifier.notify({"element", "putweb", value});
}

void putweb(const nlohmann::json& value) {
    notifier.notify({"element", "putweb", value.dump()});
}


void removeweb() {
    notifier.notify({"element", "removeweb"});
}

void removeweb(int value) {
    notifier.notify({"element", "removeweb", value});
}

void removeweb(int value1, int value2) {
    notifier.notify({"element", "removeweb", value1, value2});
}

void removeweb(float value) {
    notifier.notify({"element", "removeweb", value});
}

void removeweb(const std::string& value) {
    notifier.notify({"element", "removeweb", value});
}

void removeweb(const nlohmann::json& value) {
    notifier.notify({"element", "removeweb", value.dump()});
}


void putice() {
    notifier.notify({"element", "putice"});
}

void putice(int value) {
    notifier.notify({"element", "putice", value});
}

void putice(int value1, int value2) {
    notifier.notify({"element", "putice", value1, value2});
}

void putice(float value) {
    notifier.notify({"element", "putice", value});
}

void putice(const std::string& value) {
    notifier.notify({"element", "putice", value});
}

void putice(const nlohmann::json& value) {
    notifier.notify({"element", "putice", value.dump()});
}


void removeice() {
    notifier.notify({"element", "removeice"});
}

void removeice(int value) {
    notifier.notify({"element", "removeice", value});
}

void removeice(int value1, int value2) {
    notifier.notify({"element", "removeice", value1, value2});
}

void removeice(float value) {
    notifier.notify({"element", "removeice", value});
}

void removeice(const std::string& value) {
    notifier.notify({"element", "removeice", value});
}

void removeice(const nlohmann::json& value) {
    notifier.notify({"element", "removeice", value.dump()});
}


void hint() {
    notifier.notify({"element", "hint"});
}

void hint(int value) {
    notifier.notify({"element", "hint", value});
}

void hint(int value1, int value2) {
    notifier.notify({"element", "hint", value1, value2});
}

void hint(float value) {
    notifier.notify({"element", "hint", value});
}

void hint(const std::string& value) {
    notifier.notify({"element", "hint", value});
}

void hint(const nlohmann::json& value) {
    notifier.notify({"element", "hint", value.dump()});
}


void dark() {
    notifier.notify({"element", "dark"});
}

void dark(int value) {
    notifier.notify({"element", "dark", value});
}

void dark(int value1, int value2) {
    notifier.notify({"element", "dark", value1, value2});
}

void dark(float value) {
    notifier.notify({"element", "dark", value});
}

void dark(const std::string& value) {
    notifier.notify({"element", "dark", value});
}

void dark(const nlohmann::json& value) {
    notifier.notify({"element", "dark", value.dump()});
}


void light() {
    notifier.notify({"element", "light"});
}

void light(int value) {
    notifier.notify({"element", "light", value});
}

void light(int value1, int value2) {
    notifier.notify({"element", "light", value1, value2});
}

void light(float value) {
    notifier.notify({"element", "light", value});
}

void light(const std::string& value) {
    notifier.notify({"element", "light", value});
}

void light(const nlohmann::json& value) {
    notifier.notify({"element", "light", value.dump()});
}

}
namespace path {

void show() {
    notifier.notify({"path", "show"});
}

void show(int value) {
    notifier.notify({"path", "show", value});
}

void show(int value1, int value2) {
    notifier.notify({"path", "show", value1, value2});
}

void show(float value) {
    notifier.notify({"path", "show", value});
}

void show(const std::string& value) {
    notifier.notify({"path", "show", value});
}

void show(const nlohmann::json& value) {
    notifier.notify({"path", "show", value.dump()});
}


void hide() {
    notifier.notify({"path", "hide"});
}

void hide(int value) {
    notifier.notify({"path", "hide", value});
}

void hide(int value1, int value2) {
    notifier.notify({"path", "hide", value1, value2});
}

void hide(float value) {
    notifier.notify({"path", "hide", value});
}

void hide(const std::string& value) {
    notifier.notify({"path", "hide", value});
}

void hide(const nlohmann::json& value) {
    notifier.notify({"path", "hide", value.dump()});
}

}
namespace addition {

void new_() {
    notifier.notify({"addition", "new"});
}

void new_(int value) {
    notifier.notify({"addition", "new", value});
}

void new_(int value1, int value2) {
    notifier.notify({"addition", "new", value1, value2});
}

void new_(float value) {
    notifier.notify({"addition", "new", value});
}

void new_(const std::string& value) {
    notifier.notify({"addition", "new", value});
}

void new_(const nlohmann::json& value) {
    notifier.notify({"addition", "new", value.dump()});
}

}
namespace tool {

void bomb() {
    notifier.notify({"tool", "bomb"});
}

void bomb(int value) {
    notifier.notify({"tool", "bomb", value});
}

void bomb(int value1, int value2) {
    notifier.notify({"tool", "bomb", value1, value2});
}

void bomb(float value) {
    notifier.notify({"tool", "bomb", value});
}

void bomb(const std::string& value) {
    notifier.notify({"tool", "bomb", value});
}

void bomb(const nlohmann::json& value) {
    notifier.notify({"tool", "bomb", value.dump()});
}


void disable() {
    notifier.notify({"tool", "disable"});
}

void disable(int value) {
    notifier.notify({"tool", "disable", value});
}

void disable(int value1, int value2) {
    notifier.notify({"tool", "disable", value1, value2});
}

void disable(float value) {
    notifier.notify({"tool", "disable", value});
}

void disable(const std::string& value) {
    notifier.notify({"tool", "disable", value});
}

void disable(const nlohmann::json& value) {
    notifier.notify({"tool", "disable", value.dump()});
}


void hint() {
    notifier.notify({"tool", "hint"});
}

void hint(int value) {
    notifier.notify({"tool", "hint", value});
}

void hint(int value1, int value2) {
    notifier.notify({"tool", "hint", value1, value2});
}

void hint(float value) {
    notifier.notify({"tool", "hint", value});
}

void hint(const std::string& value) {
    notifier.notify({"tool", "hint", value});
}

void hint(const nlohmann::json& value) {
    notifier.notify({"tool", "hint", value.dump()});
}


void hit() {
    notifier.notify({"tool", "hit"});
}

void hit(int value) {
    notifier.notify({"tool", "hit", value});
}

void hit(int value1, int value2) {
    notifier.notify({"tool", "hit", value1, value2});
}

void hit(float value) {
    notifier.notify({"tool", "hit", value});
}

void hit(const std::string& value) {
    notifier.notify({"tool", "hit", value});
}

void hit(const nlohmann::json& value) {
    notifier.notify({"tool", "hit", value.dump()});
}


void mix() {
    notifier.notify({"tool", "mix"});
}

void mix(int value) {
    notifier.notify({"tool", "mix", value});
}

void mix(int value1, int value2) {
    notifier.notify({"tool", "mix", value1, value2});
}

void mix(float value) {
    notifier.notify({"tool", "mix", value});
}

void mix(const std::string& value) {
    notifier.notify({"tool", "mix", value});
}

void mix(const nlohmann::json& value) {
    notifier.notify({"tool", "mix", value.dump()});
}


void plus() {
    notifier.notify({"tool", "plus"});
}

void plus(int value) {
    notifier.notify({"tool", "plus", value});
}

void plus(int value1, int value2) {
    notifier.notify({"tool", "plus", value1, value2});
}

void plus(float value) {
    notifier.notify({"tool", "plus", value});
}

void plus(const std::string& value) {
    notifier.notify({"tool", "plus", value});
}

void plus(const nlohmann::json& value) {
    notifier.notify({"tool", "plus", value.dump()});
}

}
namespace gui {
namespace popup {

void about() {
    notifier.notify({"gui", "popup", "about"});
}

void about(int value) {
    notifier.notify({"gui", "popup", "about", value});
}

void about(int value1, int value2) {
    notifier.notify({"gui", "popup", "about", value1, value2});
}

void about(float value) {
    notifier.notify({"gui", "popup", "about", value});
}

void about(const std::string& value) {
    notifier.notify({"gui", "popup", "about", value});
}

void about(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "about", value.dump()});
}


void ads_for_hint() {
    notifier.notify({"gui", "popup", "ads_for_hint"});
}

void ads_for_hint(int value) {
    notifier.notify({"gui", "popup", "ads_for_hint", value});
}

void ads_for_hint(int value1, int value2) {
    notifier.notify({"gui", "popup", "ads_for_hint", value1, value2});
}

void ads_for_hint(float value) {
    notifier.notify({"gui", "popup", "ads_for_hint", value});
}

void ads_for_hint(const std::string& value) {
    notifier.notify({"gui", "popup", "ads_for_hint", value});
}

void ads_for_hint(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "ads_for_hint", value.dump()});
}


void adventure() {
    notifier.notify({"gui", "popup", "adventure"});
}

void adventure(int value) {
    notifier.notify({"gui", "popup", "adventure", value});
}

void adventure(int value1, int value2) {
    notifier.notify({"gui", "popup", "adventure", value1, value2});
}

void adventure(float value) {
    notifier.notify({"gui", "popup", "adventure", value});
}

void adventure(const std::string& value) {
    notifier.notify({"gui", "popup", "adventure", value});
}

void adventure(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "adventure", value.dump()});
}


void collection_complete() {
    notifier.notify({"gui", "popup", "collection_complete"});
}

void collection_complete(int value) {
    notifier.notify({"gui", "popup", "collection_complete", value});
}

void collection_complete(int value1, int value2) {
    notifier.notify({"gui", "popup", "collection_complete", value1, value2});
}

void collection_complete(float value) {
    notifier.notify({"gui", "popup", "collection_complete", value});
}

void collection_complete(const std::string& value) {
    notifier.notify({"gui", "popup", "collection_complete", value});
}

void collection_complete(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "collection_complete", value.dump()});
}


void collections() {
    notifier.notify({"gui", "popup", "collections"});
}

void collections(int value) {
    notifier.notify({"gui", "popup", "collections", value});
}

void collections(int value1, int value2) {
    notifier.notify({"gui", "popup", "collections", value1, value2});
}

void collections(float value) {
    notifier.notify({"gui", "popup", "collections", value});
}

void collections(const std::string& value) {
    notifier.notify({"gui", "popup", "collections", value});
}

void collections(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "collections", value.dump()});
}


void daily_bonus() {
    notifier.notify({"gui", "popup", "daily_bonus"});
}

void daily_bonus(int value) {
    notifier.notify({"gui", "popup", "daily_bonus", value});
}

void daily_bonus(int value1, int value2) {
    notifier.notify({"gui", "popup", "daily_bonus", value1, value2});
}

void daily_bonus(float value) {
    notifier.notify({"gui", "popup", "daily_bonus", value});
}

void daily_bonus(const std::string& value) {
    notifier.notify({"gui", "popup", "daily_bonus", value});
}

void daily_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "daily_bonus", value.dump()});
}


void daily_tasks() {
    notifier.notify({"gui", "popup", "daily_tasks"});
}

void daily_tasks(int value) {
    notifier.notify({"gui", "popup", "daily_tasks", value});
}

void daily_tasks(int value1, int value2) {
    notifier.notify({"gui", "popup", "daily_tasks", value1, value2});
}

void daily_tasks(float value) {
    notifier.notify({"gui", "popup", "daily_tasks", value});
}

void daily_tasks(const std::string& value) {
    notifier.notify({"gui", "popup", "daily_tasks", value});
}

void daily_tasks(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "daily_tasks", value.dump()});
}


void game_ui() {
    notifier.notify({"gui", "popup", "game_ui"});
}

void game_ui(int value) {
    notifier.notify({"gui", "popup", "game_ui", value});
}

void game_ui(int value1, int value2) {
    notifier.notify({"gui", "popup", "game_ui", value1, value2});
}

void game_ui(float value) {
    notifier.notify({"gui", "popup", "game_ui", value});
}

void game_ui(const std::string& value) {
    notifier.notify({"gui", "popup", "game_ui", value});
}

void game_ui(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "game_ui", value.dump()});
}


void lose_window() {
    notifier.notify({"gui", "popup", "lose_window"});
}

void lose_window(int value) {
    notifier.notify({"gui", "popup", "lose_window", value});
}

void lose_window(int value1, int value2) {
    notifier.notify({"gui", "popup", "lose_window", value1, value2});
}

void lose_window(float value) {
    notifier.notify({"gui", "popup", "lose_window", value});
}

void lose_window(const std::string& value) {
    notifier.notify({"gui", "popup", "lose_window", value});
}

void lose_window(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "lose_window", value.dump()});
}


void menu() {
    notifier.notify({"gui", "popup", "menu"});
}

void menu(int value) {
    notifier.notify({"gui", "popup", "menu", value});
}

void menu(int value1, int value2) {
    notifier.notify({"gui", "popup", "menu", value1, value2});
}

void menu(float value) {
    notifier.notify({"gui", "popup", "menu", value});
}

void menu(const std::string& value) {
    notifier.notify({"gui", "popup", "menu", value});
}

void menu(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "menu", value.dump()});
}


void new_element() {
    notifier.notify({"gui", "popup", "new_element"});
}

void new_element(int value) {
    notifier.notify({"gui", "popup", "new_element", value});
}

void new_element(int value1, int value2) {
    notifier.notify({"gui", "popup", "new_element", value1, value2});
}

void new_element(float value) {
    notifier.notify({"gui", "popup", "new_element", value});
}

void new_element(const std::string& value) {
    notifier.notify({"gui", "popup", "new_element", value});
}

void new_element(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "new_element", value.dump()});
}


void pause_window() {
    notifier.notify({"gui", "popup", "pause_window"});
}

void pause_window(int value) {
    notifier.notify({"gui", "popup", "pause_window", value});
}

void pause_window(int value1, int value2) {
    notifier.notify({"gui", "popup", "pause_window", value1, value2});
}

void pause_window(float value) {
    notifier.notify({"gui", "popup", "pause_window", value});
}

void pause_window(const std::string& value) {
    notifier.notify({"gui", "popup", "pause_window", value});
}

void pause_window(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "pause_window", value.dump()});
}


void rate_external() {
    notifier.notify({"gui", "popup", "rate_external"});
}

void rate_external(int value) {
    notifier.notify({"gui", "popup", "rate_external", value});
}

void rate_external(int value1, int value2) {
    notifier.notify({"gui", "popup", "rate_external", value1, value2});
}

void rate_external(float value) {
    notifier.notify({"gui", "popup", "rate_external", value});
}

void rate_external(const std::string& value) {
    notifier.notify({"gui", "popup", "rate_external", value});
}

void rate_external(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "rate_external", value.dump()});
}


void rate_internal() {
    notifier.notify({"gui", "popup", "rate_internal"});
}

void rate_internal(int value) {
    notifier.notify({"gui", "popup", "rate_internal", value});
}

void rate_internal(int value1, int value2) {
    notifier.notify({"gui", "popup", "rate_internal", value1, value2});
}

void rate_internal(float value) {
    notifier.notify({"gui", "popup", "rate_internal", value});
}

void rate_internal(const std::string& value) {
    notifier.notify({"gui", "popup", "rate_internal", value});
}

void rate_internal(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "rate_internal", value.dump()});
}


void settings() {
    notifier.notify({"gui", "popup", "settings"});
}

void settings(int value) {
    notifier.notify({"gui", "popup", "settings", value});
}

void settings(int value1, int value2) {
    notifier.notify({"gui", "popup", "settings", value1, value2});
}

void settings(float value) {
    notifier.notify({"gui", "popup", "settings", value});
}

void settings(const std::string& value) {
    notifier.notify({"gui", "popup", "settings", value});
}

void settings(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "settings", value.dump()});
}


void shop() {
    notifier.notify({"gui", "popup", "shop"});
}

void shop(int value) {
    notifier.notify({"gui", "popup", "shop", value});
}

void shop(int value1, int value2) {
    notifier.notify({"gui", "popup", "shop", value1, value2});
}

void shop(float value) {
    notifier.notify({"gui", "popup", "shop", value});
}

void shop(const std::string& value) {
    notifier.notify({"gui", "popup", "shop", value});
}

void shop(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "shop", value.dump()});
}


void term_of_conditions() {
    notifier.notify({"gui", "popup", "term_of_conditions"});
}

void term_of_conditions(int value) {
    notifier.notify({"gui", "popup", "term_of_conditions", value});
}

void term_of_conditions(int value1, int value2) {
    notifier.notify({"gui", "popup", "term_of_conditions", value1, value2});
}

void term_of_conditions(float value) {
    notifier.notify({"gui", "popup", "term_of_conditions", value});
}

void term_of_conditions(const std::string& value) {
    notifier.notify({"gui", "popup", "term_of_conditions", value});
}

void term_of_conditions(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "term_of_conditions", value.dump()});
}


void win_window() {
    notifier.notify({"gui", "popup", "win_window"});
}

void win_window(int value) {
    notifier.notify({"gui", "popup", "win_window", value});
}

void win_window(int value1, int value2) {
    notifier.notify({"gui", "popup", "win_window", value1, value2});
}

void win_window(float value) {
    notifier.notify({"gui", "popup", "win_window", value});
}

void win_window(const std::string& value) {
    notifier.notify({"gui", "popup", "win_window", value});
}

void win_window(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "win_window", value.dump()});
}


void level_rules() {
    notifier.notify({"gui", "popup", "level_rules"});
}

void level_rules(int value) {
    notifier.notify({"gui", "popup", "level_rules", value});
}

void level_rules(int value1, int value2) {
    notifier.notify({"gui", "popup", "level_rules", value1, value2});
}

void level_rules(float value) {
    notifier.notify({"gui", "popup", "level_rules", value});
}

void level_rules(const std::string& value) {
    notifier.notify({"gui", "popup", "level_rules", value});
}

void level_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "level_rules", value.dump()});
}


void suggest_bonus() {
    notifier.notify({"gui", "popup", "suggest_bonus"});
}

void suggest_bonus(int value) {
    notifier.notify({"gui", "popup", "suggest_bonus", value});
}

void suggest_bonus(int value1, int value2) {
    notifier.notify({"gui", "popup", "suggest_bonus", value1, value2});
}

void suggest_bonus(float value) {
    notifier.notify({"gui", "popup", "suggest_bonus", value});
}

void suggest_bonus(const std::string& value) {
    notifier.notify({"gui", "popup", "suggest_bonus", value});
}

void suggest_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "suggest_bonus", value.dump()});
}


void the_rules() {
    notifier.notify({"gui", "popup", "the_rules"});
}

void the_rules(int value) {
    notifier.notify({"gui", "popup", "the_rules", value});
}

void the_rules(int value1, int value2) {
    notifier.notify({"gui", "popup", "the_rules", value1, value2});
}

void the_rules(float value) {
    notifier.notify({"gui", "popup", "the_rules", value});
}

void the_rules(const std::string& value) {
    notifier.notify({"gui", "popup", "the_rules", value});
}

void the_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "popup", "the_rules", value.dump()});
}

}
namespace close {

void about() {
    notifier.notify({"gui", "close", "about"});
}

void about(int value) {
    notifier.notify({"gui", "close", "about", value});
}

void about(int value1, int value2) {
    notifier.notify({"gui", "close", "about", value1, value2});
}

void about(float value) {
    notifier.notify({"gui", "close", "about", value});
}

void about(const std::string& value) {
    notifier.notify({"gui", "close", "about", value});
}

void about(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "about", value.dump()});
}


void ads_for_hint() {
    notifier.notify({"gui", "close", "ads_for_hint"});
}

void ads_for_hint(int value) {
    notifier.notify({"gui", "close", "ads_for_hint", value});
}

void ads_for_hint(int value1, int value2) {
    notifier.notify({"gui", "close", "ads_for_hint", value1, value2});
}

void ads_for_hint(float value) {
    notifier.notify({"gui", "close", "ads_for_hint", value});
}

void ads_for_hint(const std::string& value) {
    notifier.notify({"gui", "close", "ads_for_hint", value});
}

void ads_for_hint(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "ads_for_hint", value.dump()});
}


void adventure() {
    notifier.notify({"gui", "close", "adventure"});
}

void adventure(int value) {
    notifier.notify({"gui", "close", "adventure", value});
}

void adventure(int value1, int value2) {
    notifier.notify({"gui", "close", "adventure", value1, value2});
}

void adventure(float value) {
    notifier.notify({"gui", "close", "adventure", value});
}

void adventure(const std::string& value) {
    notifier.notify({"gui", "close", "adventure", value});
}

void adventure(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "adventure", value.dump()});
}


void collection_complete() {
    notifier.notify({"gui", "close", "collection_complete"});
}

void collection_complete(int value) {
    notifier.notify({"gui", "close", "collection_complete", value});
}

void collection_complete(int value1, int value2) {
    notifier.notify({"gui", "close", "collection_complete", value1, value2});
}

void collection_complete(float value) {
    notifier.notify({"gui", "close", "collection_complete", value});
}

void collection_complete(const std::string& value) {
    notifier.notify({"gui", "close", "collection_complete", value});
}

void collection_complete(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "collection_complete", value.dump()});
}


void collections() {
    notifier.notify({"gui", "close", "collections"});
}

void collections(int value) {
    notifier.notify({"gui", "close", "collections", value});
}

void collections(int value1, int value2) {
    notifier.notify({"gui", "close", "collections", value1, value2});
}

void collections(float value) {
    notifier.notify({"gui", "close", "collections", value});
}

void collections(const std::string& value) {
    notifier.notify({"gui", "close", "collections", value});
}

void collections(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "collections", value.dump()});
}


void daily_bonus() {
    notifier.notify({"gui", "close", "daily_bonus"});
}

void daily_bonus(int value) {
    notifier.notify({"gui", "close", "daily_bonus", value});
}

void daily_bonus(int value1, int value2) {
    notifier.notify({"gui", "close", "daily_bonus", value1, value2});
}

void daily_bonus(float value) {
    notifier.notify({"gui", "close", "daily_bonus", value});
}

void daily_bonus(const std::string& value) {
    notifier.notify({"gui", "close", "daily_bonus", value});
}

void daily_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "daily_bonus", value.dump()});
}


void daily_tasks() {
    notifier.notify({"gui", "close", "daily_tasks"});
}

void daily_tasks(int value) {
    notifier.notify({"gui", "close", "daily_tasks", value});
}

void daily_tasks(int value1, int value2) {
    notifier.notify({"gui", "close", "daily_tasks", value1, value2});
}

void daily_tasks(float value) {
    notifier.notify({"gui", "close", "daily_tasks", value});
}

void daily_tasks(const std::string& value) {
    notifier.notify({"gui", "close", "daily_tasks", value});
}

void daily_tasks(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "daily_tasks", value.dump()});
}


void game_ui() {
    notifier.notify({"gui", "close", "game_ui"});
}

void game_ui(int value) {
    notifier.notify({"gui", "close", "game_ui", value});
}

void game_ui(int value1, int value2) {
    notifier.notify({"gui", "close", "game_ui", value1, value2});
}

void game_ui(float value) {
    notifier.notify({"gui", "close", "game_ui", value});
}

void game_ui(const std::string& value) {
    notifier.notify({"gui", "close", "game_ui", value});
}

void game_ui(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "game_ui", value.dump()});
}


void lose_window() {
    notifier.notify({"gui", "close", "lose_window"});
}

void lose_window(int value) {
    notifier.notify({"gui", "close", "lose_window", value});
}

void lose_window(int value1, int value2) {
    notifier.notify({"gui", "close", "lose_window", value1, value2});
}

void lose_window(float value) {
    notifier.notify({"gui", "close", "lose_window", value});
}

void lose_window(const std::string& value) {
    notifier.notify({"gui", "close", "lose_window", value});
}

void lose_window(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "lose_window", value.dump()});
}


void menu() {
    notifier.notify({"gui", "close", "menu"});
}

void menu(int value) {
    notifier.notify({"gui", "close", "menu", value});
}

void menu(int value1, int value2) {
    notifier.notify({"gui", "close", "menu", value1, value2});
}

void menu(float value) {
    notifier.notify({"gui", "close", "menu", value});
}

void menu(const std::string& value) {
    notifier.notify({"gui", "close", "menu", value});
}

void menu(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "menu", value.dump()});
}


void new_element() {
    notifier.notify({"gui", "close", "new_element"});
}

void new_element(int value) {
    notifier.notify({"gui", "close", "new_element", value});
}

void new_element(int value1, int value2) {
    notifier.notify({"gui", "close", "new_element", value1, value2});
}

void new_element(float value) {
    notifier.notify({"gui", "close", "new_element", value});
}

void new_element(const std::string& value) {
    notifier.notify({"gui", "close", "new_element", value});
}

void new_element(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "new_element", value.dump()});
}


void pause_window() {
    notifier.notify({"gui", "close", "pause_window"});
}

void pause_window(int value) {
    notifier.notify({"gui", "close", "pause_window", value});
}

void pause_window(int value1, int value2) {
    notifier.notify({"gui", "close", "pause_window", value1, value2});
}

void pause_window(float value) {
    notifier.notify({"gui", "close", "pause_window", value});
}

void pause_window(const std::string& value) {
    notifier.notify({"gui", "close", "pause_window", value});
}

void pause_window(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "pause_window", value.dump()});
}


void rate_external() {
    notifier.notify({"gui", "close", "rate_external"});
}

void rate_external(int value) {
    notifier.notify({"gui", "close", "rate_external", value});
}

void rate_external(int value1, int value2) {
    notifier.notify({"gui", "close", "rate_external", value1, value2});
}

void rate_external(float value) {
    notifier.notify({"gui", "close", "rate_external", value});
}

void rate_external(const std::string& value) {
    notifier.notify({"gui", "close", "rate_external", value});
}

void rate_external(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "rate_external", value.dump()});
}


void rate_internal() {
    notifier.notify({"gui", "close", "rate_internal"});
}

void rate_internal(int value) {
    notifier.notify({"gui", "close", "rate_internal", value});
}

void rate_internal(int value1, int value2) {
    notifier.notify({"gui", "close", "rate_internal", value1, value2});
}

void rate_internal(float value) {
    notifier.notify({"gui", "close", "rate_internal", value});
}

void rate_internal(const std::string& value) {
    notifier.notify({"gui", "close", "rate_internal", value});
}

void rate_internal(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "rate_internal", value.dump()});
}


void settings() {
    notifier.notify({"gui", "close", "settings"});
}

void settings(int value) {
    notifier.notify({"gui", "close", "settings", value});
}

void settings(int value1, int value2) {
    notifier.notify({"gui", "close", "settings", value1, value2});
}

void settings(float value) {
    notifier.notify({"gui", "close", "settings", value});
}

void settings(const std::string& value) {
    notifier.notify({"gui", "close", "settings", value});
}

void settings(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "settings", value.dump()});
}


void shop() {
    notifier.notify({"gui", "close", "shop"});
}

void shop(int value) {
    notifier.notify({"gui", "close", "shop", value});
}

void shop(int value1, int value2) {
    notifier.notify({"gui", "close", "shop", value1, value2});
}

void shop(float value) {
    notifier.notify({"gui", "close", "shop", value});
}

void shop(const std::string& value) {
    notifier.notify({"gui", "close", "shop", value});
}

void shop(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "shop", value.dump()});
}


void term_of_conditions() {
    notifier.notify({"gui", "close", "term_of_conditions"});
}

void term_of_conditions(int value) {
    notifier.notify({"gui", "close", "term_of_conditions", value});
}

void term_of_conditions(int value1, int value2) {
    notifier.notify({"gui", "close", "term_of_conditions", value1, value2});
}

void term_of_conditions(float value) {
    notifier.notify({"gui", "close", "term_of_conditions", value});
}

void term_of_conditions(const std::string& value) {
    notifier.notify({"gui", "close", "term_of_conditions", value});
}

void term_of_conditions(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "term_of_conditions", value.dump()});
}


void win_window() {
    notifier.notify({"gui", "close", "win_window"});
}

void win_window(int value) {
    notifier.notify({"gui", "close", "win_window", value});
}

void win_window(int value1, int value2) {
    notifier.notify({"gui", "close", "win_window", value1, value2});
}

void win_window(float value) {
    notifier.notify({"gui", "close", "win_window", value});
}

void win_window(const std::string& value) {
    notifier.notify({"gui", "close", "win_window", value});
}

void win_window(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "win_window", value.dump()});
}


void level_rules() {
    notifier.notify({"gui", "close", "level_rules"});
}

void level_rules(int value) {
    notifier.notify({"gui", "close", "level_rules", value});
}

void level_rules(int value1, int value2) {
    notifier.notify({"gui", "close", "level_rules", value1, value2});
}

void level_rules(float value) {
    notifier.notify({"gui", "close", "level_rules", value});
}

void level_rules(const std::string& value) {
    notifier.notify({"gui", "close", "level_rules", value});
}

void level_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "level_rules", value.dump()});
}


void suggest_bonus() {
    notifier.notify({"gui", "close", "suggest_bonus"});
}

void suggest_bonus(int value) {
    notifier.notify({"gui", "close", "suggest_bonus", value});
}

void suggest_bonus(int value1, int value2) {
    notifier.notify({"gui", "close", "suggest_bonus", value1, value2});
}

void suggest_bonus(float value) {
    notifier.notify({"gui", "close", "suggest_bonus", value});
}

void suggest_bonus(const std::string& value) {
    notifier.notify({"gui", "close", "suggest_bonus", value});
}

void suggest_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "suggest_bonus", value.dump()});
}


void the_rules() {
    notifier.notify({"gui", "close", "the_rules"});
}

void the_rules(int value) {
    notifier.notify({"gui", "close", "the_rules", value});
}

void the_rules(int value1, int value2) {
    notifier.notify({"gui", "close", "the_rules", value1, value2});
}

void the_rules(float value) {
    notifier.notify({"gui", "close", "the_rules", value});
}

void the_rules(const std::string& value) {
    notifier.notify({"gui", "close", "the_rules", value});
}

void the_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "close", "the_rules", value.dump()});
}

}
namespace show {

void about() {
    notifier.notify({"gui", "show", "about"});
}

void about(int value) {
    notifier.notify({"gui", "show", "about", value});
}

void about(int value1, int value2) {
    notifier.notify({"gui", "show", "about", value1, value2});
}

void about(float value) {
    notifier.notify({"gui", "show", "about", value});
}

void about(const std::string& value) {
    notifier.notify({"gui", "show", "about", value});
}

void about(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "about", value.dump()});
}


void ads_for_hint() {
    notifier.notify({"gui", "show", "ads_for_hint"});
}

void ads_for_hint(int value) {
    notifier.notify({"gui", "show", "ads_for_hint", value});
}

void ads_for_hint(int value1, int value2) {
    notifier.notify({"gui", "show", "ads_for_hint", value1, value2});
}

void ads_for_hint(float value) {
    notifier.notify({"gui", "show", "ads_for_hint", value});
}

void ads_for_hint(const std::string& value) {
    notifier.notify({"gui", "show", "ads_for_hint", value});
}

void ads_for_hint(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "ads_for_hint", value.dump()});
}


void adventure() {
    notifier.notify({"gui", "show", "adventure"});
}

void adventure(int value) {
    notifier.notify({"gui", "show", "adventure", value});
}

void adventure(int value1, int value2) {
    notifier.notify({"gui", "show", "adventure", value1, value2});
}

void adventure(float value) {
    notifier.notify({"gui", "show", "adventure", value});
}

void adventure(const std::string& value) {
    notifier.notify({"gui", "show", "adventure", value});
}

void adventure(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "adventure", value.dump()});
}


void collection_complete() {
    notifier.notify({"gui", "show", "collection_complete"});
}

void collection_complete(int value) {
    notifier.notify({"gui", "show", "collection_complete", value});
}

void collection_complete(int value1, int value2) {
    notifier.notify({"gui", "show", "collection_complete", value1, value2});
}

void collection_complete(float value) {
    notifier.notify({"gui", "show", "collection_complete", value});
}

void collection_complete(const std::string& value) {
    notifier.notify({"gui", "show", "collection_complete", value});
}

void collection_complete(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "collection_complete", value.dump()});
}


void collections() {
    notifier.notify({"gui", "show", "collections"});
}

void collections(int value) {
    notifier.notify({"gui", "show", "collections", value});
}

void collections(int value1, int value2) {
    notifier.notify({"gui", "show", "collections", value1, value2});
}

void collections(float value) {
    notifier.notify({"gui", "show", "collections", value});
}

void collections(const std::string& value) {
    notifier.notify({"gui", "show", "collections", value});
}

void collections(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "collections", value.dump()});
}


void daily_bonus() {
    notifier.notify({"gui", "show", "daily_bonus"});
}

void daily_bonus(int value) {
    notifier.notify({"gui", "show", "daily_bonus", value});
}

void daily_bonus(int value1, int value2) {
    notifier.notify({"gui", "show", "daily_bonus", value1, value2});
}

void daily_bonus(float value) {
    notifier.notify({"gui", "show", "daily_bonus", value});
}

void daily_bonus(const std::string& value) {
    notifier.notify({"gui", "show", "daily_bonus", value});
}

void daily_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "daily_bonus", value.dump()});
}


void daily_tasks() {
    notifier.notify({"gui", "show", "daily_tasks"});
}

void daily_tasks(int value) {
    notifier.notify({"gui", "show", "daily_tasks", value});
}

void daily_tasks(int value1, int value2) {
    notifier.notify({"gui", "show", "daily_tasks", value1, value2});
}

void daily_tasks(float value) {
    notifier.notify({"gui", "show", "daily_tasks", value});
}

void daily_tasks(const std::string& value) {
    notifier.notify({"gui", "show", "daily_tasks", value});
}

void daily_tasks(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "daily_tasks", value.dump()});
}


void game_ui() {
    notifier.notify({"gui", "show", "game_ui"});
}

void game_ui(int value) {
    notifier.notify({"gui", "show", "game_ui", value});
}

void game_ui(int value1, int value2) {
    notifier.notify({"gui", "show", "game_ui", value1, value2});
}

void game_ui(float value) {
    notifier.notify({"gui", "show", "game_ui", value});
}

void game_ui(const std::string& value) {
    notifier.notify({"gui", "show", "game_ui", value});
}

void game_ui(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "game_ui", value.dump()});
}


void lose_window() {
    notifier.notify({"gui", "show", "lose_window"});
}

void lose_window(int value) {
    notifier.notify({"gui", "show", "lose_window", value});
}

void lose_window(int value1, int value2) {
    notifier.notify({"gui", "show", "lose_window", value1, value2});
}

void lose_window(float value) {
    notifier.notify({"gui", "show", "lose_window", value});
}

void lose_window(const std::string& value) {
    notifier.notify({"gui", "show", "lose_window", value});
}

void lose_window(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "lose_window", value.dump()});
}


void menu() {
    notifier.notify({"gui", "show", "menu"});
}

void menu(int value) {
    notifier.notify({"gui", "show", "menu", value});
}

void menu(int value1, int value2) {
    notifier.notify({"gui", "show", "menu", value1, value2});
}

void menu(float value) {
    notifier.notify({"gui", "show", "menu", value});
}

void menu(const std::string& value) {
    notifier.notify({"gui", "show", "menu", value});
}

void menu(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "menu", value.dump()});
}


void new_element() {
    notifier.notify({"gui", "show", "new_element"});
}

void new_element(int value) {
    notifier.notify({"gui", "show", "new_element", value});
}

void new_element(int value1, int value2) {
    notifier.notify({"gui", "show", "new_element", value1, value2});
}

void new_element(float value) {
    notifier.notify({"gui", "show", "new_element", value});
}

void new_element(const std::string& value) {
    notifier.notify({"gui", "show", "new_element", value});
}

void new_element(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "new_element", value.dump()});
}


void pause_window() {
    notifier.notify({"gui", "show", "pause_window"});
}

void pause_window(int value) {
    notifier.notify({"gui", "show", "pause_window", value});
}

void pause_window(int value1, int value2) {
    notifier.notify({"gui", "show", "pause_window", value1, value2});
}

void pause_window(float value) {
    notifier.notify({"gui", "show", "pause_window", value});
}

void pause_window(const std::string& value) {
    notifier.notify({"gui", "show", "pause_window", value});
}

void pause_window(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "pause_window", value.dump()});
}


void rate_external() {
    notifier.notify({"gui", "show", "rate_external"});
}

void rate_external(int value) {
    notifier.notify({"gui", "show", "rate_external", value});
}

void rate_external(int value1, int value2) {
    notifier.notify({"gui", "show", "rate_external", value1, value2});
}

void rate_external(float value) {
    notifier.notify({"gui", "show", "rate_external", value});
}

void rate_external(const std::string& value) {
    notifier.notify({"gui", "show", "rate_external", value});
}

void rate_external(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "rate_external", value.dump()});
}


void rate_internal() {
    notifier.notify({"gui", "show", "rate_internal"});
}

void rate_internal(int value) {
    notifier.notify({"gui", "show", "rate_internal", value});
}

void rate_internal(int value1, int value2) {
    notifier.notify({"gui", "show", "rate_internal", value1, value2});
}

void rate_internal(float value) {
    notifier.notify({"gui", "show", "rate_internal", value});
}

void rate_internal(const std::string& value) {
    notifier.notify({"gui", "show", "rate_internal", value});
}

void rate_internal(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "rate_internal", value.dump()});
}


void settings() {
    notifier.notify({"gui", "show", "settings"});
}

void settings(int value) {
    notifier.notify({"gui", "show", "settings", value});
}

void settings(int value1, int value2) {
    notifier.notify({"gui", "show", "settings", value1, value2});
}

void settings(float value) {
    notifier.notify({"gui", "show", "settings", value});
}

void settings(const std::string& value) {
    notifier.notify({"gui", "show", "settings", value});
}

void settings(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "settings", value.dump()});
}


void shop() {
    notifier.notify({"gui", "show", "shop"});
}

void shop(int value) {
    notifier.notify({"gui", "show", "shop", value});
}

void shop(int value1, int value2) {
    notifier.notify({"gui", "show", "shop", value1, value2});
}

void shop(float value) {
    notifier.notify({"gui", "show", "shop", value});
}

void shop(const std::string& value) {
    notifier.notify({"gui", "show", "shop", value});
}

void shop(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "shop", value.dump()});
}


void term_of_conditions() {
    notifier.notify({"gui", "show", "term_of_conditions"});
}

void term_of_conditions(int value) {
    notifier.notify({"gui", "show", "term_of_conditions", value});
}

void term_of_conditions(int value1, int value2) {
    notifier.notify({"gui", "show", "term_of_conditions", value1, value2});
}

void term_of_conditions(float value) {
    notifier.notify({"gui", "show", "term_of_conditions", value});
}

void term_of_conditions(const std::string& value) {
    notifier.notify({"gui", "show", "term_of_conditions", value});
}

void term_of_conditions(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "term_of_conditions", value.dump()});
}


void win_window() {
    notifier.notify({"gui", "show", "win_window"});
}

void win_window(int value) {
    notifier.notify({"gui", "show", "win_window", value});
}

void win_window(int value1, int value2) {
    notifier.notify({"gui", "show", "win_window", value1, value2});
}

void win_window(float value) {
    notifier.notify({"gui", "show", "win_window", value});
}

void win_window(const std::string& value) {
    notifier.notify({"gui", "show", "win_window", value});
}

void win_window(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "win_window", value.dump()});
}


void level_rules() {
    notifier.notify({"gui", "show", "level_rules"});
}

void level_rules(int value) {
    notifier.notify({"gui", "show", "level_rules", value});
}

void level_rules(int value1, int value2) {
    notifier.notify({"gui", "show", "level_rules", value1, value2});
}

void level_rules(float value) {
    notifier.notify({"gui", "show", "level_rules", value});
}

void level_rules(const std::string& value) {
    notifier.notify({"gui", "show", "level_rules", value});
}

void level_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "level_rules", value.dump()});
}


void suggest_bonus() {
    notifier.notify({"gui", "show", "suggest_bonus"});
}

void suggest_bonus(int value) {
    notifier.notify({"gui", "show", "suggest_bonus", value});
}

void suggest_bonus(int value1, int value2) {
    notifier.notify({"gui", "show", "suggest_bonus", value1, value2});
}

void suggest_bonus(float value) {
    notifier.notify({"gui", "show", "suggest_bonus", value});
}

void suggest_bonus(const std::string& value) {
    notifier.notify({"gui", "show", "suggest_bonus", value});
}

void suggest_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "suggest_bonus", value.dump()});
}


void the_rules() {
    notifier.notify({"gui", "show", "the_rules"});
}

void the_rules(int value) {
    notifier.notify({"gui", "show", "the_rules", value});
}

void the_rules(int value1, int value2) {
    notifier.notify({"gui", "show", "the_rules", value1, value2});
}

void the_rules(float value) {
    notifier.notify({"gui", "show", "the_rules", value});
}

void the_rules(const std::string& value) {
    notifier.notify({"gui", "show", "the_rules", value});
}

void the_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "show", "the_rules", value.dump()});
}

}
namespace hide {

void about() {
    notifier.notify({"gui", "hide", "about"});
}

void about(int value) {
    notifier.notify({"gui", "hide", "about", value});
}

void about(int value1, int value2) {
    notifier.notify({"gui", "hide", "about", value1, value2});
}

void about(float value) {
    notifier.notify({"gui", "hide", "about", value});
}

void about(const std::string& value) {
    notifier.notify({"gui", "hide", "about", value});
}

void about(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "about", value.dump()});
}


void ads_for_hint() {
    notifier.notify({"gui", "hide", "ads_for_hint"});
}

void ads_for_hint(int value) {
    notifier.notify({"gui", "hide", "ads_for_hint", value});
}

void ads_for_hint(int value1, int value2) {
    notifier.notify({"gui", "hide", "ads_for_hint", value1, value2});
}

void ads_for_hint(float value) {
    notifier.notify({"gui", "hide", "ads_for_hint", value});
}

void ads_for_hint(const std::string& value) {
    notifier.notify({"gui", "hide", "ads_for_hint", value});
}

void ads_for_hint(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "ads_for_hint", value.dump()});
}


void adventure() {
    notifier.notify({"gui", "hide", "adventure"});
}

void adventure(int value) {
    notifier.notify({"gui", "hide", "adventure", value});
}

void adventure(int value1, int value2) {
    notifier.notify({"gui", "hide", "adventure", value1, value2});
}

void adventure(float value) {
    notifier.notify({"gui", "hide", "adventure", value});
}

void adventure(const std::string& value) {
    notifier.notify({"gui", "hide", "adventure", value});
}

void adventure(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "adventure", value.dump()});
}


void collection_complete() {
    notifier.notify({"gui", "hide", "collection_complete"});
}

void collection_complete(int value) {
    notifier.notify({"gui", "hide", "collection_complete", value});
}

void collection_complete(int value1, int value2) {
    notifier.notify({"gui", "hide", "collection_complete", value1, value2});
}

void collection_complete(float value) {
    notifier.notify({"gui", "hide", "collection_complete", value});
}

void collection_complete(const std::string& value) {
    notifier.notify({"gui", "hide", "collection_complete", value});
}

void collection_complete(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "collection_complete", value.dump()});
}


void collections() {
    notifier.notify({"gui", "hide", "collections"});
}

void collections(int value) {
    notifier.notify({"gui", "hide", "collections", value});
}

void collections(int value1, int value2) {
    notifier.notify({"gui", "hide", "collections", value1, value2});
}

void collections(float value) {
    notifier.notify({"gui", "hide", "collections", value});
}

void collections(const std::string& value) {
    notifier.notify({"gui", "hide", "collections", value});
}

void collections(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "collections", value.dump()});
}


void daily_bonus() {
    notifier.notify({"gui", "hide", "daily_bonus"});
}

void daily_bonus(int value) {
    notifier.notify({"gui", "hide", "daily_bonus", value});
}

void daily_bonus(int value1, int value2) {
    notifier.notify({"gui", "hide", "daily_bonus", value1, value2});
}

void daily_bonus(float value) {
    notifier.notify({"gui", "hide", "daily_bonus", value});
}

void daily_bonus(const std::string& value) {
    notifier.notify({"gui", "hide", "daily_bonus", value});
}

void daily_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "daily_bonus", value.dump()});
}


void daily_tasks() {
    notifier.notify({"gui", "hide", "daily_tasks"});
}

void daily_tasks(int value) {
    notifier.notify({"gui", "hide", "daily_tasks", value});
}

void daily_tasks(int value1, int value2) {
    notifier.notify({"gui", "hide", "daily_tasks", value1, value2});
}

void daily_tasks(float value) {
    notifier.notify({"gui", "hide", "daily_tasks", value});
}

void daily_tasks(const std::string& value) {
    notifier.notify({"gui", "hide", "daily_tasks", value});
}

void daily_tasks(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "daily_tasks", value.dump()});
}


void game_ui() {
    notifier.notify({"gui", "hide", "game_ui"});
}

void game_ui(int value) {
    notifier.notify({"gui", "hide", "game_ui", value});
}

void game_ui(int value1, int value2) {
    notifier.notify({"gui", "hide", "game_ui", value1, value2});
}

void game_ui(float value) {
    notifier.notify({"gui", "hide", "game_ui", value});
}

void game_ui(const std::string& value) {
    notifier.notify({"gui", "hide", "game_ui", value});
}

void game_ui(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "game_ui", value.dump()});
}


void lose_window() {
    notifier.notify({"gui", "hide", "lose_window"});
}

void lose_window(int value) {
    notifier.notify({"gui", "hide", "lose_window", value});
}

void lose_window(int value1, int value2) {
    notifier.notify({"gui", "hide", "lose_window", value1, value2});
}

void lose_window(float value) {
    notifier.notify({"gui", "hide", "lose_window", value});
}

void lose_window(const std::string& value) {
    notifier.notify({"gui", "hide", "lose_window", value});
}

void lose_window(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "lose_window", value.dump()});
}


void menu() {
    notifier.notify({"gui", "hide", "menu"});
}

void menu(int value) {
    notifier.notify({"gui", "hide", "menu", value});
}

void menu(int value1, int value2) {
    notifier.notify({"gui", "hide", "menu", value1, value2});
}

void menu(float value) {
    notifier.notify({"gui", "hide", "menu", value});
}

void menu(const std::string& value) {
    notifier.notify({"gui", "hide", "menu", value});
}

void menu(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "menu", value.dump()});
}


void new_element() {
    notifier.notify({"gui", "hide", "new_element"});
}

void new_element(int value) {
    notifier.notify({"gui", "hide", "new_element", value});
}

void new_element(int value1, int value2) {
    notifier.notify({"gui", "hide", "new_element", value1, value2});
}

void new_element(float value) {
    notifier.notify({"gui", "hide", "new_element", value});
}

void new_element(const std::string& value) {
    notifier.notify({"gui", "hide", "new_element", value});
}

void new_element(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "new_element", value.dump()});
}


void pause_window() {
    notifier.notify({"gui", "hide", "pause_window"});
}

void pause_window(int value) {
    notifier.notify({"gui", "hide", "pause_window", value});
}

void pause_window(int value1, int value2) {
    notifier.notify({"gui", "hide", "pause_window", value1, value2});
}

void pause_window(float value) {
    notifier.notify({"gui", "hide", "pause_window", value});
}

void pause_window(const std::string& value) {
    notifier.notify({"gui", "hide", "pause_window", value});
}

void pause_window(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "pause_window", value.dump()});
}


void rate_external() {
    notifier.notify({"gui", "hide", "rate_external"});
}

void rate_external(int value) {
    notifier.notify({"gui", "hide", "rate_external", value});
}

void rate_external(int value1, int value2) {
    notifier.notify({"gui", "hide", "rate_external", value1, value2});
}

void rate_external(float value) {
    notifier.notify({"gui", "hide", "rate_external", value});
}

void rate_external(const std::string& value) {
    notifier.notify({"gui", "hide", "rate_external", value});
}

void rate_external(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "rate_external", value.dump()});
}


void rate_internal() {
    notifier.notify({"gui", "hide", "rate_internal"});
}

void rate_internal(int value) {
    notifier.notify({"gui", "hide", "rate_internal", value});
}

void rate_internal(int value1, int value2) {
    notifier.notify({"gui", "hide", "rate_internal", value1, value2});
}

void rate_internal(float value) {
    notifier.notify({"gui", "hide", "rate_internal", value});
}

void rate_internal(const std::string& value) {
    notifier.notify({"gui", "hide", "rate_internal", value});
}

void rate_internal(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "rate_internal", value.dump()});
}


void settings() {
    notifier.notify({"gui", "hide", "settings"});
}

void settings(int value) {
    notifier.notify({"gui", "hide", "settings", value});
}

void settings(int value1, int value2) {
    notifier.notify({"gui", "hide", "settings", value1, value2});
}

void settings(float value) {
    notifier.notify({"gui", "hide", "settings", value});
}

void settings(const std::string& value) {
    notifier.notify({"gui", "hide", "settings", value});
}

void settings(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "settings", value.dump()});
}


void shop() {
    notifier.notify({"gui", "hide", "shop"});
}

void shop(int value) {
    notifier.notify({"gui", "hide", "shop", value});
}

void shop(int value1, int value2) {
    notifier.notify({"gui", "hide", "shop", value1, value2});
}

void shop(float value) {
    notifier.notify({"gui", "hide", "shop", value});
}

void shop(const std::string& value) {
    notifier.notify({"gui", "hide", "shop", value});
}

void shop(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "shop", value.dump()});
}


void term_of_conditions() {
    notifier.notify({"gui", "hide", "term_of_conditions"});
}

void term_of_conditions(int value) {
    notifier.notify({"gui", "hide", "term_of_conditions", value});
}

void term_of_conditions(int value1, int value2) {
    notifier.notify({"gui", "hide", "term_of_conditions", value1, value2});
}

void term_of_conditions(float value) {
    notifier.notify({"gui", "hide", "term_of_conditions", value});
}

void term_of_conditions(const std::string& value) {
    notifier.notify({"gui", "hide", "term_of_conditions", value});
}

void term_of_conditions(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "term_of_conditions", value.dump()});
}


void win_window() {
    notifier.notify({"gui", "hide", "win_window"});
}

void win_window(int value) {
    notifier.notify({"gui", "hide", "win_window", value});
}

void win_window(int value1, int value2) {
    notifier.notify({"gui", "hide", "win_window", value1, value2});
}

void win_window(float value) {
    notifier.notify({"gui", "hide", "win_window", value});
}

void win_window(const std::string& value) {
    notifier.notify({"gui", "hide", "win_window", value});
}

void win_window(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "win_window", value.dump()});
}


void level_rules() {
    notifier.notify({"gui", "hide", "level_rules"});
}

void level_rules(int value) {
    notifier.notify({"gui", "hide", "level_rules", value});
}

void level_rules(int value1, int value2) {
    notifier.notify({"gui", "hide", "level_rules", value1, value2});
}

void level_rules(float value) {
    notifier.notify({"gui", "hide", "level_rules", value});
}

void level_rules(const std::string& value) {
    notifier.notify({"gui", "hide", "level_rules", value});
}

void level_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "level_rules", value.dump()});
}


void suggest_bonus() {
    notifier.notify({"gui", "hide", "suggest_bonus"});
}

void suggest_bonus(int value) {
    notifier.notify({"gui", "hide", "suggest_bonus", value});
}

void suggest_bonus(int value1, int value2) {
    notifier.notify({"gui", "hide", "suggest_bonus", value1, value2});
}

void suggest_bonus(float value) {
    notifier.notify({"gui", "hide", "suggest_bonus", value});
}

void suggest_bonus(const std::string& value) {
    notifier.notify({"gui", "hide", "suggest_bonus", value});
}

void suggest_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "suggest_bonus", value.dump()});
}


void the_rules() {
    notifier.notify({"gui", "hide", "the_rules"});
}

void the_rules(int value) {
    notifier.notify({"gui", "hide", "the_rules", value});
}

void the_rules(int value1, int value2) {
    notifier.notify({"gui", "hide", "the_rules", value1, value2});
}

void the_rules(float value) {
    notifier.notify({"gui", "hide", "the_rules", value});
}

void the_rules(const std::string& value) {
    notifier.notify({"gui", "hide", "the_rules", value});
}

void the_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "hide", "the_rules", value.dump()});
}

}
namespace update {

void about() {
    notifier.notify({"gui", "update", "about"});
}

void about(int value) {
    notifier.notify({"gui", "update", "about", value});
}

void about(int value1, int value2) {
    notifier.notify({"gui", "update", "about", value1, value2});
}

void about(float value) {
    notifier.notify({"gui", "update", "about", value});
}

void about(const std::string& value) {
    notifier.notify({"gui", "update", "about", value});
}

void about(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "about", value.dump()});
}


void ads_for_hint() {
    notifier.notify({"gui", "update", "ads_for_hint"});
}

void ads_for_hint(int value) {
    notifier.notify({"gui", "update", "ads_for_hint", value});
}

void ads_for_hint(int value1, int value2) {
    notifier.notify({"gui", "update", "ads_for_hint", value1, value2});
}

void ads_for_hint(float value) {
    notifier.notify({"gui", "update", "ads_for_hint", value});
}

void ads_for_hint(const std::string& value) {
    notifier.notify({"gui", "update", "ads_for_hint", value});
}

void ads_for_hint(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "ads_for_hint", value.dump()});
}


void adventure() {
    notifier.notify({"gui", "update", "adventure"});
}

void adventure(int value) {
    notifier.notify({"gui", "update", "adventure", value});
}

void adventure(int value1, int value2) {
    notifier.notify({"gui", "update", "adventure", value1, value2});
}

void adventure(float value) {
    notifier.notify({"gui", "update", "adventure", value});
}

void adventure(const std::string& value) {
    notifier.notify({"gui", "update", "adventure", value});
}

void adventure(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "adventure", value.dump()});
}


void collection_complete() {
    notifier.notify({"gui", "update", "collection_complete"});
}

void collection_complete(int value) {
    notifier.notify({"gui", "update", "collection_complete", value});
}

void collection_complete(int value1, int value2) {
    notifier.notify({"gui", "update", "collection_complete", value1, value2});
}

void collection_complete(float value) {
    notifier.notify({"gui", "update", "collection_complete", value});
}

void collection_complete(const std::string& value) {
    notifier.notify({"gui", "update", "collection_complete", value});
}

void collection_complete(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "collection_complete", value.dump()});
}


void collections() {
    notifier.notify({"gui", "update", "collections"});
}

void collections(int value) {
    notifier.notify({"gui", "update", "collections", value});
}

void collections(int value1, int value2) {
    notifier.notify({"gui", "update", "collections", value1, value2});
}

void collections(float value) {
    notifier.notify({"gui", "update", "collections", value});
}

void collections(const std::string& value) {
    notifier.notify({"gui", "update", "collections", value});
}

void collections(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "collections", value.dump()});
}


void daily_bonus() {
    notifier.notify({"gui", "update", "daily_bonus"});
}

void daily_bonus(int value) {
    notifier.notify({"gui", "update", "daily_bonus", value});
}

void daily_bonus(int value1, int value2) {
    notifier.notify({"gui", "update", "daily_bonus", value1, value2});
}

void daily_bonus(float value) {
    notifier.notify({"gui", "update", "daily_bonus", value});
}

void daily_bonus(const std::string& value) {
    notifier.notify({"gui", "update", "daily_bonus", value});
}

void daily_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "daily_bonus", value.dump()});
}


void daily_tasks() {
    notifier.notify({"gui", "update", "daily_tasks"});
}

void daily_tasks(int value) {
    notifier.notify({"gui", "update", "daily_tasks", value});
}

void daily_tasks(int value1, int value2) {
    notifier.notify({"gui", "update", "daily_tasks", value1, value2});
}

void daily_tasks(float value) {
    notifier.notify({"gui", "update", "daily_tasks", value});
}

void daily_tasks(const std::string& value) {
    notifier.notify({"gui", "update", "daily_tasks", value});
}

void daily_tasks(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "daily_tasks", value.dump()});
}


void game_ui() {
    notifier.notify({"gui", "update", "game_ui"});
}

void game_ui(int value) {
    notifier.notify({"gui", "update", "game_ui", value});
}

void game_ui(int value1, int value2) {
    notifier.notify({"gui", "update", "game_ui", value1, value2});
}

void game_ui(float value) {
    notifier.notify({"gui", "update", "game_ui", value});
}

void game_ui(const std::string& value) {
    notifier.notify({"gui", "update", "game_ui", value});
}

void game_ui(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "game_ui", value.dump()});
}


void lose_window() {
    notifier.notify({"gui", "update", "lose_window"});
}

void lose_window(int value) {
    notifier.notify({"gui", "update", "lose_window", value});
}

void lose_window(int value1, int value2) {
    notifier.notify({"gui", "update", "lose_window", value1, value2});
}

void lose_window(float value) {
    notifier.notify({"gui", "update", "lose_window", value});
}

void lose_window(const std::string& value) {
    notifier.notify({"gui", "update", "lose_window", value});
}

void lose_window(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "lose_window", value.dump()});
}


void menu() {
    notifier.notify({"gui", "update", "menu"});
}

void menu(int value) {
    notifier.notify({"gui", "update", "menu", value});
}

void menu(int value1, int value2) {
    notifier.notify({"gui", "update", "menu", value1, value2});
}

void menu(float value) {
    notifier.notify({"gui", "update", "menu", value});
}

void menu(const std::string& value) {
    notifier.notify({"gui", "update", "menu", value});
}

void menu(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "menu", value.dump()});
}


void new_element() {
    notifier.notify({"gui", "update", "new_element"});
}

void new_element(int value) {
    notifier.notify({"gui", "update", "new_element", value});
}

void new_element(int value1, int value2) {
    notifier.notify({"gui", "update", "new_element", value1, value2});
}

void new_element(float value) {
    notifier.notify({"gui", "update", "new_element", value});
}

void new_element(const std::string& value) {
    notifier.notify({"gui", "update", "new_element", value});
}

void new_element(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "new_element", value.dump()});
}


void pause_window() {
    notifier.notify({"gui", "update", "pause_window"});
}

void pause_window(int value) {
    notifier.notify({"gui", "update", "pause_window", value});
}

void pause_window(int value1, int value2) {
    notifier.notify({"gui", "update", "pause_window", value1, value2});
}

void pause_window(float value) {
    notifier.notify({"gui", "update", "pause_window", value});
}

void pause_window(const std::string& value) {
    notifier.notify({"gui", "update", "pause_window", value});
}

void pause_window(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "pause_window", value.dump()});
}


void rate_external() {
    notifier.notify({"gui", "update", "rate_external"});
}

void rate_external(int value) {
    notifier.notify({"gui", "update", "rate_external", value});
}

void rate_external(int value1, int value2) {
    notifier.notify({"gui", "update", "rate_external", value1, value2});
}

void rate_external(float value) {
    notifier.notify({"gui", "update", "rate_external", value});
}

void rate_external(const std::string& value) {
    notifier.notify({"gui", "update", "rate_external", value});
}

void rate_external(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "rate_external", value.dump()});
}


void rate_internal() {
    notifier.notify({"gui", "update", "rate_internal"});
}

void rate_internal(int value) {
    notifier.notify({"gui", "update", "rate_internal", value});
}

void rate_internal(int value1, int value2) {
    notifier.notify({"gui", "update", "rate_internal", value1, value2});
}

void rate_internal(float value) {
    notifier.notify({"gui", "update", "rate_internal", value});
}

void rate_internal(const std::string& value) {
    notifier.notify({"gui", "update", "rate_internal", value});
}

void rate_internal(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "rate_internal", value.dump()});
}


void settings() {
    notifier.notify({"gui", "update", "settings"});
}

void settings(int value) {
    notifier.notify({"gui", "update", "settings", value});
}

void settings(int value1, int value2) {
    notifier.notify({"gui", "update", "settings", value1, value2});
}

void settings(float value) {
    notifier.notify({"gui", "update", "settings", value});
}

void settings(const std::string& value) {
    notifier.notify({"gui", "update", "settings", value});
}

void settings(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "settings", value.dump()});
}


void shop() {
    notifier.notify({"gui", "update", "shop"});
}

void shop(int value) {
    notifier.notify({"gui", "update", "shop", value});
}

void shop(int value1, int value2) {
    notifier.notify({"gui", "update", "shop", value1, value2});
}

void shop(float value) {
    notifier.notify({"gui", "update", "shop", value});
}

void shop(const std::string& value) {
    notifier.notify({"gui", "update", "shop", value});
}

void shop(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "shop", value.dump()});
}


void term_of_conditions() {
    notifier.notify({"gui", "update", "term_of_conditions"});
}

void term_of_conditions(int value) {
    notifier.notify({"gui", "update", "term_of_conditions", value});
}

void term_of_conditions(int value1, int value2) {
    notifier.notify({"gui", "update", "term_of_conditions", value1, value2});
}

void term_of_conditions(float value) {
    notifier.notify({"gui", "update", "term_of_conditions", value});
}

void term_of_conditions(const std::string& value) {
    notifier.notify({"gui", "update", "term_of_conditions", value});
}

void term_of_conditions(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "term_of_conditions", value.dump()});
}


void win_window() {
    notifier.notify({"gui", "update", "win_window"});
}

void win_window(int value) {
    notifier.notify({"gui", "update", "win_window", value});
}

void win_window(int value1, int value2) {
    notifier.notify({"gui", "update", "win_window", value1, value2});
}

void win_window(float value) {
    notifier.notify({"gui", "update", "win_window", value});
}

void win_window(const std::string& value) {
    notifier.notify({"gui", "update", "win_window", value});
}

void win_window(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "win_window", value.dump()});
}


void level_rules() {
    notifier.notify({"gui", "update", "level_rules"});
}

void level_rules(int value) {
    notifier.notify({"gui", "update", "level_rules", value});
}

void level_rules(int value1, int value2) {
    notifier.notify({"gui", "update", "level_rules", value1, value2});
}

void level_rules(float value) {
    notifier.notify({"gui", "update", "level_rules", value});
}

void level_rules(const std::string& value) {
    notifier.notify({"gui", "update", "level_rules", value});
}

void level_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "level_rules", value.dump()});
}


void suggest_bonus() {
    notifier.notify({"gui", "update", "suggest_bonus"});
}

void suggest_bonus(int value) {
    notifier.notify({"gui", "update", "suggest_bonus", value});
}

void suggest_bonus(int value1, int value2) {
    notifier.notify({"gui", "update", "suggest_bonus", value1, value2});
}

void suggest_bonus(float value) {
    notifier.notify({"gui", "update", "suggest_bonus", value});
}

void suggest_bonus(const std::string& value) {
    notifier.notify({"gui", "update", "suggest_bonus", value});
}

void suggest_bonus(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "suggest_bonus", value.dump()});
}


void the_rules() {
    notifier.notify({"gui", "update", "the_rules"});
}

void the_rules(int value) {
    notifier.notify({"gui", "update", "the_rules", value});
}

void the_rules(int value1, int value2) {
    notifier.notify({"gui", "update", "the_rules", value1, value2});
}

void the_rules(float value) {
    notifier.notify({"gui", "update", "the_rules", value});
}

void the_rules(const std::string& value) {
    notifier.notify({"gui", "update", "the_rules", value});
}

void the_rules(const nlohmann::json& value) {
    notifier.notify({"gui", "update", "the_rules", value.dump()});
}

}
}
namespace achievements {

void show() {
    notifier.notify({"achievements", "show"});
}

void show(int value) {
    notifier.notify({"achievements", "show", value});
}

void show(int value1, int value2) {
    notifier.notify({"achievements", "show", value1, value2});
}

void show(float value) {
    notifier.notify({"achievements", "show", value});
}

void show(const std::string& value) {
    notifier.notify({"achievements", "show", value});
}

void show(const nlohmann::json& value) {
    notifier.notify({"achievements", "show", value.dump()});
}


void increment() {
    notifier.notify({"achievements", "increment"});
}

void increment(int value) {
    notifier.notify({"achievements", "increment", value});
}

void increment(int value1, int value2) {
    notifier.notify({"achievements", "increment", value1, value2});
}

void increment(float value) {
    notifier.notify({"achievements", "increment", value});
}

void increment(const std::string& value) {
    notifier.notify({"achievements", "increment", value});
}

void increment(const nlohmann::json& value) {
    notifier.notify({"achievements", "increment", value.dump()});
}


void reveal() {
    notifier.notify({"achievements", "reveal"});
}

void reveal(int value) {
    notifier.notify({"achievements", "reveal", value});
}

void reveal(int value1, int value2) {
    notifier.notify({"achievements", "reveal", value1, value2});
}

void reveal(float value) {
    notifier.notify({"achievements", "reveal", value});
}

void reveal(const std::string& value) {
    notifier.notify({"achievements", "reveal", value});
}

void reveal(const nlohmann::json& value) {
    notifier.notify({"achievements", "reveal", value.dump()});
}


void setSteps() {
    notifier.notify({"achievements", "setSteps"});
}

void setSteps(int value) {
    notifier.notify({"achievements", "setSteps", value});
}

void setSteps(int value1, int value2) {
    notifier.notify({"achievements", "setSteps", value1, value2});
}

void setSteps(float value) {
    notifier.notify({"achievements", "setSteps", value});
}

void setSteps(const std::string& value) {
    notifier.notify({"achievements", "setSteps", value});
}

void setSteps(const nlohmann::json& value) {
    notifier.notify({"achievements", "setSteps", value.dump()});
}


void unlock() {
    notifier.notify({"achievements", "unlock"});
}

void unlock(int value) {
    notifier.notify({"achievements", "unlock", value});
}

void unlock(int value1, int value2) {
    notifier.notify({"achievements", "unlock", value1, value2});
}

void unlock(float value) {
    notifier.notify({"achievements", "unlock", value});
}

void unlock(const std::string& value) {
    notifier.notify({"achievements", "unlock", value});
}

void unlock(const nlohmann::json& value) {
    notifier.notify({"achievements", "unlock", value.dump()});
}

}
namespace sound {

void sound_on() {
    notifier.notify({"sound", "sound_on"});
}

void sound_on(int value) {
    notifier.notify({"sound", "sound_on", value});
}

void sound_on(int value1, int value2) {
    notifier.notify({"sound", "sound_on", value1, value2});
}

void sound_on(float value) {
    notifier.notify({"sound", "sound_on", value});
}

void sound_on(const std::string& value) {
    notifier.notify({"sound", "sound_on", value});
}

void sound_on(const nlohmann::json& value) {
    notifier.notify({"sound", "sound_on", value.dump()});
}


void sound_off() {
    notifier.notify({"sound", "sound_off"});
}

void sound_off(int value) {
    notifier.notify({"sound", "sound_off", value});
}

void sound_off(int value1, int value2) {
    notifier.notify({"sound", "sound_off", value1, value2});
}

void sound_off(float value) {
    notifier.notify({"sound", "sound_off", value});
}

void sound_off(const std::string& value) {
    notifier.notify({"sound", "sound_off", value});
}

void sound_off(const nlohmann::json& value) {
    notifier.notify({"sound", "sound_off", value.dump()});
}


void music_on() {
    notifier.notify({"sound", "music_on"});
}

void music_on(int value) {
    notifier.notify({"sound", "music_on", value});
}

void music_on(int value1, int value2) {
    notifier.notify({"sound", "music_on", value1, value2});
}

void music_on(float value) {
    notifier.notify({"sound", "music_on", value});
}

void music_on(const std::string& value) {
    notifier.notify({"sound", "music_on", value});
}

void music_on(const nlohmann::json& value) {
    notifier.notify({"sound", "music_on", value.dump()});
}


void music_off() {
    notifier.notify({"sound", "music_off"});
}

void music_off(int value) {
    notifier.notify({"sound", "music_off", value});
}

void music_off(int value1, int value2) {
    notifier.notify({"sound", "music_off", value1, value2});
}

void music_off(float value) {
    notifier.notify({"sound", "music_off", value});
}

void music_off(const std::string& value) {
    notifier.notify({"sound", "music_off", value});
}

void music_off(const nlohmann::json& value) {
    notifier.notify({"sound", "music_off", value.dump()});
}

}
namespace ads {

void interscenial() {
    notifier.notify({"ads", "interscenial"});
}

void interscenial(int value) {
    notifier.notify({"ads", "interscenial", value});
}

void interscenial(int value1, int value2) {
    notifier.notify({"ads", "interscenial", value1, value2});
}

void interscenial(float value) {
    notifier.notify({"ads", "interscenial", value});
}

void interscenial(const std::string& value) {
    notifier.notify({"ads", "interscenial", value});
}

void interscenial(const nlohmann::json& value) {
    notifier.notify({"ads", "interscenial", value.dump()});
}


void rewarded() {
    notifier.notify({"ads", "rewarded"});
}

void rewarded(int value) {
    notifier.notify({"ads", "rewarded", value});
}

void rewarded(int value1, int value2) {
    notifier.notify({"ads", "rewarded", value1, value2});
}

void rewarded(float value) {
    notifier.notify({"ads", "rewarded", value});
}

void rewarded(const std::string& value) {
    notifier.notify({"ads", "rewarded", value});
}

void rewarded(const nlohmann::json& value) {
    notifier.notify({"ads", "rewarded", value.dump()});
}

}
namespace inapp {

void buy() {
    notifier.notify({"inapp", "buy"});
}

void buy(int value) {
    notifier.notify({"inapp", "buy", value});
}

void buy(int value1, int value2) {
    notifier.notify({"inapp", "buy", value1, value2});
}

void buy(float value) {
    notifier.notify({"inapp", "buy", value});
}

void buy(const std::string& value) {
    notifier.notify({"inapp", "buy", value});
}

void buy(const nlohmann::json& value) {
    notifier.notify({"inapp", "buy", value.dump()});
}

}
namespace sys {

void navigate() {
    notifier.notify({"sys", "navigate"});
}

void navigate(int value) {
    notifier.notify({"sys", "navigate", value});
}

void navigate(int value1, int value2) {
    notifier.notify({"sys", "navigate", value1, value2});
}

void navigate(float value) {
    notifier.notify({"sys", "navigate", value});
}

void navigate(const std::string& value) {
    notifier.notify({"sys", "navigate", value});
}

void navigate(const nlohmann::json& value) {
    notifier.notify({"sys", "navigate", value.dump()});
}

}
}
