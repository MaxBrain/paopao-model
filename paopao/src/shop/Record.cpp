#include "shop/Record.h"
#include "3rdParty/json.hpp"

namespace shop {

Record::Record() {}

Record::Record(const nlohmann::json& data) {
	parsed = false;
    if (data.is_discarded()) return;
    if (!data.contains("ident")) return;
    if (!data.contains("title")) return;
    if (!data.contains("description")) return;
    if (!data.contains("currency_code")) return;
    if (!data.contains("price_string")) return;
    if (!data["ident"].is_string()) return;
    if (!data["title"].is_string()) return;
    if (!data["description"].is_string()) return;
    if (!data["currency_code"].is_string()) return;
    if (!data["price_string"].is_string()) return;
    ident = data["ident"];
    title = data["title"];
    description = data["description"];
    currency_code = data["currency_code"];
    price_string = data["price_string"];
	parsed = true;
}

}
