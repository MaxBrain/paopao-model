#include "shop/BoughtItem.h"
#include "3rdParty/json.hpp"

namespace shop {

BoughtItem::BoughtItem(const nlohmann::json& data) {
    if (data.is_discarded()) return;
    if (!data.contains("productID")) return;
    if (!data.contains("purchaseToken")) return;
    if (!data.contains("developerPayload")) return;
    if (!data.contains("signature")) return;
    if (!data["productID"].is_string()) return;
    if (!data["purchaseToken"].is_string()) return;
    if (!data["developerPayload"].is_string()) return;
    if (!data["signature"].is_string()) return;
    productID = data["productID"];
    purchaseToken = data["purchaseToken"];
    developerPayload = data["developerPayload"];
    signature = data["signature"];
    parsed = true;
}

}
