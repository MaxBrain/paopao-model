#include "shop/Shop.h"
#include "3rdParty/json.hpp"
#include "mediator/Logger.h"
#include "shop/BoughtItem.h"
#include "notify/notify.h"
#include "component/Components.h"
#include "ui/UI.h"
#include "i18n/L10n.h"
#include "config/Config.h"
#include "bonus/Bonuses.h"

namespace shop {

bool Shop::isEnabled() const {
	return enabled;
}

void Shop::setEnabled(bool enabled) {
	this->enabled = enabled;
}

bool Shop::isInitialized() const {
	return initialized;
}

void Shop::init(const std::string& elements) {
	nlohmann::json json{ nlohmann::json::parse(elements, nullptr, false) };
	if (json.is_discarded()) {
		LOG_ERROR("Can not parse json for Shop::init");
		return;
	}
	for (const auto good : json) {
		Record record{ good };
		if (record.parsed) goods[record.ident] = record;
	}
	initialized = true;
	component::ui->menu.updateShopButton();
}

bool Shop::isActive() const {
	return isEnabled() && isInitialized() && (component::config->shop != config::Shop::None);
}

void Shop::setBoughtElements(const std::string& data) {
	nlohmann::json json{ nlohmann::json::parse(data, nullptr, false) };
	if (json.is_discarded()) {
		LOG_ERROR("Can not parse json for Shop::setBoughtElements");
		return;
	}
	for (const auto item : json) {
		BoughtItem record{ item };
		if (record.parsed && record.productID == to_string(ItemType::NO_ADS)) {
			no_ads_bought = true;
		}
	}
}

void Shop::buy(ItemType itemType) {

}

void Shop::load(const nlohmann::json& data) {
	if (!data.is_object()) {
		LOG_ERROR("Non object for Shop::load");
		return;
	}
	if (!data.contains("shop")) {
		LOG_INFO("No data about shop in save");
		return;
	}
	const auto& shop_data{ data["shop"] };
	if (!shop_data.is_object()) {
		LOG_ERROR("Non object for data field in Shop::load");
		return;
	}
	if (shop_data.contains("no_ads")) {
		if (!shop_data["no_ads"].is_boolean()) {
			LOG_ERROR("Non bool element data.shop.no_ads in Shop::load");
			return;
		}
		no_ads_bought = data["shop"]["no_ads"];
	}
}

void Shop::save(nlohmann::json& data) {
	data["shop"]["no_ads"] = no_ads_bought;
}


bool Shop::noAdsBought() const {
	return no_ads_bought;
}

std::string Shop::getCost(ItemType itemType) const {
	auto id = to_string(itemType);
	auto it = goods.find(id);
	if (it == goods.end()) {
		LOG_ERROR("Can not find good in Shop::getCost. itemType[%d] id[%s]", static_cast<int>(itemType), id.c_str());
		return "";
	}
	return it->second.price_string;
}

std::string Shop::getTitle(ItemType itemType) const {
	auto id = to_string(itemType);
	auto it = goods.find(id);
	if (it == goods.end()) {
		LOG_ERROR("Can not find good in Shop::getTitle. itemType[%d] id[%s]", static_cast<int>(itemType), id.c_str());
		return "";
	}
	return it->second.title;
}

void Shop::buyComplete(const std::string& item) {
	ItemType itemType = parse(item);
	if (itemType == shop::ItemType::NO_TYPE) {
		LOG_ERROR("Looks like ItemType::NO_TYPE bought. String [%s]", item.c_str());
		return;
	}
	const std::string& title{ getTitle(itemType) };
	switch (itemType)
	{
	case shop::ItemType::NO_ADS:
		no_ads_bought = true;
		component::ui->daily_bonus.show(title, ui::DailyBonus::NoAds, false);
		component::ui->menu.updateNoAdsButton();
		component::ui->shop.updateNoAdsButton();
		break;
	case shop::ItemType::BOMB_LITTLE:
		component::bonuses->bomb += BOMB_LITTLE_COUNT;
		component::ui->suggest_bonus.hide();
		component::ui->daily_bonus.show(title, ui::DailyBonus::Bomb, true);
		component::ui->game_ui.updateBomb();
		break;
	case shop::ItemType::BOMB_BIG:
		component::bonuses->bomb += BOMB_BIG_COUNT;
		component::ui->suggest_bonus.hide();
		component::ui->daily_bonus.show(title, ui::DailyBonus::Bomb, true);
		component::ui->game_ui.updateBomb();
		break;
	case shop::ItemType::BOMB_PACK:
		component::bonuses->bomb += BOMB_PACK_COUNT;
		component::ui->daily_bonus.show(title, ui::DailyBonus::Bomb, false);
		break;
	case shop::ItemType::HINT_LITTLE:
		component::bonuses->hint += HINT_LITTLE_COUNT;
		component::ui->suggest_bonus.hide();
		component::ui->daily_bonus.show(title, ui::DailyBonus::Hint, true);
		component::ui->game_ui.updateHint();
		break;
	case shop::ItemType::HINT_BIG:
		component::bonuses->hint += HINT_BIG_COUNT;
		component::ui->suggest_bonus.hide();
		component::ui->daily_bonus.show(title, ui::DailyBonus::Hint, true);
		component::ui->game_ui.updateHint();
		break;
	case shop::ItemType::HINT_PACK:
		component::bonuses->hint += HINT_PACK_COUNT;
		component::ui->daily_bonus.show(title, ui::DailyBonus::Hint, false);
		break;
	case shop::ItemType::MIX_LITTLE:
		component::bonuses->mix += MIX_LITTLE_COUNT;
		component::ui->daily_bonus.show(title, ui::DailyBonus::Mix, true);
		component::ui->game_ui.updateMix();
		break;
	case shop::ItemType::MIX_BIG:
		component::bonuses->mix += MIX_BIG_COUNT;
		component::ui->daily_bonus.show(title, ui::DailyBonus::Mix, true);
		component::ui->game_ui.updateMix();
		break;
	case shop::ItemType::MIX_PACK:
		component::bonuses->mix += MIX_PACK_COUNT;
		component::ui->daily_bonus.show(title, ui::DailyBonus::Mix, false);
		break;
	}
}

bool Shop::buttonNoAdsShow() const {
	return isActive() && !noAdsBought();
}

}
