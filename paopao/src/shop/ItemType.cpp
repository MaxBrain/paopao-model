#include <map>
#include "shop/ItemType.h"
#include "mediator/Logger.h"

namespace shop {

namespace {

const std::map<std::string, ItemType> stringToEnum {
    {"NO_ADS", ItemType::NO_ADS},
    {"BOMB_LITTLE", ItemType::BOMB_LITTLE},
    {"BOMB_BIG", ItemType::BOMB_BIG},
    {"BOMB_PACK", ItemType::BOMB_PACK},
    {"HINT_LITTLE", ItemType::HINT_LITTLE},
    {"HINT_BIG", ItemType::HINT_BIG},
    {"HINT_PACK", ItemType::HINT_PACK},
    {"MIX_LITTLE", ItemType::MIX_LITTLE},
    {"MIX_BIG", ItemType::MIX_BIG},
    {"MIX_PACK", ItemType::MIX_PACK},
    {"NO_TYPE", ItemType::NO_TYPE},
};

const std::map<ItemType, std::string> enumToString{
    { ItemType::NO_ADS, "NO_ADS" },
    { ItemType::BOMB_LITTLE, "BOMB_LITTLE" },
    { ItemType::BOMB_BIG, "BOMB_BIG" },
    { ItemType::BOMB_PACK, "BOMB_PACK" },
    { ItemType::HINT_LITTLE, "HINT_LITTLE" },
    { ItemType::HINT_BIG, "HINT_BIG" },
    { ItemType::HINT_PACK, "HINT_PACK" },
    { ItemType::MIX_LITTLE, "MIX_LITTLE" },
    { ItemType::MIX_BIG, "MIX_BIG" },
    { ItemType::MIX_PACK, "MIX_PACK" },
    { ItemType::NO_TYPE, "NO_TYPE" },
};

}

ItemType parse(const std::string& str) {
    auto it = stringToEnum.find(str);
    if (it == stringToEnum.end()) {
        LOG_ERROR("Try to parse [] as shop::ItemType");
        return ItemType::NO_TYPE;
    }
    return it->second;
}

std::string to_string(ItemType itemType) {
    if (itemType == ItemType::NO_TYPE) {
        LOG_ERROR("Try to stringify ItemType::NO_TYPE");
        return "";
    }
    auto it = enumToString.find(itemType);
    if (it == enumToString.end()) {
        LOG_ERROR("Try to stringify unknown shop::ItemType [%d]", static_cast<int>(itemType));
        return "";
    }
    return it->second;
}

}
