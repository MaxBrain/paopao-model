#include "gameplay/Tutorial.h"
#include "gameplay/Level.h"
#include "ui/UI.h"
#include "component/Components.h"

namespace gameplay {

void Tutorial::start() {
	tiles = component::level->tiles();
	for (size_t x = 0; x < component::level->width(); x++) {
		for (size_t y = 0; y < component::level->height(); y++) {
			if (
				component::level->get(x, y).canAction() &&
				!(((x == 3) && (y == 6)) || ((x == 3) && (y == 8)))
				) {
				component::level->lock(x, y);
			}
		}
	}
}

void Tutorial::step() {
	switch (tiles - component::level->tiles())
	{
		case 2:
			component::level->unlock(3, 5);
			component::level->unlock(4, 8);
			break;
		case 4:
			component::level->unlock(4, 3);
			component::level->unlock(4, 6);
			break;
		case 6:
			component::level->unlock(3, 3);
			component::level->unlock(4, 5);
			break;
		case 8:
			for (size_t x = 0; x < component::level->width(); x++) {
				for (size_t y = 0; y < component::level->height(); y++) {
					if (component::level->get(x, y).present) {
						component::level->unlock(x, y);
					}
				}
			}
			break;
	}
}

}
