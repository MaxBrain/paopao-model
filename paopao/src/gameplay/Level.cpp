#include <map>
#include <utility>
#include "gameplay/Level.h"
#include "gameplay/Point.h"
#include "common/Rules.h"
#include "common/Random.h"
#include "common/Utils.h"
#include "mediator/Logger.h"
#include "notify/notify.h"
#include "component/Components.h"

namespace gameplay {

namespace {
const Element emptyElement;
}

size_t Level::getID(size_t x, size_t y) const {
    return x + y * width();
}

void Level::allocate() {
    allocation.clear();
    elements.clear();
    allocation.resize(height()*width());
    for (size_t id = 0; id < allocation.size(); id++) {
        allocation[id].id = static_cast<int>(id);
    }
    elements.resize(width());
    for (size_t x = 0; x < width(); x++) {
        elements[x].resize(height());
        for (size_t y = 0; y < height(); y++) {
            elements[x][y] = &allocation[getID(x, y)];
            elements[x][y]->id = static_cast<int>(getID(x, y));
        }
    }
    tilesCount = 0;
}

Level::Level() {
    allocate();
}

void Level::load(const levels::Level& level) {
    allocate();
    for (size_t x = 0; x < width(); x++) {
        for (size_t y = 0; y < height(); y++) {
            const size_t id{ getID(x, y) };
            allocation[id] = Element{level.get(x,y)};
            elements[x][y]->id = static_cast<int>(id);
            if (allocation[id].present) {
                tilesCount++;
                notifier.notify({
                    "element",
                    "new",
                    static_cast<int>(id),
                    static_cast<int>(x),
                    static_cast<int>(y),
                    allocation[id].color,
                    allocation[id].image
                });
            }
        }
    }
    time = level.time;
    baseTime = level.time;
    rules = level.rules;
    hard = level.hard;
    tutorial_step = nullptr;
}

size_t Level::width() const {
    return common::Rules::Width;
}

size_t Level::height() const {
    return common::Rules::Height;
}

const Element& Level::get(size_t x, size_t y) const {
    if (x >= width()) return emptyElement;
    if (y >= height()) return emptyElement;
	if (x >= elements.size()) {
		LOG_ERROR("Out of range while get element from level");
		return emptyElement;
	}
	auto& column{ elements[x] };
	if (y >= column.size()) {
		LOG_ERROR("Out of range while get element from level");
		return emptyElement;
	}
    Element* element = column[y];
    return element ? *element : emptyElement;
}

const Element& Level::get(size_t id) const {
	if (id >= allocation.size()) return emptyElement;
	return allocation[id];
}

size_t Level::tiles() const {
    return tilesCount;
}

bool Level::empty() const {
    return tilesCount == 0;
}

bool Level::canStep() const {
    std::map<Element, std::vector<Point>> buckets;
    for (size_t x = 0; x < width(); x++) {
        for (size_t y = 0; y < height(); y++) {
            if (get(x, y).canAction()) {
                buckets[get(x, y)].push_back(Point{ x, y });
            }
        }
    }
    for (const auto& bucket : buckets) {
        for (size_t start = 0; start < bucket.second.size(); start++) {
            for (size_t finish = 0; finish < bucket.second.size(); finish++) {
                if (canStep(
                    bucket.second[start].x,
                    bucket.second[start].y,
                    bucket.second[finish].x,
                    bucket.second[finish].y
                )) {
                    return true;
                }
            }
        }
    }
    return false;
}

Step Level::help() const {
    std::map<Element, std::vector<Point>> buckets;
    for (size_t x = 0; x < width(); x++) {
        for (size_t y = 0; y < height(); y++) {
            if (get(x, y).canAction()) {
                buckets[get(x, y)].push_back(Point{ x, y });
            }
        }
    }
    for (const auto& bucket : buckets) {
        for (size_t start = 0; start < bucket.second.size(); start++) {
            for (size_t finish = 0; finish < bucket.second.size(); finish++) {
                if (canStep(
                    bucket.second[start].x,
                    bucket.second[start].y,
                    bucket.second[finish].x,
                    bucket.second[finish].y
                )) {
                    return Step {
                        bucket.second[start].x,
                        bucket.second[start].y,
                        bucket.second[finish].x,
                        bucket.second[finish].y
                    };
                }
            }
        }
    }
    return Step{};
}


bool Level::findVertical(int x, int y, int xf, int yf, size_t turns, const Element& element, gameplay::LineComplete& line) const {
    if ((turns == 0) && (xf != x)) return false;
    for (int iy = y - 1; iy >= -1; iy--) {
        if ((xf == x) && (yf == iy) && ((element.present == false) || (get(x, iy) == element))) {
            line.push_back(x);
            line.push_back(iy);
            return true;
        }
        if (get(x, iy).present) {
            break;
        }
        if (turns > 0) {
            if (findHorizontal(x, iy, xf, yf, turns - 1, element, line)) {
                line.push_back(x);
                line.push_back(iy);
                return true;
            }
        }
    }
    for (int iy = y + 1; iy <= static_cast<int>(height()); iy++) {
        if ((xf == x) && (yf == iy) && ((element.present == false) || (get(x, iy) == element))) {
            line.push_back(x);
            line.push_back(iy);
            return true;
        }
        if (get(x, iy).present) {
            break;
        }
        if (turns > 0) {
            if (findHorizontal(x, iy, xf, yf, turns - 1, element, line)) {
                line.push_back(x);
                line.push_back(iy);
                return true;
            }
        }
    }
    return false;
}

bool Level::findHorizontal(int x, int y, int xf, int yf, size_t turns, const Element& element, gameplay::LineComplete& line) const {
    if ((turns == 0) && (yf != y)) return false;
    for (int ix = x - 1; ix >= -1; ix--) {
        if ((xf == ix) && (yf == y) && ((element.present == false) || (get(ix, y) == element))) {
            line.push_back(ix);
            line.push_back(y);
            return true;
        }
        if (get(ix, y).present) {
            break;
        }
        if (turns > 0) {
            if (findVertical(ix, y, xf, yf, turns - 1, element, line)) {
                line.push_back(ix);
                line.push_back(y);
                return true;
            }
        }
    }
    for (int ix = x + 1; ix <= static_cast<int>(width()); ix++) {
        if ((xf == ix) && (yf == y) && ((element.present == false) || (get(ix, y) == element))) {
            line.push_back(ix);
            line.push_back(y);
            return true;
        }
        if (get(ix, y).present) {
            break;
        }
        if (turns > 0) {
            if (findVertical(ix, y, xf, yf, turns - 1, element, line)) {
                line.push_back(ix);
                line.push_back(y);
                return true;
            }
        }
    }
    return false;
}

bool Level::canStep(size_t x1, size_t y1, size_t x2, size_t y2) const {
    LineComplete line;
    return canStep(x1, y1, x2, y2, line);
}

bool Level::canStep(size_t x1, size_t y1, size_t x2, size_t y2, LineComplete& line) const {
    auto element1{ get(x1, y1) };
    auto element2{ get(x2, y2) };
    if (element1.blocked) return false;
    if (!element1.present) return false;
    if (element2.blocked) return false;
    if (!element2.present) return false;
    if (element1.color != element2.color) return false;
    if (element1.image != element2.image) return false;
    LineComplete line1;
    LineComplete line2;
    const auto canStep1{ findHorizontal(static_cast<int>(x1), static_cast<int>(y1), static_cast<int>(x2), static_cast<int>(y2), 2, element1, line1) };
    const auto canStep2{ findVertical(static_cast<int>(x1), static_cast<int>(y1), static_cast<int>(x2), static_cast<int>(y2), 2, element1, line2) };
    if (!canStep1 && !canStep2) return false;
    if (canStep1&&canStep2) {
        line = line1.size()<line2.size()?line1:line2;
    } else
    if (canStep1) {
        line = line1;
    } else
    if (canStep2) {
        line = line2;
    }
    line.push_back(static_cast<int>(x1));
    line.push_back(static_cast<int>(y1));
    return true;
}

bool Level::havePath(size_t x1, size_t y1, size_t x2, size_t y2, LineComplete& line) const {
	LineComplete line1;
	LineComplete line2;
	const auto canStep1{ findHorizontal(static_cast<int>(x1), static_cast<int>(y1), static_cast<int>(x2), static_cast<int>(y2), 2, emptyElement, line1) };
	const auto canStep2{ findVertical(static_cast<int>(x1), static_cast<int>(y1), static_cast<int>(x2), static_cast<int>(y2), 2, emptyElement, line2) };
	if (!canStep1 && !canStep2) return false;
	if (canStep1 && canStep2) {
		line = line1.size() < line2.size() ? line1 : line2;
	}
	else
	if (canStep1) {
		line = line1;
	}
	else
	if (canStep2) {
		line = line2;
	}
    line.push_back(static_cast<int>(x1));
    line.push_back(static_cast<int>(y1));
    return true;
}

void Level::step(size_t x1, size_t y1, size_t x2, size_t y2) {
	if (canStep(x1, y1, x2, y2)) {
		elements[x1][y1]->present = false;
		elements[x2][y2]->present = false;
		tilesCount -= 2;
        notify::element::drop(elements[x1][y1]->id);
        notify::element::drop(elements[x2][y2]->id);
        if (tutorial_step) tutorial_step();
        apply_rules();
        if ((tiles() > 0) && !canStep()) mix();
    }
}

void Level::mix() {
    std::vector<size_t> ids;
    std::vector<Point> points;
    for (size_t x = 0; x < width(); x++) {
        for (size_t y = 0; y < height(); y++) {
            const Element&element{ get(x, y) };
            if (element.canAction()) {
                ids.push_back(element.id);
                points.push_back(Point{ x, y });
            }
        }
    }
    do {
        std::vector<size_t> ids_ = ids;
        std::vector<Point> points_ = points;
        while (!ids_.empty()) {
            auto id{ common::Random::get() % points_.size() };
            Point& point{ points_[id] };
            elements[point.x][point.y] = &allocation[ids_[ids_.size() - 1]];
            ids_.pop_back();
            common::utils::quickRemove(points_, id);
        }
    }
    while (!canStep());

    for (size_t x = 0; x < width(); x++) {
        for (size_t y = 0; y < height(); y++) {
            const Element& element{ get(x,y) };
            if (element.canAction()) {
                notifier.notify(Parameters{
                    "element",
                    "randommove",
                    static_cast<int>(element.id),
                    static_cast<int>(x),
                    static_cast<int>(y)
                    });
            }
        }
    }
}

void Level::lock(size_t x, size_t y) {
    auto element{ get(x,y) };
    if (!element.present) {
        LOG_ERROR("No point for lock %zu %zu", x, y);
        return;
    }
    elements[x][y]->blocked = true;
    notify::element::dark(elements[x][y]->id);
}

void Level::unlock(size_t x, size_t y) {
    auto element{ get(x,y) };
    if (!element.present) {
        LOG_ERROR("No point for unlock %zu %zu", x, y);
        return;
    }
    elements[x][y]->blocked = false;
    notify::element::light(elements[x][y]->id);
}

void Level::drop(size_t x, size_t y) {
    if (get(x, y).present) {
        elements[x][y]->present = false;
        tilesCount--;
    }
}

void Level::blast_element(size_t x, size_t y) {
    if (get(x, y).present) {
        elements[x][y]->present = false;
        tilesCount--;
        notify::tool::hit(static_cast<int>(x), static_cast<int>(y));
        notify::element::drop(get(x, y).id);
    }
}

void Level::element_move(size_t x1, size_t y1, size_t x2, size_t y2) {
    if (x1 >= width()) {
        LOG_ERROR("Try to move element from x = %zd", x1);
        return;
    }
    if (x2 >= width()) {
        LOG_ERROR("Try to move element to x = %zd", x2);
        return;
    }
    if (y1 >= height()) {
        LOG_ERROR("Try to move element from y = %zd", y1);
        return;
    }
    if (y2 >= height()) {
        LOG_ERROR("Try to move element to y = %zd", y2);
        return;
    }
    if (elements[x1][y1]->canAction() && !elements[x2][y2]->present) {
        std::swap(elements[x1][y1], elements[x2][y2]);
        notifier.notify(Parameters{
            "element", "move",
            static_cast<int>(elements[x2][y2]->id),
            static_cast<int>(x2),
            static_cast<int>(y2)
            });
    }
}

void Level::apply_rules(char r) {
    switch (r)
    {
    case '0':
        break;
    case '1':
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = height() - 1; y > 0; y--) {
                element_move(x, y - 1, x, y);
            }
        }
        break;
    case '2':
        for (size_t x = width() - 1; x > 0; x--) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x - 1, y, x, y);
            }
        }
        break;
    case '3':
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = 0; y < height() - 1; y++) {
                element_move(x, y + 1, x, y);
            }
        }
        break;
    case '4':
        for (size_t x = 0; x < width() - 1; x++) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x + 1, y, x, y);
            }
        }
        break;
    case '5':
        for (size_t x = 0; x < width() / 2; x++) {
            for (size_t y = height() - 1; y > 0; y--) {
                element_move(x, y - 1, x, y);
            }
        }
        for (size_t x = width() / 2; x < width(); x++) {
            for (size_t y = 0; y < height() - 1; y++) {
                element_move(x, y + 1, x, y);
            }
        }
        break;
    case '6':
        for (size_t x = width() - 1; x > 0; x--) {
            for (size_t y = height() / 2; y < height(); y++) {
                element_move(x - 1, y, x, y);
            }
        }
        for (size_t x = 0; x < width() - 1; x++) {
            for (size_t y = 0; y < height() / 2; y++) {
                element_move(x + 1, y, x, y);
            }
        }
        break;
    case 'b':
        for (size_t x = 0; x < width() / 2; x++) {
            for (size_t y = 0; y < height() - 1; y++) {
                element_move(x, y + 1, x, y);
            }
        }
        for (size_t x = width() / 2; x < width(); x++) {
            for (size_t y = height() - 1; y > 0; y--) {
                element_move(x, y - 1, x, y);
            }
        }
        break;
    case 'c':
        for (size_t x = 0; x < width() - 1; x++) {
            for (size_t y = height() / 2; y < height(); y++) {
                element_move(x + 1, y, x, y);
            }
        }
        for (size_t x = width() - 1; x > 0 ; x--) {
            for (size_t y = 0; y < height() / 2; y++) {
                element_move(x - 1, y, x, y);
            }
        }
        break;
    case 'd':
        for (size_t x = width() / 2 - 1; x > 0; x--) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x - 1, y, x, y);
            }
        }
        for (size_t x = width() / 2; x < width() - 1; x++) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x, y, x - 1, y);
            }
        }
        break;
    case 'e':
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = height() / 2 - 1; y > 0; y--) {
                element_move(x, y - 1, x, y);
            }
        }
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = height() / 2; y < height() - 1; y++) {
                element_move(x, y + 1, x, y);
            }
        }
        break;
    case 'f':
        for (size_t x = width() - 1; x > width() / 2; x--) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x - 1, y, x, y);
            }
        }
        for (size_t x = 0; x < width() / 2 - 1; x++) {
            for (size_t y = 0; y < height(); y++) {
                element_move(x + 1, y, x, y);
            }
        }
        break;
    case 'g':
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = height() - 1; y > height() / 2; y--) {
                element_move(x, y - 1, x, y);
            }
        }
        for (size_t x = 0; x < width(); x++) {
            for (size_t y = 0; y < height() / 2 - 1; y++) {
                element_move(x, y + 1, x, y);
            }
        }
        break;
    default:
        break;
    }
}

void Level::apply_rules() {
    switch (rules)
    {
    case '0':
        break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'g':
        apply_rules(rules);
        break;
    case '7':
        apply_rules('b');
        apply_rules('c');
        break;
    case '8':
        apply_rules('5');
        apply_rules('6');
        break;
    case '9':
        if (tiles() % 4) apply_rules('5'); else apply_rules('6');
        break;
    case 'a':
        if (tiles() % 4) apply_rules('b'); else apply_rules('c');
        break;
    case 'h':
        apply_rules('f');
        apply_rules('g');
        break;
    case 'i':
        apply_rules('d');
        apply_rules('e');
        break;
    default:
        break;
    }
}

}
