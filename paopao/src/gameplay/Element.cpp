#include "gameplay/Element.h"

namespace gameplay {

Element::Element(const levels::Element& element)
	: color(element.color)
	, image(element.image)
	, present(element.present)
{}

bool Element::canAction() const {
	if (!present) return false;
	if (blocked) return false;
	return true;
}

bool operator==(const Element& lhs, const Element& rhs) {
	return
		(lhs.image == rhs.image) &&
		(lhs.color == rhs.color) &&
		(lhs.blocked == rhs.blocked) &&
		(lhs.present == rhs.present);
}

bool operator<(const Element& lhs, const Element& rhs) {
	if (lhs.image < rhs.image) return true;
	if (lhs.image > rhs.image) return false;
	if (lhs.color < rhs.color) return true;
	if (lhs.color > rhs.color) return false;
	if (lhs.blocked < rhs.blocked) return true;
	if (lhs.blocked > rhs.blocked) return false;
	if (lhs.present < rhs.present) return true;
	if (lhs.present > rhs.present) return false;
	return false;
}

}
