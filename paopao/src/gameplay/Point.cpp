#include "gameplay/Point.h"

namespace gameplay {

bool operator==(const Point& rhs, const Point& lhs) {
	return (rhs.x == lhs.x) && (rhs.y == lhs.y);
}

}
