#include "component/Components.h"
#include "gameplay/Gameplay.h"
#include "common/Strings.h"
#include "common/Rules.h"
#include "game/State.h"
#include "notify/notify.h"
#include "advertising/Ads.h"
#include "component/Components.h"
#include "levels/Store.h"
#include "ui/UI.h"
#include "mediator/Logger.h"
#include "gameplay/Controller.h"
#include "bonus/Bonuses.h"
#include "gameplay/Tutorial.h"

namespace gameplay {

namespace {

static const std::string kTime{ "time" };
gameplay::Tutorial tutorial;

}

void Gameplay::play_level(const levels::Level& level) {
	notify::state::clear_arrays();
	prev_level = component::state->currentLevel;
	component::level->load(level);
	component::controller->start();
	component::ui->game_ui.show();
	rewardAdsShowed = false;
	unpause();
	if (
		(component::state->levelType != levels::LevelType::Regular) ||
		(component::state->currentLevel != 1) ||
		(!component::state->firstRun)
	) {
		component::ui->level_rules.show(static_cast<int>(prev_level));
	}
	else {
		component::ui->the_rules.show();
		tutorial.start();
		component::level->tutorial_step = []() {tutorial.step(); };
	}
}

void Gameplay::play_level(levels::LevelType levelType, size_t level) {
	if (!component::state->canPlayLevel(levelType, level)) {
		LOG_ERROR("Try to play nonaccesible level");
		return;
	}
	component::state->currentLevel = level;
	component::state->levelType = levelType;
	play_curr_level();
}

void Gameplay::play_curr_level() {
	play_level(component::store->getCurrentLevel());
}

void Gameplay::replay() {
	component::state->currentLevel = prev_level;
	play_curr_level();
}


void Gameplay::load(const nlohmann::json& data) {
	component::bonuses->load(data);
}

void Gameplay::save(nlohmann::json& data) {
	component::bonuses->save(data);
}

void Gameplay::pause() {
	component::controller->pause();
}

void Gameplay::unpause() {
	component::controller->unpause();
}

void Gameplay::tick(float dt) {
	component::controller->tick(dt);
	if (component::controller->getStatus() == GameplayStatus::NoGame) return;
	if (component::controller->getStatus() == GameplayStatus::Paused) return;
	component::level->time -= dt;
	component::ui->game_ui.updateTimer(component::level->time);
	if (component::level->empty()) {
		component::controller->setStatus(GameplayStatus::Paused);
		const auto next_exists{ component::state->switchToNextLevel() };
		component::ui->win_window.show(3, !next_exists);
		return;
	}
	if (component::level->time<=0) {
		component::controller->setStatus(GameplayStatus::Paused);
		component::ui->lose_window.show(!rewardAdsShowed && component::ads->canShowRewardedAds());
		return;
	}
}

void Gameplay::markRewardAdsShowed() {
	rewardAdsShowed = true;
}

void Gameplay::appendTime() {
	notify::tool::plus();
	component::level->time = common::Rules::AppendTime;
	component::controller->unpause();
}

void Gameplay::stop() {
	notify::state::clear_arrays();
	component::controller->stop();
}

}
