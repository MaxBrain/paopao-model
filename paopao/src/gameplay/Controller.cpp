#include "component/Components.h"
#include "gameplay/Controller.h"
#include "gameplay/Gameplay.h"
#include "mediator/Logger.h"
#include "3rdParty/json.hpp"
#include "common/Rules.h"
#include "notify/notify.h"
#include "bonus/Bonuses.h"

namespace gameplay {

void Controller::dropInputState() {
    if (selected_tile != NoTileSelected) {
		notify::element::deactivate(selected_tile);
    }
    selected_tile = NoTileSelected;
}

void Controller::showLine() {
	nlohmann::json json{ line };
	notify::path::show(json[0]);
	needHideLine = false;
}

void Controller::showLineComplete() {
	nlohmann::json json{ lineComplete };
	notify::path::show(json[0]);
}

void Controller::showLineForTime() {
	showLineComplete();
	needHideLine = true;
	needHideLineTimer = common::Rules::ShowLineTimer;
}


void Controller::block_input(float dt) {
    dropInputState();
    status = GameplayStatus::Animation;
    timer = dt;
}

void Controller::selectTile(const Element& element, size_t x, size_t y, bool quite) {
	if (!quite) {
		notify::element::activate(element.id);
	}
	selected_tile = element.id;
	selectedX = x;
	selectedY = y;
}

void Controller::deselectTile() {
	if (selected_tile != NoTileSelected) {
		notify::element::deactivate(selected_tile);
		selected_tile = NoTileSelected;
	}
}

void Controller::process3parameters(const std::string& cmd, float float_x, float float_y) {
    const int x{ static_cast<int>(round(float_x)) };
    const int y{ static_cast<int>(round(float_y)) };
	float_x -= 0.5;
	float_y -= 0.5;
    const Element& element{ component::level->get(x, y) };
    switch (status) {
    case GameplayStatus::WaitForPlayerAction:
        if (cmd == "click") {
			if (!element.canAction()) {
				deselectTile();
				return;
			}
			if (selected_tile == element.id) {
				deselectTile();
				return;
			}
			if (selected_tile == NoTileSelected) {
				selectTile(element, x, y);
				return;
			}
			if (component::level->canStep(selectedX, selectedY, x, y, lineComplete)) {
				deselectTile();
				component::level->step(selectedX, selectedY, x, y);
				showLineForTime();
				return;
			}
			deselectTile();
			selectTile(element, x, y);
        }
        else
        if (cmd == "drag") {
			if (!element.canAction()) {
				return;
			}
			if (selected_tile == element.id) {
				status = GameplayStatus::Dragged;
				return;
			}
			if (selected_tile != NoTileSelected) {
				deselectTile();
				selectTile(element, x, y);
				previousX = selectedX;
				previousY = selectedY;
				status = GameplayStatus::Dragged;
				return;
			}
			selectTile(element, x, y, true);
			previousX = selectedX;
			previousY = selectedY;
			status = GameplayStatus::Dragged;
        }
        break;
    case GameplayStatus::Animation:
        return;
    case GameplayStatus::Paused:
        return;
    case GameplayStatus::Dragged:
        if (cmd == "click") return;
        if (cmd == "drag") return;
        if (cmd == "move") {
			if (selected_tile == element.id) {
				return;
			}
            if ((x == previousX) && (y == previousY)) {
                if (line.size() >= 2) {
                    line[0] = float_x;
                    line[1] = float_y;
					showLine();
				}
				return;
            }
            previousX = x;
            previousY = y;
            if (component::level->havePath(selectedX, selectedY, x, y, lineComplete)) {
                line.clear();
                line.reserve(lineComplete.size());
                for (auto& e : lineComplete) {
                    line.push_back(static_cast<float>(e));
                }
                if (line.size() >= 2) {
                    line[0] = float_x;
                    line[1] = float_y;
                }
            }
			showLine();
        }
        if (cmd == "drop") {
			if (component::level->canStep(selectedX, selectedY, x, y, lineComplete)) {
				deselectTile();
				component::level->step(selectedX, selectedY, x, y);
				showLineForTime();
				status = GameplayStatus::WaitForPlayerAction;
				return;
			}
			status = GameplayStatus::WaitForPlayerAction;
			notify::path::hide();
        }
        break;
	case GameplayStatus::WaitBombPosition:
		if ((cmd == "click") || (cmd == "drag")) {
			component::bonuses->applyBomb(*component::level, x, y);
			status = GameplayStatus::WaitForPlayerAction;
		}
		break;
    case GameplayStatus::NoGame:
        return;
    }
}

void Controller::input(const Parameters & parameters) {
    if (parameters.size() == 3) {
        if (!parameters[0].is<std::string>()) return;
        if (!parameters[1].is<float>()) return;
        if (!parameters[2].is<float>()) return;
        process3parameters(
			parameters[0].get_unchecked<std::string>(),
			parameters[1].get_unchecked<float>(),
			parameters[2].get_unchecked<float>()
		);
    }
}

void Controller::start() {
	notify::controller::activate();
	status = GameplayStatus::WaitForPlayerAction;
}

void Controller::stop() {
	notify::controller::deactivate();
    status = GameplayStatus::NoGame;
}

gameplay::GameplayStatus Controller::getStatus() {
	return status;
}

void Controller::setStatus(gameplay::GameplayStatus status_) {
	status = status_;
}

void Controller::pause() {
	if (status == GameplayStatus::NoGame) return;
	if (status == GameplayStatus::Dragged) {
		selected_tile = NoTileSelected;
		notify::path::hide();
	}
	status = GameplayStatus::Paused;
}

void Controller::unpause() {
	if (status == GameplayStatus::NoGame) return;
	status = GameplayStatus::WaitForPlayerAction;
}

void Controller::tick(float dt) {
	if (needHideLine) {
		needHideLineTimer -= dt;
		if (needHideLineTimer <= 0) {
			notify::path::hide();
			needHideLine = false;
		}
	}
	if (status != GameplayStatus::Animation) return;
	timer -= dt;
}

void Controller::useBomb() {
	status = GameplayStatus::WaitBombPosition;
}

void Controller::cancelBomb() {
	status = GameplayStatus::WaitForPlayerAction;
}

bool Controller::isBomb() const {
	return status == GameplayStatus::WaitBombPosition;
}

}
