#include "component/Components.h"
#include "component/detail.h"

namespace component {

namespace {
gameplay::Gameplay gameplay_;
gameplay::Level level_;
gameplay::Controller controller_;
game::Game game_;
ui::UI ui_;
bonus::Bonuses bonuses_;
i18n::L10n l10n_;
game::State state_;
config::Config config_;
advertising::Ads ads_;
levels::Store store_;
adventure::Adventure adventure_;
shop::Shop shop_;
generator::Generator generator_;
dailybonus::DailyBonus daily_bonus_;
dailyquest::DailyQuest daily_quest_;
}

gameplay::Gameplay* gameplay{&gameplay_};
gameplay::Level* level{&level_};
gameplay::Controller* controller{&controller_};
game::Game* game{&game_};
ui::UI* ui{&ui_};
bonus::Bonuses* bonuses{&bonuses_};
i18n::L10n* l10n{&l10n_};
game::State* state{&state_};
config::Config* config{&config_};
advertising::Ads* ads{&ads_};
levels::Store* store{&store_};
adventure::Adventure* adventure{&adventure_};
shop::Shop* shop{&shop_};
generator::Generator* generator{&generator_};
dailybonus::DailyBonus* daily_bonus{&daily_bonus_};
dailyquest::DailyQuest* daily_quest{&daily_quest_};
}
