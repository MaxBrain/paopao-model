#pragma once
#include <string>

namespace experiment {

enum class ExperimentType {
    TasksSize,
    TasksFrequency,
    InterscenialADSTimer,
    Learn,
    Level1,
    Density,
    DensityDelta,
	AddTimeAfterStep,
};

std::string GetDescription(ExperimentType experimentType);

}
