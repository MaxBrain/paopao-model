#pragma once
#include "experiment/ExperimentType.h"

namespace experiment {

constexpr int TasksSizeShort = 1;
constexpr int TasksSizeMiddle = 3;
constexpr int TasksSizeLong = 5;
constexpr int TasksSizeMin = 1;
constexpr int TasksSizeMax = 500;


constexpr int TasksFrequency24h = 24 * 60 * 60;
constexpr int TasksFrequency12h = 12 * 60 * 60;
constexpr int TasksFrequency6h =  6 * 60 * 60;
constexpr int TasksFrequency3h = 3 * 60 * 60;
constexpr int TasksFrequencyMin = 15 * 60;
constexpr int TasksFrequencyMax =  7 * 24 * 60 * 60;

constexpr int InterscenialADSTimer5m = 300;
constexpr int InterscenialADSTimer10m = 600;
constexpr int InterscenialADSTimerMin = 60;
constexpr int InterscenialADSTimerMax = 60 * 60;

constexpr int LearnRules = 0;
constexpr int LearnShow = 1;
constexpr int LearnRulesShow = 2;
constexpr int LearnMin = 0;
constexpr int LearnMax = 2;

constexpr int Level1Simple = 0;
constexpr int Level1Middle = 1;
constexpr int Level1Min = 0;
constexpr int Level1Max = 0;

constexpr int DensityMini = 30;
constexpr int DensityMiddle = 60;
constexpr int DensityBig = 90;
constexpr int DensityMin = 10;
constexpr int DensityMax = 100;

constexpr int DensityDeltaMini = 20;
constexpr int DensityDeltaBig = 60;
constexpr int DensityDeltaMin = 0;
constexpr int DensityDeltaMax = 100;

constexpr int AddTimeAfterStepNone = 0;
constexpr int AddTimeAfterStepYes = 1000;
constexpr int AddTimeAfterStepMin = 0;
constexpr int AddTimeAfterStepMax = 5000;

class Experiment {
public:
    static void Set(ExperimentType experimentType, int value);
    static int Get(ExperimentType experimentType);
};

}
