#pragma once

#include "component/fwd.h"

namespace component {

extern gameplay::Gameplay* gameplay;
extern gameplay::Level* level;
extern gameplay::Controller* controller;
extern game::Game* game;
extern ui::UI* ui;
extern bonus::Bonuses* bonuses;
extern i18n::L10n* l10n;
extern game::State* state;
extern config::Config* config;
extern advertising::Ads* ads;
extern levels::Store* store;
extern adventure::Adventure* adventure;
extern shop::Shop* shop;
extern generator::Generator* generator;
extern dailybonus::DailyBonus* daily_bonus;
extern dailyquest::DailyQuest* daily_quest;


}
