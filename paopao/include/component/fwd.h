#pragma once

namespace gameplay {
class Gameplay;
class Level;
class Controller;
}

namespace game {
class Game;
class State;
}

namespace ui {
class UI;
}

namespace bonus {
class Bonuses;
}

namespace i18n {
	class L10n;
}

namespace config {
	class Config;
}

namespace advertising {
	class Ads;
}

namespace levels {
	class Store;
}

namespace adventure {
	class Adventure;
}

namespace shop {
	class Shop;
}

namespace generator {
	class Generator;
}

namespace dailybonus {
	class DailyBonus;
}

namespace dailyquest {
	class DailyQuest;
}
