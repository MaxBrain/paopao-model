#pragma once
#include "3rdParty/json_fwd.hpp"
#include "dailybonus/Plan.h"

namespace dailybonus {

class DailyBonus {
public:
	Plan plan;
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
	void tick();
	bool hilight();
};

}
