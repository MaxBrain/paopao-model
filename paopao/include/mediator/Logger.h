#pragma once

#ifdef UNITTESTS

int printf(char const* const _Format, ...);

#define LOG_ERROR printf
#define LOG_INFO printf

#else

#include <dmsdk/dlib/log.h>

#define LOG_ERROR dmLogError
#define LOG_INFO dmLogInfo

#endif
