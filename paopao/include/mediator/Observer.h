#pragma once

template <typename E>
class Observer {
public:
	virtual void process(const E&){}
};
