#pragma once

#include <vector>
#include <string>
#include "mapbox/variant.hpp"

using Parameter = mapbox::util::variant<int, float, std::string>;

using Parameters = std::vector<Parameter>;
using Command = const Parameters&;

struct ParameterComparator {
    bool operator()(const Parameter& lhs, const Parameter& rhs) const;
};

struct ParametersComparator {
    bool operator()(const Parameters& lhs, const Parameters& rhs) const;
};
