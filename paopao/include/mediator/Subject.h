#pragma once

#include <vector>
#include <algorithm>
#include "Observer.h"

template <typename E>
class Subject {
	std::vector<Observer<E>*> observers;
public:
	void addObserver(Observer<E>* observer) {
		observers.push_back(observer);
	}
	void removeObserver(Observer<E>* observer) {
		observers.erase(std::remove(observers.begin(), observers.end(), observer), observers.end());
	}
	void notify(const E& event) {
		for (auto observer : observers) {
			observer->process(event);
		}
	}
};
