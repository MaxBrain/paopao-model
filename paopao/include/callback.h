#pragma once

#include <dmsdk/sdk.h>
#include "mediator/ControllerTypes.h"

void SetLuaCallback(lua_State* L, int pos);
void CallbackUpdate();
void Initialize();
void Finalize();

void AddToQueue(const Parameters& parameters);
