#pragma once
#include <vector>
#include "dailyquest/Task.h"
#include "3rdParty/json_fwd.hpp"

namespace dailyquest {

class DailyQuest {
public:
	std::vector<Task> tasks;
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
    bool isActive();
};

}
