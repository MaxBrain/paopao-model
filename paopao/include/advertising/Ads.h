#pragma once

#include <functional>

namespace advertising {

class Ads {
	std::function<void()> complete;
	std::function<void()> cancel;
	bool rewardedAdsLoaded{ false };
	bool interscenialAdsLoaded{ false };
	bool enabled{ false };
public:
	bool isEnabled() const;
	void setEnabled(bool enabled_);
	void setRewardedAdsLoaded();
	void setInterscenialAdsLoaded();
	void showInterscenialAds();
	void showRewardedAds(std::function<void()> complete_, std::function<void()> cancel_);
	void call_rewarded_complete();
	void call_rewarded_cancel();
	bool canShowInterscenialAds() const;
	bool canShowRewardedAds() const;
};

}
