#pragma once

namespace ui {

class LevelRules {
    bool is_showed{ false };
public:
    void show(int level);
    void hide();
};

}
