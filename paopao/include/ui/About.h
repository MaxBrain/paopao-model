#pragma once

namespace ui {

class About {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
