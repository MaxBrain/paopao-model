#pragma once

namespace ui {

class CollectionComplete {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
