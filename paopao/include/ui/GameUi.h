#pragma once

namespace ui {

class GameUi {
	bool is_showed{ false };
	int previous_level_time{ 300 };
public:
	void show();
	void hide();
	void updateTimer(float time);
	void updateSound();
	void updateBomb();
	void updateHint();
	void updateMix();
};

}
