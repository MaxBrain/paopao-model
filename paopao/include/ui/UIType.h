#pragma once

#include <string>

static const std::string kAbout{ "about" };
static const std::string kAdsForHint{ "ads_for_hint" };
static const std::string kAdventure{ "adventure" };
static const std::string kCollectionComplete{ "collection_complete" };
static const std::string kCollections{ "collections" };
static const std::string kDailyBonus{ "daily_bonus" };
static const std::string kDailyTasks{ "daily_tasks" };
static const std::string kGameUi{ "game_ui" };
static const std::string kLoseWindow{ "lose_window" };
static const std::string kMenu{ "menu" };
static const std::string kNewElement{ "new_element" };
static const std::string kPauseWindow{ "pause_window" };
static const std::string kRateExternal{ "rate_external" };
static const std::string kRateInternal{ "rate_internal" };
static const std::string kSettings{ "settings" };
static const std::string kShop{ "shop" };
static const std::string kTermOfConditions{ "term_of_conditions" };
static const std::string kWinWindow{ "win_window" };

namespace ui {

enum class UIType {
	About,
	AdsForHint,
	Adventure,
	CollectionComplete,
	Collections,
	DailyBonus,
	DailyTasks,
	GameUi,
	LoseWindow,
	Menu,
	NewElement,
	PauseWindow,
	RateExternal,
	RateInternal,
	Settings,
	Shop,
	TermOfConditions,
	WinWindow,
	None,
};

UIType ConverToUIType(const std::string& str);

}
