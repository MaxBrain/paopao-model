#pragma once

namespace ui {

class AdsForHint {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
