#pragma once

namespace ui {

class Menu {
	bool active{false};
public:
	void show();
	void hide();
	void updateNoAdsButton();
	void updateShopButton();
};

}
