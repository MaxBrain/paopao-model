#pragma once
#include "bonus/BonusType.h"

namespace ui {

class SuggestBonus {
    bool is_showed{ false };
    bonus::BonusType bonusType;
public:
    void show(bonus::BonusType bonusType);
    void hide();
    bonus::BonusType getBonusType() const;
    void click_little_inapp();
    void click_big_inapp();
    void click_view_ads();
    void updateRewardedVideoState();
    static void rewarded_video_complete();
    static void rewarded_video_canceled();
};

}
