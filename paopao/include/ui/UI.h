#pragma once

#include <set>
#include <map>
#include "mediator/ControllerTypes.h"
#include "ui/UIType.h"
#include "ui/About.h"
#include "ui/AdsForHint.h"
#include "ui/Adventure.h"
#include "ui/CollectionComplete.h"
#include "ui/Collections.h"
#include "ui/DailyBonus.h"
#include "ui/DailyTasks.h"
#include "ui/GameUi.h"
#include "ui/LevelRules.h"
#include "ui/LoseWindow.h"
#include "ui/Menu.h"
#include "ui/NewElement.h"
#include "ui/PauseWindow.h"
#include "ui/RateExternal.h"
#include "ui/RateInternal.h"
#include "ui/Settings.h"
#include "ui/Shop.h"
#include "ui/SuggestBonus.h"
#include "ui/TermOfConditions.h"
#include "ui/TheRules.h"
#include "ui/WinWindow.h"

namespace game {
class Game;
}

namespace ui {

constexpr int kShopFromMenu{ 0 };
constexpr int kShopFromGameplay{ 1 };

static const std::set<UIType> one{ UIType::GameUi, UIType::Menu };

class UI {
	bool game_ui_showed{ false };
	bool main_menu_showed{ false };
	std::map<UIType, bool> showed;
public:
	ui::About about;
	ui::AdsForHint ads_for_hint;
	ui::Adventure adventure;
	ui::CollectionComplete collection_complete;
	ui::Collections collections;
	ui::DailyBonus daily_bonus;
	ui::DailyTasks daily_tasks;
	ui::GameUi game_ui;
	ui::LevelRules level_rules;
	ui::LoseWindow lose_window;
	ui::Menu menu;
	ui::NewElement new_element;
	ui::PauseWindow pause_window;
	ui::RateExternal rate_external;
	ui::RateInternal rate_internal;
	ui::Settings settings;
	ui::Shop shop;
	ui::SuggestBonus suggest_bonus;
	ui::TermOfConditions term_of_conditions;
	ui::TheRules the_rules;
	ui::WinWindow win_window;
};

}
