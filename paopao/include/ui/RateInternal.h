#pragma once

namespace ui {

class RateInternal {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
