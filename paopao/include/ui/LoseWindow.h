#pragma once

namespace ui {

class LoseWindow {
    bool is_showed{false};
public:
    void show(bool continue_button);
    void hide();
    void hide_continue();
};

}
