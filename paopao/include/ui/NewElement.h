#pragma once

namespace ui {

class NewElement {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
