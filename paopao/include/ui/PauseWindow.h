#pragma once

namespace ui {

class PauseWindow {
	bool is_showed{ false };
public:
	void show();
	void hide();
};

}
