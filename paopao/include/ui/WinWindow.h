#pragma once

namespace ui {

class WinWindow {
    bool is_showed{false};
public:
    void show(int stars, bool hide_next);
    void hide();
};

}
