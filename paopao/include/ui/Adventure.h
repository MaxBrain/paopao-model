#pragma once

namespace ui {

class Adventure {
    bool is_showed{false};
public:
    void show();
    void hide();
};

}
