#pragma once

namespace ui {

class Shop {
	bool is_showed{ false };
public:
	void show();
	void hide();
	void updateNoAdsButton();
};

}
