#pragma once

#include <string>

namespace ui {

class DailyBonus {
    bool unpause_on_close{ false };
public:
    void show(const std::string& title, const size_t bonus, bool unpause_on_close);
    void hide();
    static const size_t Bomb{ 0 };
    static const size_t Mix{ 1 };
    static const size_t Hint{ 2 };
    static const size_t NoAds{ 3 };
};

}
