#pragma once

namespace ui {

class Settings {
    bool is_showed{false};
public:
    void show();
    void hide();
    void update_lang();
    void update_sound();
    void update_music();
};

}
