#pragma once

#include "gameplay/Level.h"
#include "levels/LevelType.h"
#include "3rdParty/json_fwd.hpp"

namespace gameplay {

class Gameplay {
	int prevTime{ 300 };
	size_t prev_level{ 1 };
	bool rewardAdsShowed{false};
	void play_level(const levels::Level& level);
public:
	void play_curr_level();
	void replay();
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
	void pause();
	void unpause();
	void tick(float dt);
	void markRewardAdsShowed();
	void appendTime();
	void stop();
	void play_level(levels::LevelType levelType, size_t level);

};

}
