#pragma once
#include <functional>
#include "gameplay/Element.h"
#include "levels/Level.h"
#include "gameplay/Step.h"
#include "gameplay/LineComplete.h"
#include "mediator/ControllerTypes.h"

namespace gameplay {

class Level {
	std::vector<Element> allocation;
	std::vector<std::vector<Element*>> elements;
	size_t tilesCount{0};
	void allocate();
	bool findHorizontal(int x, int y, int xf, int yf, size_t turns, const Element& element, gameplay::LineComplete& line) const;
	bool findVertical(int x, int y, int xf, int yf, size_t turns, const Element& element, gameplay::LineComplete& line) const;
public:
	Level();
	void load(const levels::Level& level);
	size_t width() const;
	size_t height() const;
	const Element& get(size_t x, size_t y) const;
	const Element& get(size_t id) const;
	size_t tiles() const;
	bool empty() const;
	bool canStep() const;
	bool canStep(size_t x1, size_t y1, size_t x2, size_t y2) const;
	bool canStep(size_t x1, size_t y1, size_t x2, size_t y2, LineComplete& line) const;
	bool havePath(size_t x1, size_t y1, size_t x2, size_t y2, LineComplete& line) const;
	void step(size_t x1, size_t y1, size_t x2, size_t y2);
	void mix();
	Step help() const;
	void lock(size_t x, size_t y);
	void unlock(size_t x, size_t y);
	void drop(size_t x, size_t y);
	void blast_element(size_t x, size_t y);
	void apply_rules();
	void apply_rules(char r);
	size_t getID(size_t x, size_t y) const;
	float time{ 300 };
	float baseTime{ 300 };
	char rules{ '0' };
	bool hard{ false };
	void element_move(size_t x1, size_t y1, size_t x2, size_t y2);
	std::function<void()> tutorial_step;
};

}
