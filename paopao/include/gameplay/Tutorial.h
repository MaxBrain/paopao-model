#pragma once
#include <stddef.h>

namespace gameplay {

class Tutorial {
	size_t tiles;
public:
	void start();
	void step();
};

}
