#pragma once

namespace gameplay {

enum class GameplayStatus {
    WaitForPlayerAction,
    Animation,
    Paused,
    Dragged,
    WaitBombPosition,
	NoGame,
};

}
