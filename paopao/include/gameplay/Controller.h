#pragma once

#include "mediator/ControllerTypes.h"
#include "gameplay/GameplayStatus.h"
#include "gameplay/Level.h"
#include "gameplay/Line.h"
#include "gameplay/LineComplete.h"

namespace gameplay {

class Gameplay;

constexpr int NoTileSelected{ -1 };

class Controller {
	gameplay::GameplayStatus status{ gameplay::GameplayStatus::NoGame };
	int selected_tile{ NoTileSelected };
	size_t selectedX{ 0 };
	size_t selectedY{ 0 };
	size_t previousX{ 0 };
	size_t previousY{ 0 };
	float timer{ 0 };
	void dropInputState();
	LineComplete lineComplete;
	Line line;
	void selectTile(const Element& element, size_t x, size_t y, bool quite = false);
	void deselectTile();
	bool needHideLine{ false };
	float needHideLineTimer{ 0 };
	void showLineForTime();
public:
	void block_input(float dt);
	void process3parameters(const std::string& cmd, float float_x, float float_y);
	void showLine();
	void showLineComplete();
	void input(const Parameters& parameters);
	void start();
	void stop();
	gameplay::GameplayStatus getStatus();
	void setStatus(gameplay::GameplayStatus status_);
	void pause();
	void unpause();
	void tick(float dt);
	void useBomb();
	void cancelBomb();
	bool isBomb() const;
};

}
