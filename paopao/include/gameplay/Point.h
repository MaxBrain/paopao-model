#pragma once

#include <stddef.h>

namespace gameplay {

class Point {
public:
	size_t x{ 0 };
	size_t y{ 0 };
};

bool operator==(const Point& rhs, const Point& lhs);

}
