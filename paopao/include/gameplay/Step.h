#pragma once

namespace gameplay {

struct Step {
	size_t x1{ 0 };
	size_t y1{ 0 };
	size_t x2{ 0 };
	size_t y2{ 0 };
};

}
