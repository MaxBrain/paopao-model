#pragma once
#include "levels/Element.h"

namespace gameplay {

class Element {
public:
	Element() = default;
	Element(const levels::Element& element);
	int id{ 0 };
	int color{ 0 };
	int image{ 0 };
	bool blocked{ false };
	bool present{ false };
	bool canAction() const;
};

bool operator==(const Element& lhs, const Element& rhs);
bool operator<(const Element& lhs, const Element& rhs);

}
