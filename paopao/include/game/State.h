#pragma once
#include "3rdParty/json_fwd.hpp"
#include "levels/LevelType.h"
#include "game/StarStore.h"

namespace game {

static constexpr auto kState{ "state" };
static constexpr auto kFirstRun{ "firstRun" };

class State {
	std::string session;
public:
	State();
	bool firstRun{ true /*false*/ };
	levels::LevelType levelType{ levels::LevelType::Regular};
	size_t page{ 0 };
	size_t activeLevels[2]{ 1, 1 };
	size_t activeLevel() const;
	size_t currentLevel{ 1 };
	bool music{ true };
	bool sound{ true };
	std::string lang{ "ru" };
	bool system_language{ true };
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
	StarStore starStore;
	std::string getStars(levels::LevelType levelType_, size_t page_) const;
	bool haveNextLevel() const;
	bool switchToNextLevel();
	void switchLevelType();
	bool canPlayLevel(levels::LevelType levelType_, size_t level_) const;
	std::string getSession() const;
	bool game_ui_sound() const;
};

}
