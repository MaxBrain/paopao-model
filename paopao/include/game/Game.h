#pragma once
#include <string>

namespace game {

class Game {
	float timer{ 0 };
	size_t ticker{ 0 };
public:
	void load(std::string data);
	std::string save();
	void tick(float dt);
	size_t current_tick();
};

}
