#pragma once

#include <vector>
#include <string>
#include "levels/LevelType.h"

namespace game {

class StarStore {
public:
	std::vector<std::string> data{ "", "" };
	void setStar(levels::LevelType levelType, size_t level, size_t stars);
	size_t getStar(levels::LevelType levelType, size_t level) const;
	std::string getStars(levels::LevelType levelType, size_t page) const;
};

}
