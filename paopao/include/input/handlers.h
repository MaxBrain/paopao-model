#pragma once

#include "mediator/ControllerTypes.h"

namespace input {
namespace gui {
namespace about {
void close(const Parameters& parameters);
void music(const Parameters& parameters);
void OK(const Parameters& parameters);
}
namespace ads_for_hint {
void close(const Parameters& parameters);
void watch(const Parameters& parameters);
}
namespace adventure {
void close(const Parameters& parameters);
void active_challenge(const Parameters& parameters);
}
namespace collection_complete {
void close(const Parameters& parameters);
}
namespace collections {
void close(const Parameters& parameters);
}
namespace daily_bonus {
void close(const Parameters& parameters);
}
namespace daily_tasks {
void close(const Parameters& parameters);
void goto_1(const Parameters& parameters);
void goto_2(const Parameters& parameters);
void goto_3(const Parameters& parameters);
void goto_4(const Parameters& parameters);
void goto_5(const Parameters& parameters);
}
namespace game_ui {
void sound(const Parameters& parameters);
void hint(const Parameters& parameters);
void bomb(const Parameters& parameters);
void mix(const Parameters& parameters);
void pause(const Parameters& parameters);
}
namespace level_rules {
void OK(const Parameters& parameters);
}
namespace lose_window {
void to_menu(const Parameters& parameters);
void add_time(const Parameters& parameters);
void replay(const Parameters& parameters);
}
namespace menu {
void achievements(const Parameters& parameters);
void settings(const Parameters& parameters);
void challenge(const Parameters& parameters);
void no_ads(const Parameters& parameters);
void like(const Parameters& parameters);
void collections(const Parameters& parameters);
void tasks(const Parameters& parameters);
void prev(const Parameters& parameters);
void next(const Parameters& parameters);
void about(const Parameters& parameters);
void shop(const Parameters& parameters);
void level(const Parameters& parameters);
}
namespace new_element {
void close(const Parameters& parameters);
}
namespace pause_window {
void close(const Parameters& parameters);
void resume(const Parameters& parameters);
void replay(const Parameters& parameters);
void to_menu(const Parameters& parameters);
}
namespace rate_external {
void close(const Parameters& parameters);
void not_now(const Parameters& parameters);
void rate(const Parameters& parameters);
}
namespace rate_internal {
void close(const Parameters& parameters);
void bad(const Parameters& parameters);
void later(const Parameters& parameters);
void good(const Parameters& parameters);
}
namespace settings {
void close(const Parameters& parameters);
void switch_sound(const Parameters& parameters);
void switch_music(const Parameters& parameters);
void lang_1(const Parameters& parameters);
void lang_2(const Parameters& parameters);
void lang_3(const Parameters& parameters);
void lang_4(const Parameters& parameters);
void lang_5(const Parameters& parameters);
void lang_6(const Parameters& parameters);
void lang_7(const Parameters& parameters);
void lang_8(const Parameters& parameters);
void lang_9(const Parameters& parameters);
void lang_10(const Parameters& parameters);
void lang_11(const Parameters& parameters);
void lang_12(const Parameters& parameters);
}
namespace shop {
void close(const Parameters& parameters);
void buy_1(const Parameters& parameters);
void buy_2(const Parameters& parameters);
void buy_3(const Parameters& parameters);
void buy_4(const Parameters& parameters);
}
namespace suggest_bonus {
void close(const Parameters& parameters);
void ads(const Parameters& parameters);
void little_inapp(const Parameters& parameters);
void big_inapp(const Parameters& parameters);
}
namespace term_of_conditions {
void close(const Parameters& parameters);
void policy(const Parameters& parameters);
void agree(const Parameters& parameters);
}
namespace win_window {
void to_menu(const Parameters& parameters);
void next(const Parameters& parameters);
void replay(const Parameters& parameters);
}
namespace the_rules {
void OK(const Parameters& parameters);
}
}
void block_input(const Parameters& parameters);
void click(const Parameters& parameters);
void drag(const Parameters& parameters);
void move(const Parameters& parameters);
void drop(const Parameters& parameters);
namespace interscenial_ads {
void load(const Parameters& parameters);
}
namespace rewarded_ads {
void load(const Parameters& parameters);
void complete(const Parameters& parameters);
void cancel(const Parameters& parameters);
}
namespace inapp {
void init(const Parameters& parameters);
void bought(const Parameters& parameters);
}
}
