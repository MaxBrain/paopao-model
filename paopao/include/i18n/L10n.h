#pragma once
#include <map>
#include <vector>
#include <string>

namespace i18n {

class L10n {
	std::vector<std::string> langs;
	std::map<std::string, std::vector<std::string>> data;
	size_t lang{ 0 };
public:
	void load(const std::string& file);
	const std::string& get(const std::string& key) const;
	void translate(const std::string& lang);
	const std::vector<std::string>& langs_list() const;
};

}
