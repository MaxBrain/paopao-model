#pragma once
#include "3rdParty/dlib/array.h"
#include "value/Element.h"

namespace value {

class Record {
public:
	dmArray<Element> elements;
};

}
