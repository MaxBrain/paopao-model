#pragma once

namespace value {

enum class Type {
	Blast,
	Hint,
	Mix
};

const char* to_string(Type type);

}
