#pragma once
#include "value/Type.h"

namespace value {

class Element {
public:
	Type type;
	int count{ 0 };
};

}
