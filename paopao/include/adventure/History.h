#pragma once
#include <vector>
#include "adventure/HistoryRecord.h"

namespace adventure {

class History {
public:
	std::vector<HistoryRecord> records;
};

}
