#pragma once
#include <vector>
#include "3rdParty/json_fwd.hpp"
#include "adventure/History.h"

namespace adventure {

class Adventure {
public:
	History history;
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
    bool isActive();
};

}
