#pragma once
#include <string>
#include <map>
#include <vector>
#include "player/PlayRecord.h"
#include "mediator/ControllerTypes.h"

namespace player {

class Player {
public:
	size_t current_record{ 0 };
	std::map<size_t, std::vector<Parameters>> trackOutput;
	std::vector<PlayRecord> records;
	std::vector<Parameters> output;
	std::vector<PlayRecord> parseTrack(const std::string& track);
	void checkOutput();
	void fillOutput();
	void applyMessage(const PlayRecord& playRecord);
	void applyMessages(size_t tick);
	void playFile(const std::string& filename);
};

}
