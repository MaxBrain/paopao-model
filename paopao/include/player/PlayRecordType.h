#pragma once
#include <string>

namespace player {

enum class PlayRecordType {
    i18n,
    config,
    load,
    input,
    output,
    unknown
};

PlayRecordType toPlayRecordType(const std::string& string);

}
