#pragma once

#include "player/PlayRecordType.h"
#include "mediator/ControllerTypes.h"

namespace player {

constexpr char delimeter{';'};

struct PlayRecord {
    PlayRecordType type;
    size_t tick;
    Parameters parameters;
};

PlayRecord parsePlayRecord(std::string string);

}
