#pragma once
#include <string>
#include "3rdParty/json_fwd.hpp"

namespace shop {

class Record {
public:
	Record();
	Record(const nlohmann::json& data);
	std::string ident;
	std::string title;
	std::string description;
	std::string currency_code;
	std::string price_string;
	bool parsed{ false };
};

}
