#pragma once
#include <string>
#include <map>
#include "3rdParty/json_fwd.hpp"
#include "shop/ItemType.h"
#include "shop/Record.h"

namespace shop {

constexpr int BOMB_LITTLE_COUNT{ 5 };
constexpr int BOMB_BIG_COUNT{ 15 };
constexpr int BOMB_PACK_COUNT{ 50 };
constexpr int HINT_LITTLE_COUNT{ 5 };
constexpr int HINT_BIG_COUNT{ 15 };
constexpr int HINT_PACK_COUNT{ 50 };
constexpr int MIX_LITTLE_COUNT{ 5 };
constexpr int MIX_BIG_COUNT{ 15 };
constexpr int MIX_PACK_COUNT{ 50 };

class Shop {
	bool enabled{ false };
	bool initialized{ false };
	bool no_ads_bought{ false };
public:
	std::map<std::string, Record> goods;
	bool isEnabled() const;
	void setEnabled(bool enabled);
	bool isInitialized() const;
	void init(const std::string& elements);
	bool isActive() const;
	void setBoughtElements(const std::string& data);
	void buy(ItemType itemType);
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
	bool noAdsBought() const;
	std::string getCost(ItemType itemType) const;
	std::string getTitle(ItemType itemType) const;
	void buyComplete(const std::string& item);
	bool buttonNoAdsShow() const;
};

}
