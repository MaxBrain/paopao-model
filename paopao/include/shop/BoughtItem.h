#pragma once
#include <string>
#include "3rdParty/json_fwd.hpp"

namespace shop {

class BoughtItem {
public:
	BoughtItem(const nlohmann::json& data);
	std::string productID;
	std::string purchaseToken;
	std::string developerPayload;
	std::string signature;
	bool parsed{ false };
};

}
