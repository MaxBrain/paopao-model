#pragma once
#include <string>

namespace shop {

enum class ItemType {
	NO_ADS,
	BOMB_LITTLE,
	BOMB_BIG,
	BOMB_PACK,
	HINT_LITTLE,
	HINT_BIG,
	HINT_PACK,
	MIX_LITTLE,
	MIX_BIG,
	MIX_PACK,
	NO_TYPE,
};

ItemType parse(const std::string& str);
std::string to_string(ItemType itemType);

}
