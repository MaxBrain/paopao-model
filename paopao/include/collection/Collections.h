#pragma once
#include <vector>
#include "3rdParty/json_fwd.hpp"
#include "collection/Collection.h"

namespace collection {

class Collections {
	bool active{ false };
public:
	std::vector<Collection> collections;
	void addElement();
	void showWindow();
	bool isActive();
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
};

}
