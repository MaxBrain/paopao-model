#pragma once
#include <vector>
#include "collection/Element.h"

namespace collection {

class Collection {
public:
	std::vector<Element> elements;
	void addElement();
	bool complete();
};

}
