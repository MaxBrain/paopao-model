#pragma once

namespace levels {

class Element {
public:
	int color{ 0 };
	int image{ 0 };
	bool present{ false };
};

bool operator==(const levels::Element& e1, const levels::Element& e2);

}
