#pragma once

namespace levels {

enum class LevelType {
	Regular = 0,
	Adventure = 1,
};

}
