#pragma once

#include "levels/LevelType.h"
#include <functional>

namespace levels {

class PageKey {
public:
	LevelType levelType{ LevelType::Regular };
	size_t page{ 0 };
};

bool operator < (const PageKey& rhs, const PageKey& lhs);
bool operator == (const PageKey& rhs, const PageKey& lhs);

}

namespace std {
template <> struct hash<levels::PageKey> {
    size_t operator()(const levels::PageKey& x) const {
        return ((static_cast<size_t>(x.levelType)) << 28) + x.page;
    }
};
}
