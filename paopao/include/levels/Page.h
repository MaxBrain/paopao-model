#pragma once

#include "mapbox/optional.hpp"

namespace levels {

class Page {
public:
	std::string color;
	std::string rules;
	std::string hard;
};

class Pages {
public:
	int background{ 0 };
	mapbox::util::optional<Page> left;
	mapbox::util::optional<Page> active;
	mapbox::util::optional<Page> right;
};

}
