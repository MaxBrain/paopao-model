#pragma once

#include <map>
#include <string>
#include "levels/LevelType.h"
#include "levels/Level.h"
#include "levels/Page.h"

namespace levels {

class Store {
public:
	Level getLevel(LevelType levelType, size_t number) const;
	Pages getPages(LevelType levelType, size_t page) const;
	Level getCurrentLevel() const;
};

}
