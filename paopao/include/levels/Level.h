#pragma once

#include <vector>
#include <string>
#include "levels/Element.h"
#include "generator/Generator.h"

namespace levels {

class Level {
	std::vector<std::vector<Element>> area;
	static const Element emptyElement;
public:
	Level();
	Level(const std::vector<std::string>& data);
	Level(const generator::Block& block);
	const Element& get(size_t x, size_t y) const;
	void set(size_t x, size_t y, const Element& element);
	size_t width() const;
	size_t height() const;
	float time{ 300 };
	char rules{ '0' };
	bool hard{ false };
};

bool operator==(const Level& lhs, const Level& rhs);

}
