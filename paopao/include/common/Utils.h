#pragma once
#include <vector>
#include <string>

namespace common {
namespace utils {

template<typename T>
void quickRemove(std::vector<T>& vector, size_t id) {
	if (id>=vector.size()) return;
	std::swap(vector[id], vector[vector.size()-1]);
	vector.pop_back();
}

std::string get_file_contents(const std::string& filename);
std::vector<std::string> split(const std::string& str, char delim);

}

}
