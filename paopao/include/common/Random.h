#pragma once

namespace common {

class Random {
public:
	static void init(unsigned v1, unsigned v2 = 11111, unsigned v3 = 22222, unsigned v4 = 33333);
	static unsigned get();
};

}
