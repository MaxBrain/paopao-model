#pragma once

#include <string>

static const std::string kController{"controller"};
static const std::string kActivate{"activate"};
static const std::string kDeactivate{"deactivate"};
static const std::string kState{"state"};
static const std::string kNewlevel{"newlevel"};
static const std::string kWindow{"window"};
static const std::string kWin{"win"};
static const std::string kElement{"element"};
static const std::string kNew{"new"};
static const std::string kMove{"move"};
static const std::string kRandommove{"randommove"};
static const std::string kDrop{"drop"};
static const std::string kPutweb{"putweb"};
static const std::string kRemoveweb{"removeweb"};
static const std::string kPutice{"putice"};
static const std::string kRemoveice{"removeice"};
static const std::string kHint{"hint"};
static const std::string kPath{"path"};
static const std::string kShow{"show"};
static const std::string kHide{"hide"};
static const std::string kAddition{"addition"};
static const std::string kTool{"tool"};
static const std::string kBomb{"bomb"};
static const std::string kDisable{"disable"};
static const std::string kHit{"hit"};
static const std::string kMix{"mix"};
static const std::string kPlus{"plus"};
static const std::string kAchievements{"achievements"};
static const std::string kIncrement{"increment"};
static const std::string kReveal{"reveal"};
static const std::string kSetSteps{"setSteps"};
static const std::string kUnlock{"unlock"};
static const std::string kGui{"gui"};
static const std::string kPopup{"popup"};
static const std::string kClose{"close"};
static const std::string kUpdate{"update"};


static const std::string kGameUI{ "game_ui" };
