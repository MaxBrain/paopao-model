#pragma once

#include <unordered_map>
#include <functional>

namespace common {

template<class Key, class Value>
class Cache {
	using Getter = std::function<Value(const Key&)>;
	std::unordered_map<Key, Value> cache;
	Getter get_function;
public:
	Cache(Getter getter)
		: get_function(getter)
	{}
	Value get(const Key& key) {
		auto it = cache.find(key);
		if (it == cache.end()) {
			cache[key] = get_function(key);
		}
		return cache[key];
	}
};

}
