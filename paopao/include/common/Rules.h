#pragma once

#include <stddef.h>

namespace common {

namespace Rules {
	static constexpr size_t Width{ 8 };
	static constexpr size_t Height{ 12 };
	static constexpr size_t LevelsInAdventure{ 500 };
	static constexpr size_t PageWidth{ 5 };
	static constexpr size_t PageHeight{ 4 };
	static constexpr size_t PageSize{ PageWidth * PageHeight };
	static constexpr size_t PagesInAdventure{ 500 / PageSize };
	static constexpr float ShowLineTimer{ 0.5f };
    namespace bonuses_base {
        static const size_t bomb{3};
        static const size_t hint{3};
        static const size_t mix{3};
    }
	static constexpr float AppendTime{ 30.0f };
	static const size_t bombForAds{ 1 };
	static const size_t hintForAds{ 1 };
	static const size_t mixForAds{ 1 };
}

}
