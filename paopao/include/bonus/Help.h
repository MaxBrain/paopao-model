#pragma once
#include "gameplay/Level.h"

namespace bonus {

class Help {
public:
	static void apply(gameplay::Level& level);
};

}
