#pragma once

namespace bonus {

enum class BonusType {
	Bomb,
	Hint,
	Mix,
};

}
