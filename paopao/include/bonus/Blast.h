#pragma once
#include "gameplay/Level.h"

namespace bonus {

class Blast {
public:
	static void apply(gameplay::Level& level, size_t x, size_t y);
};

}
