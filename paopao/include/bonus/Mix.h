#pragma once
#include "gameplay/Level.h"

namespace bonus {

class Mix {
public:
	static void apply(gameplay::Level& level);
};

}
