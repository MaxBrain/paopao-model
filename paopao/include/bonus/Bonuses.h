#pragma once
#include "3rdParty/json_fwd.hpp"

namespace gameplay {
class Level;
}

namespace bonus {

class Bonuses {
public:
	Bonuses();
	void load(const nlohmann::json& data);
	void save(nlohmann::json& data);
	size_t bomb;
	size_t hint;
	size_t mix;
	void applyBomb(gameplay::Level& level, size_t x, size_t y);
	void applyHint(gameplay::Level& level);
	void applyMix(gameplay::Level& level);
};

}
