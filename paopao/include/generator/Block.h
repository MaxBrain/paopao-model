#pragma once

#include <vector>
#include <functional>

namespace generator {

enum class Dir {
	forward = 1,
	backward = -1
};

using Area = std::vector<std::vector<unsigned>>;

struct BlockSize {
	unsigned w;
	unsigned h;
};

struct Point {
	unsigned x;
	unsigned y;
};

class Block
{
	Area area;
public:
	static void drop_rnd();
	static std::function<unsigned()> rnd;
	Block(unsigned w, unsigned h);
	Block(const BlockSize& blockSize);
	void fill(unsigned p_i);
	void put(const Block& block, unsigned x, unsigned y, Dir horizontal, Dir vertical);
	unsigned w() const;
	unsigned h() const;
	const Area& getArea() const;
	void resize(unsigned w, unsigned h);
	void clear();
	void putColor(const Point& point, unsigned color);
	bool fail() const;
	unsigned points() const;
};

}
