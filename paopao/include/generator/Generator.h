#pragma once

#include <vector>
#include <string>
#include "common/Cache.h"
#include "generator/Block.h"
#include "levels/PageKey.h"

namespace generator {

using Direction = std::vector<Dir>;
using Directions = std::vector<Direction>;

Direction getRandomDirections(unsigned size);

static const std::string AVAILABLE_RULES_5{ "01234" };
static const std::string AVAILABLE_RULES_7{ "0123456" };
static const std::string AVAILABLE_RULES_9{ "012345678" };
static const std::string AVAILABLE_RULES{ "0123456789a" };

constexpr size_t COUNT_OF_0_ON_PAGE{ 5 };


struct BlockRecord {
	unsigned id;
	unsigned x;
	unsigned y;
	Dir vertical;
	Dir horizontal;
};

struct VerticalForm {
	unsigned style = 0;
	unsigned p0 = 0, p1 = 0, p2 = 0, p3 = 0;
	std::vector<unsigned> IDs;
	std::vector<unsigned> hs;
	unsigned blockTypes;
	void generate();
	void generate0();
	void generate1();
	void generate2();
	bool empty() const;
	Directions directions;
	unsigned nBlocks() const;
	void generateDirection(unsigned columns);
	void generateBlockIDs();
	void appendIDs(unsigned add);
	unsigned getX0(unsigned block);
	std::vector<unsigned> getX();
};

struct HorizotalForm {
	unsigned style;
	unsigned p0 = 0, p1 = 0, p2 = 0;
	void generate();
	bool needAdditionalVertical() const;
	bool empty() const;
	Directions directions;
	unsigned nBlocks() const;
	void generateDirection(unsigned rows);
	unsigned ws();
	unsigned getY0(unsigned block);
	std::vector<unsigned> getY();
};

class Generator
{
	VerticalForm verticalForm;
	HorizotalForm horizonalForm;
	VerticalForm additionalVerticalForm;

	std::vector<BlockSize> blockSizes;
	std::vector<BlockRecord> blockRecords;
	mutable common::Cache<levels::PageKey, std::string> rules_cache;
	std::string generateRules(const levels::PageKey& key) const;
	size_t getDefinedRandom(const levels::PageKey& key) const;
	void clear_5(std::string& levels, const levels::PageKey& key, size_t n = COUNT_OF_0_ON_PAGE) const;
public:
	Block result;
	void makeVerticalForm();
	void makeHorisontalForm();
	void makeAdditionalVerticalForm();
	void generateVerticalDirection();
	void generateHorizontalDirection();
	void generateAdditionalVerticalDirection();
	void generateBlocks();
	void fillBlocks();
	void fillTiles(float colors);

	Generator();
	void generate(float colors);
	const Block& getResult() const;

	char getRulesForLevel(levels::LevelType levelType, size_t level);
	std::string getRules(const levels::PageKey& key) const;
};

}
