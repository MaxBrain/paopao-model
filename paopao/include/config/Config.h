#pragma once

#include <string>

#include "config/Achievements.h"
#include "config/Ads.h"
#include "config/Rewarded.h"
#include "config/Shop.h"
#include "config/Statictics.h"

namespace config {

class Config {
public:
	Achievements achievements{ Achievements::None };
	Ads ads{ Ads::None };
	Rewarded rewarded{ Rewarded::None };
	Shop shop{ Shop::None };;
	Statictics statictics{ Statictics::None };;
	void load(const std::string& config);
};

}
