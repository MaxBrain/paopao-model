#pragma once

#include <string>

namespace config {

enum class Statictics {
	None,
	Google,
	Yandex,
};

Statictics parseStatictics(const std::string& string);

}
