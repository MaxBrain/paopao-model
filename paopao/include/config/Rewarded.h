#pragma once

#include <string>

namespace config {

enum class Rewarded {
	None,
	AdMob,
	Yandex,
};

Rewarded parseRewarded(const std::string& string);

}
