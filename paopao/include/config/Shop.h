#pragma once

#include <string>

namespace config {

enum class Shop {
	None,
	Base,
	Yandex,
};

Shop parseShop(const std::string& string);

}
