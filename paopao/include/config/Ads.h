#pragma once

#include <string>

namespace config {

enum class Ads {
	None,
	AdMob,
	Yandex,
};

Ads parseAds(const std::string& string);

}
