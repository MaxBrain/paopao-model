#pragma once

#include <string>

namespace config {

enum class Achievements {
	None,
	GP,
	IOS,
};

Achievements parseAchievements(const std::string& string);

}
