#pragma once

#include <string>
#include "3rdParty/json_fwd.hpp"
#include "mediator/Subject.h"
#include "mediator/ControllerTypes.h"

extern Subject<Parameters> notifier;

namespace notify {
namespace controller {

void activate();
void activate(int value);
void activate(int value1, int value2);
void activate(float value);
void activate(const std::string& value);
void activate(const nlohmann::json& value);

void deactivate();
void deactivate(int value);
void deactivate(int value1, int value2);
void deactivate(float value);
void deactivate(const std::string& value);
void deactivate(const nlohmann::json& value);
}
namespace state {

void newlevel();
void newlevel(int value);
void newlevel(int value1, int value2);
void newlevel(float value);
void newlevel(const std::string& value);
void newlevel(const nlohmann::json& value);

void clear_arrays();
void clear_arrays(int value);
void clear_arrays(int value1, int value2);
void clear_arrays(float value);
void clear_arrays(const std::string& value);
void clear_arrays(const nlohmann::json& value);
}
namespace window {

void win();
void win(int value);
void win(int value1, int value2);
void win(float value);
void win(const std::string& value);
void win(const nlohmann::json& value);
}
namespace element {

void new_();
void new_(int value);
void new_(int value1, int value2);
void new_(float value);
void new_(const std::string& value);
void new_(const nlohmann::json& value);

void activate();
void activate(int value);
void activate(int value1, int value2);
void activate(float value);
void activate(const std::string& value);
void activate(const nlohmann::json& value);

void deactivate();
void deactivate(int value);
void deactivate(int value1, int value2);
void deactivate(float value);
void deactivate(const std::string& value);
void deactivate(const nlohmann::json& value);

void move();
void move(int value);
void move(int value1, int value2);
void move(float value);
void move(const std::string& value);
void move(const nlohmann::json& value);

void randommove();
void randommove(int value);
void randommove(int value1, int value2);
void randommove(float value);
void randommove(const std::string& value);
void randommove(const nlohmann::json& value);

void drop();
void drop(int value);
void drop(int value1, int value2);
void drop(float value);
void drop(const std::string& value);
void drop(const nlohmann::json& value);

void putweb();
void putweb(int value);
void putweb(int value1, int value2);
void putweb(float value);
void putweb(const std::string& value);
void putweb(const nlohmann::json& value);

void removeweb();
void removeweb(int value);
void removeweb(int value1, int value2);
void removeweb(float value);
void removeweb(const std::string& value);
void removeweb(const nlohmann::json& value);

void putice();
void putice(int value);
void putice(int value1, int value2);
void putice(float value);
void putice(const std::string& value);
void putice(const nlohmann::json& value);

void removeice();
void removeice(int value);
void removeice(int value1, int value2);
void removeice(float value);
void removeice(const std::string& value);
void removeice(const nlohmann::json& value);

void hint();
void hint(int value);
void hint(int value1, int value2);
void hint(float value);
void hint(const std::string& value);
void hint(const nlohmann::json& value);

void dark();
void dark(int value);
void dark(int value1, int value2);
void dark(float value);
void dark(const std::string& value);
void dark(const nlohmann::json& value);

void light();
void light(int value);
void light(int value1, int value2);
void light(float value);
void light(const std::string& value);
void light(const nlohmann::json& value);
}
namespace path {

void show();
void show(int value);
void show(int value1, int value2);
void show(float value);
void show(const std::string& value);
void show(const nlohmann::json& value);

void hide();
void hide(int value);
void hide(int value1, int value2);
void hide(float value);
void hide(const std::string& value);
void hide(const nlohmann::json& value);
}
namespace addition {

void new_();
void new_(int value);
void new_(int value1, int value2);
void new_(float value);
void new_(const std::string& value);
void new_(const nlohmann::json& value);
}
namespace tool {

void bomb();
void bomb(int value);
void bomb(int value1, int value2);
void bomb(float value);
void bomb(const std::string& value);
void bomb(const nlohmann::json& value);

void disable();
void disable(int value);
void disable(int value1, int value2);
void disable(float value);
void disable(const std::string& value);
void disable(const nlohmann::json& value);

void hint();
void hint(int value);
void hint(int value1, int value2);
void hint(float value);
void hint(const std::string& value);
void hint(const nlohmann::json& value);

void hit();
void hit(int value);
void hit(int value1, int value2);
void hit(float value);
void hit(const std::string& value);
void hit(const nlohmann::json& value);

void mix();
void mix(int value);
void mix(int value1, int value2);
void mix(float value);
void mix(const std::string& value);
void mix(const nlohmann::json& value);

void plus();
void plus(int value);
void plus(int value1, int value2);
void plus(float value);
void plus(const std::string& value);
void plus(const nlohmann::json& value);
}
namespace gui {
namespace popup {

void about();
void about(int value);
void about(int value1, int value2);
void about(float value);
void about(const std::string& value);
void about(const nlohmann::json& value);

void ads_for_hint();
void ads_for_hint(int value);
void ads_for_hint(int value1, int value2);
void ads_for_hint(float value);
void ads_for_hint(const std::string& value);
void ads_for_hint(const nlohmann::json& value);

void adventure();
void adventure(int value);
void adventure(int value1, int value2);
void adventure(float value);
void adventure(const std::string& value);
void adventure(const nlohmann::json& value);

void collection_complete();
void collection_complete(int value);
void collection_complete(int value1, int value2);
void collection_complete(float value);
void collection_complete(const std::string& value);
void collection_complete(const nlohmann::json& value);

void collections();
void collections(int value);
void collections(int value1, int value2);
void collections(float value);
void collections(const std::string& value);
void collections(const nlohmann::json& value);

void daily_bonus();
void daily_bonus(int value);
void daily_bonus(int value1, int value2);
void daily_bonus(float value);
void daily_bonus(const std::string& value);
void daily_bonus(const nlohmann::json& value);

void daily_tasks();
void daily_tasks(int value);
void daily_tasks(int value1, int value2);
void daily_tasks(float value);
void daily_tasks(const std::string& value);
void daily_tasks(const nlohmann::json& value);

void game_ui();
void game_ui(int value);
void game_ui(int value1, int value2);
void game_ui(float value);
void game_ui(const std::string& value);
void game_ui(const nlohmann::json& value);

void lose_window();
void lose_window(int value);
void lose_window(int value1, int value2);
void lose_window(float value);
void lose_window(const std::string& value);
void lose_window(const nlohmann::json& value);

void menu();
void menu(int value);
void menu(int value1, int value2);
void menu(float value);
void menu(const std::string& value);
void menu(const nlohmann::json& value);

void new_element();
void new_element(int value);
void new_element(int value1, int value2);
void new_element(float value);
void new_element(const std::string& value);
void new_element(const nlohmann::json& value);

void pause_window();
void pause_window(int value);
void pause_window(int value1, int value2);
void pause_window(float value);
void pause_window(const std::string& value);
void pause_window(const nlohmann::json& value);

void rate_external();
void rate_external(int value);
void rate_external(int value1, int value2);
void rate_external(float value);
void rate_external(const std::string& value);
void rate_external(const nlohmann::json& value);

void rate_internal();
void rate_internal(int value);
void rate_internal(int value1, int value2);
void rate_internal(float value);
void rate_internal(const std::string& value);
void rate_internal(const nlohmann::json& value);

void settings();
void settings(int value);
void settings(int value1, int value2);
void settings(float value);
void settings(const std::string& value);
void settings(const nlohmann::json& value);

void shop();
void shop(int value);
void shop(int value1, int value2);
void shop(float value);
void shop(const std::string& value);
void shop(const nlohmann::json& value);

void term_of_conditions();
void term_of_conditions(int value);
void term_of_conditions(int value1, int value2);
void term_of_conditions(float value);
void term_of_conditions(const std::string& value);
void term_of_conditions(const nlohmann::json& value);

void win_window();
void win_window(int value);
void win_window(int value1, int value2);
void win_window(float value);
void win_window(const std::string& value);
void win_window(const nlohmann::json& value);

void level_rules();
void level_rules(int value);
void level_rules(int value1, int value2);
void level_rules(float value);
void level_rules(const std::string& value);
void level_rules(const nlohmann::json& value);

void suggest_bonus();
void suggest_bonus(int value);
void suggest_bonus(int value1, int value2);
void suggest_bonus(float value);
void suggest_bonus(const std::string& value);
void suggest_bonus(const nlohmann::json& value);

void the_rules();
void the_rules(int value);
void the_rules(int value1, int value2);
void the_rules(float value);
void the_rules(const std::string& value);
void the_rules(const nlohmann::json& value);
}
namespace close {

void about();
void about(int value);
void about(int value1, int value2);
void about(float value);
void about(const std::string& value);
void about(const nlohmann::json& value);

void ads_for_hint();
void ads_for_hint(int value);
void ads_for_hint(int value1, int value2);
void ads_for_hint(float value);
void ads_for_hint(const std::string& value);
void ads_for_hint(const nlohmann::json& value);

void adventure();
void adventure(int value);
void adventure(int value1, int value2);
void adventure(float value);
void adventure(const std::string& value);
void adventure(const nlohmann::json& value);

void collection_complete();
void collection_complete(int value);
void collection_complete(int value1, int value2);
void collection_complete(float value);
void collection_complete(const std::string& value);
void collection_complete(const nlohmann::json& value);

void collections();
void collections(int value);
void collections(int value1, int value2);
void collections(float value);
void collections(const std::string& value);
void collections(const nlohmann::json& value);

void daily_bonus();
void daily_bonus(int value);
void daily_bonus(int value1, int value2);
void daily_bonus(float value);
void daily_bonus(const std::string& value);
void daily_bonus(const nlohmann::json& value);

void daily_tasks();
void daily_tasks(int value);
void daily_tasks(int value1, int value2);
void daily_tasks(float value);
void daily_tasks(const std::string& value);
void daily_tasks(const nlohmann::json& value);

void game_ui();
void game_ui(int value);
void game_ui(int value1, int value2);
void game_ui(float value);
void game_ui(const std::string& value);
void game_ui(const nlohmann::json& value);

void lose_window();
void lose_window(int value);
void lose_window(int value1, int value2);
void lose_window(float value);
void lose_window(const std::string& value);
void lose_window(const nlohmann::json& value);

void menu();
void menu(int value);
void menu(int value1, int value2);
void menu(float value);
void menu(const std::string& value);
void menu(const nlohmann::json& value);

void new_element();
void new_element(int value);
void new_element(int value1, int value2);
void new_element(float value);
void new_element(const std::string& value);
void new_element(const nlohmann::json& value);

void pause_window();
void pause_window(int value);
void pause_window(int value1, int value2);
void pause_window(float value);
void pause_window(const std::string& value);
void pause_window(const nlohmann::json& value);

void rate_external();
void rate_external(int value);
void rate_external(int value1, int value2);
void rate_external(float value);
void rate_external(const std::string& value);
void rate_external(const nlohmann::json& value);

void rate_internal();
void rate_internal(int value);
void rate_internal(int value1, int value2);
void rate_internal(float value);
void rate_internal(const std::string& value);
void rate_internal(const nlohmann::json& value);

void settings();
void settings(int value);
void settings(int value1, int value2);
void settings(float value);
void settings(const std::string& value);
void settings(const nlohmann::json& value);

void shop();
void shop(int value);
void shop(int value1, int value2);
void shop(float value);
void shop(const std::string& value);
void shop(const nlohmann::json& value);

void term_of_conditions();
void term_of_conditions(int value);
void term_of_conditions(int value1, int value2);
void term_of_conditions(float value);
void term_of_conditions(const std::string& value);
void term_of_conditions(const nlohmann::json& value);

void win_window();
void win_window(int value);
void win_window(int value1, int value2);
void win_window(float value);
void win_window(const std::string& value);
void win_window(const nlohmann::json& value);

void level_rules();
void level_rules(int value);
void level_rules(int value1, int value2);
void level_rules(float value);
void level_rules(const std::string& value);
void level_rules(const nlohmann::json& value);

void suggest_bonus();
void suggest_bonus(int value);
void suggest_bonus(int value1, int value2);
void suggest_bonus(float value);
void suggest_bonus(const std::string& value);
void suggest_bonus(const nlohmann::json& value);

void the_rules();
void the_rules(int value);
void the_rules(int value1, int value2);
void the_rules(float value);
void the_rules(const std::string& value);
void the_rules(const nlohmann::json& value);
}
namespace show {

void about();
void about(int value);
void about(int value1, int value2);
void about(float value);
void about(const std::string& value);
void about(const nlohmann::json& value);

void ads_for_hint();
void ads_for_hint(int value);
void ads_for_hint(int value1, int value2);
void ads_for_hint(float value);
void ads_for_hint(const std::string& value);
void ads_for_hint(const nlohmann::json& value);

void adventure();
void adventure(int value);
void adventure(int value1, int value2);
void adventure(float value);
void adventure(const std::string& value);
void adventure(const nlohmann::json& value);

void collection_complete();
void collection_complete(int value);
void collection_complete(int value1, int value2);
void collection_complete(float value);
void collection_complete(const std::string& value);
void collection_complete(const nlohmann::json& value);

void collections();
void collections(int value);
void collections(int value1, int value2);
void collections(float value);
void collections(const std::string& value);
void collections(const nlohmann::json& value);

void daily_bonus();
void daily_bonus(int value);
void daily_bonus(int value1, int value2);
void daily_bonus(float value);
void daily_bonus(const std::string& value);
void daily_bonus(const nlohmann::json& value);

void daily_tasks();
void daily_tasks(int value);
void daily_tasks(int value1, int value2);
void daily_tasks(float value);
void daily_tasks(const std::string& value);
void daily_tasks(const nlohmann::json& value);

void game_ui();
void game_ui(int value);
void game_ui(int value1, int value2);
void game_ui(float value);
void game_ui(const std::string& value);
void game_ui(const nlohmann::json& value);

void lose_window();
void lose_window(int value);
void lose_window(int value1, int value2);
void lose_window(float value);
void lose_window(const std::string& value);
void lose_window(const nlohmann::json& value);

void menu();
void menu(int value);
void menu(int value1, int value2);
void menu(float value);
void menu(const std::string& value);
void menu(const nlohmann::json& value);

void new_element();
void new_element(int value);
void new_element(int value1, int value2);
void new_element(float value);
void new_element(const std::string& value);
void new_element(const nlohmann::json& value);

void pause_window();
void pause_window(int value);
void pause_window(int value1, int value2);
void pause_window(float value);
void pause_window(const std::string& value);
void pause_window(const nlohmann::json& value);

void rate_external();
void rate_external(int value);
void rate_external(int value1, int value2);
void rate_external(float value);
void rate_external(const std::string& value);
void rate_external(const nlohmann::json& value);

void rate_internal();
void rate_internal(int value);
void rate_internal(int value1, int value2);
void rate_internal(float value);
void rate_internal(const std::string& value);
void rate_internal(const nlohmann::json& value);

void settings();
void settings(int value);
void settings(int value1, int value2);
void settings(float value);
void settings(const std::string& value);
void settings(const nlohmann::json& value);

void shop();
void shop(int value);
void shop(int value1, int value2);
void shop(float value);
void shop(const std::string& value);
void shop(const nlohmann::json& value);

void term_of_conditions();
void term_of_conditions(int value);
void term_of_conditions(int value1, int value2);
void term_of_conditions(float value);
void term_of_conditions(const std::string& value);
void term_of_conditions(const nlohmann::json& value);

void win_window();
void win_window(int value);
void win_window(int value1, int value2);
void win_window(float value);
void win_window(const std::string& value);
void win_window(const nlohmann::json& value);

void level_rules();
void level_rules(int value);
void level_rules(int value1, int value2);
void level_rules(float value);
void level_rules(const std::string& value);
void level_rules(const nlohmann::json& value);

void suggest_bonus();
void suggest_bonus(int value);
void suggest_bonus(int value1, int value2);
void suggest_bonus(float value);
void suggest_bonus(const std::string& value);
void suggest_bonus(const nlohmann::json& value);

void the_rules();
void the_rules(int value);
void the_rules(int value1, int value2);
void the_rules(float value);
void the_rules(const std::string& value);
void the_rules(const nlohmann::json& value);
}
namespace hide {

void about();
void about(int value);
void about(int value1, int value2);
void about(float value);
void about(const std::string& value);
void about(const nlohmann::json& value);

void ads_for_hint();
void ads_for_hint(int value);
void ads_for_hint(int value1, int value2);
void ads_for_hint(float value);
void ads_for_hint(const std::string& value);
void ads_for_hint(const nlohmann::json& value);

void adventure();
void adventure(int value);
void adventure(int value1, int value2);
void adventure(float value);
void adventure(const std::string& value);
void adventure(const nlohmann::json& value);

void collection_complete();
void collection_complete(int value);
void collection_complete(int value1, int value2);
void collection_complete(float value);
void collection_complete(const std::string& value);
void collection_complete(const nlohmann::json& value);

void collections();
void collections(int value);
void collections(int value1, int value2);
void collections(float value);
void collections(const std::string& value);
void collections(const nlohmann::json& value);

void daily_bonus();
void daily_bonus(int value);
void daily_bonus(int value1, int value2);
void daily_bonus(float value);
void daily_bonus(const std::string& value);
void daily_bonus(const nlohmann::json& value);

void daily_tasks();
void daily_tasks(int value);
void daily_tasks(int value1, int value2);
void daily_tasks(float value);
void daily_tasks(const std::string& value);
void daily_tasks(const nlohmann::json& value);

void game_ui();
void game_ui(int value);
void game_ui(int value1, int value2);
void game_ui(float value);
void game_ui(const std::string& value);
void game_ui(const nlohmann::json& value);

void lose_window();
void lose_window(int value);
void lose_window(int value1, int value2);
void lose_window(float value);
void lose_window(const std::string& value);
void lose_window(const nlohmann::json& value);

void menu();
void menu(int value);
void menu(int value1, int value2);
void menu(float value);
void menu(const std::string& value);
void menu(const nlohmann::json& value);

void new_element();
void new_element(int value);
void new_element(int value1, int value2);
void new_element(float value);
void new_element(const std::string& value);
void new_element(const nlohmann::json& value);

void pause_window();
void pause_window(int value);
void pause_window(int value1, int value2);
void pause_window(float value);
void pause_window(const std::string& value);
void pause_window(const nlohmann::json& value);

void rate_external();
void rate_external(int value);
void rate_external(int value1, int value2);
void rate_external(float value);
void rate_external(const std::string& value);
void rate_external(const nlohmann::json& value);

void rate_internal();
void rate_internal(int value);
void rate_internal(int value1, int value2);
void rate_internal(float value);
void rate_internal(const std::string& value);
void rate_internal(const nlohmann::json& value);

void settings();
void settings(int value);
void settings(int value1, int value2);
void settings(float value);
void settings(const std::string& value);
void settings(const nlohmann::json& value);

void shop();
void shop(int value);
void shop(int value1, int value2);
void shop(float value);
void shop(const std::string& value);
void shop(const nlohmann::json& value);

void term_of_conditions();
void term_of_conditions(int value);
void term_of_conditions(int value1, int value2);
void term_of_conditions(float value);
void term_of_conditions(const std::string& value);
void term_of_conditions(const nlohmann::json& value);

void win_window();
void win_window(int value);
void win_window(int value1, int value2);
void win_window(float value);
void win_window(const std::string& value);
void win_window(const nlohmann::json& value);

void level_rules();
void level_rules(int value);
void level_rules(int value1, int value2);
void level_rules(float value);
void level_rules(const std::string& value);
void level_rules(const nlohmann::json& value);

void suggest_bonus();
void suggest_bonus(int value);
void suggest_bonus(int value1, int value2);
void suggest_bonus(float value);
void suggest_bonus(const std::string& value);
void suggest_bonus(const nlohmann::json& value);

void the_rules();
void the_rules(int value);
void the_rules(int value1, int value2);
void the_rules(float value);
void the_rules(const std::string& value);
void the_rules(const nlohmann::json& value);
}
namespace update {

void about();
void about(int value);
void about(int value1, int value2);
void about(float value);
void about(const std::string& value);
void about(const nlohmann::json& value);

void ads_for_hint();
void ads_for_hint(int value);
void ads_for_hint(int value1, int value2);
void ads_for_hint(float value);
void ads_for_hint(const std::string& value);
void ads_for_hint(const nlohmann::json& value);

void adventure();
void adventure(int value);
void adventure(int value1, int value2);
void adventure(float value);
void adventure(const std::string& value);
void adventure(const nlohmann::json& value);

void collection_complete();
void collection_complete(int value);
void collection_complete(int value1, int value2);
void collection_complete(float value);
void collection_complete(const std::string& value);
void collection_complete(const nlohmann::json& value);

void collections();
void collections(int value);
void collections(int value1, int value2);
void collections(float value);
void collections(const std::string& value);
void collections(const nlohmann::json& value);

void daily_bonus();
void daily_bonus(int value);
void daily_bonus(int value1, int value2);
void daily_bonus(float value);
void daily_bonus(const std::string& value);
void daily_bonus(const nlohmann::json& value);

void daily_tasks();
void daily_tasks(int value);
void daily_tasks(int value1, int value2);
void daily_tasks(float value);
void daily_tasks(const std::string& value);
void daily_tasks(const nlohmann::json& value);

void game_ui();
void game_ui(int value);
void game_ui(int value1, int value2);
void game_ui(float value);
void game_ui(const std::string& value);
void game_ui(const nlohmann::json& value);

void lose_window();
void lose_window(int value);
void lose_window(int value1, int value2);
void lose_window(float value);
void lose_window(const std::string& value);
void lose_window(const nlohmann::json& value);

void menu();
void menu(int value);
void menu(int value1, int value2);
void menu(float value);
void menu(const std::string& value);
void menu(const nlohmann::json& value);

void new_element();
void new_element(int value);
void new_element(int value1, int value2);
void new_element(float value);
void new_element(const std::string& value);
void new_element(const nlohmann::json& value);

void pause_window();
void pause_window(int value);
void pause_window(int value1, int value2);
void pause_window(float value);
void pause_window(const std::string& value);
void pause_window(const nlohmann::json& value);

void rate_external();
void rate_external(int value);
void rate_external(int value1, int value2);
void rate_external(float value);
void rate_external(const std::string& value);
void rate_external(const nlohmann::json& value);

void rate_internal();
void rate_internal(int value);
void rate_internal(int value1, int value2);
void rate_internal(float value);
void rate_internal(const std::string& value);
void rate_internal(const nlohmann::json& value);

void settings();
void settings(int value);
void settings(int value1, int value2);
void settings(float value);
void settings(const std::string& value);
void settings(const nlohmann::json& value);

void shop();
void shop(int value);
void shop(int value1, int value2);
void shop(float value);
void shop(const std::string& value);
void shop(const nlohmann::json& value);

void term_of_conditions();
void term_of_conditions(int value);
void term_of_conditions(int value1, int value2);
void term_of_conditions(float value);
void term_of_conditions(const std::string& value);
void term_of_conditions(const nlohmann::json& value);

void win_window();
void win_window(int value);
void win_window(int value1, int value2);
void win_window(float value);
void win_window(const std::string& value);
void win_window(const nlohmann::json& value);

void level_rules();
void level_rules(int value);
void level_rules(int value1, int value2);
void level_rules(float value);
void level_rules(const std::string& value);
void level_rules(const nlohmann::json& value);

void suggest_bonus();
void suggest_bonus(int value);
void suggest_bonus(int value1, int value2);
void suggest_bonus(float value);
void suggest_bonus(const std::string& value);
void suggest_bonus(const nlohmann::json& value);

void the_rules();
void the_rules(int value);
void the_rules(int value1, int value2);
void the_rules(float value);
void the_rules(const std::string& value);
void the_rules(const nlohmann::json& value);
}
}
namespace achievements {

void show();
void show(int value);
void show(int value1, int value2);
void show(float value);
void show(const std::string& value);
void show(const nlohmann::json& value);

void increment();
void increment(int value);
void increment(int value1, int value2);
void increment(float value);
void increment(const std::string& value);
void increment(const nlohmann::json& value);

void reveal();
void reveal(int value);
void reveal(int value1, int value2);
void reveal(float value);
void reveal(const std::string& value);
void reveal(const nlohmann::json& value);

void setSteps();
void setSteps(int value);
void setSteps(int value1, int value2);
void setSteps(float value);
void setSteps(const std::string& value);
void setSteps(const nlohmann::json& value);

void unlock();
void unlock(int value);
void unlock(int value1, int value2);
void unlock(float value);
void unlock(const std::string& value);
void unlock(const nlohmann::json& value);
}
namespace sound {

void sound_on();
void sound_on(int value);
void sound_on(int value1, int value2);
void sound_on(float value);
void sound_on(const std::string& value);
void sound_on(const nlohmann::json& value);

void sound_off();
void sound_off(int value);
void sound_off(int value1, int value2);
void sound_off(float value);
void sound_off(const std::string& value);
void sound_off(const nlohmann::json& value);

void music_on();
void music_on(int value);
void music_on(int value1, int value2);
void music_on(float value);
void music_on(const std::string& value);
void music_on(const nlohmann::json& value);

void music_off();
void music_off(int value);
void music_off(int value1, int value2);
void music_off(float value);
void music_off(const std::string& value);
void music_off(const nlohmann::json& value);
}
namespace ads {

void interscenial();
void interscenial(int value);
void interscenial(int value1, int value2);
void interscenial(float value);
void interscenial(const std::string& value);
void interscenial(const nlohmann::json& value);

void rewarded();
void rewarded(int value);
void rewarded(int value1, int value2);
void rewarded(float value);
void rewarded(const std::string& value);
void rewarded(const nlohmann::json& value);
}
namespace inapp {

void buy();
void buy(int value);
void buy(int value1, int value2);
void buy(float value);
void buy(const std::string& value);
void buy(const nlohmann::json& value);
}
namespace sys {

void navigate();
void navigate(int value);
void navigate(int value1, int value2);
void navigate(float value);
void navigate(const std::string& value);
void navigate(const nlohmann::json& value);
}
}
