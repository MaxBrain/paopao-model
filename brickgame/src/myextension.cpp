// BrickGameModel.cpp
// Extension lib defines
#define LIB_NAME "BrickGameModel"
#define MODULE_NAME "brickgamemodel"

#define DLIB_LOG_DOMAIN LIB_NAME

// include the Defold SDK
#include <dmsdk/sdk.h>
#include <string>

static int Rot13(lua_State* L)
{
    int top = lua_gettop(L);

    // Check and get parameter string from stack
    const char* str = luaL_checkstring(L, 1);

    // Allocate new string
    size_t len = strlen(str);
    char *rot = (char *) malloc(len + 1);

    // Iterate over the parameter string and create rot13 string
    for(int i = 0; i <= len; i++) {
        const char c = str[i];
        if((c >= 'A' && c <= 'M') || (c >= 'a' && c <= 'm')) {
            // Between A-M just add 13 to the char.
            rot[i] = c + 13;
        } else if((c >= 'N' && c <= 'Z') || (c >= 'n' && c <= 'z')) {
            // If rolling past 'Z' which happens below 'M', wrap back (subtract 13)
            rot[i] = c - 13;
        } else {
            // Leave character intact
            rot[i] = c;
        }
    }

    // Put the rotated string on the stack
    lua_pushstring(L, rot);

    // Free string memory. Lua has a copy by now.
    free(rot);

    // Assert that there is one item on the stack.
    assert(top + 1 == lua_gettop(L));

    // Return 1 item
    return 1;
}

static int PlayLevel(lua_State* L)
{
    lua_getfield(L, LUA_GLOBALSINDEX, "handler");                     /* function to be called */
    lua_pushstring(L, "how");                                                  /* 1st argument */
    lua_getfield(L, LUA_GLOBALSINDEX, "t");                             /* table to be indexed */
    lua_getfield(L, -1, "x");                                  /* push result of t.x (2nd arg) */
    lua_remove(L, -2);                                            /* remove 't' from the stack */
    lua_pushinteger(L, 14);                                                    /* 3rd argument */
    lua_call(L, 3, 1);                               /* call 'f' with 3 arguments and 1 result */
    lua_setfield(L, LUA_GLOBALSINDEX, "a");                                  /* set global 'a' */
    
    return 0;
}

class Row {
public:
    std::string name;
    std::string date;
    std::string ip;
    std::string custom;
};

static int GetStruct(lua_State* L)
{
    Row row;
    row.name = "Name";
    row.date = "10/11/1999";
    row.ip = "127.0.0.1";
    row.custom = "Some text";
    
    lua_createtable(L, 0, 4);

    lua_pushstring(L, row.name.c_str());
    lua_setfield(L, -2, "name");  /* 3rd element from the stack top */

    lua_pushstring(L, row.date.c_str());
    lua_setfield(L, -2, "date");

    lua_pushstring(L, row.ip.c_str());
    lua_setfield(L, -2, "ip");

    lua_pushstring(L, row.custom.c_str());
    lua_setfield(L, -2, "custom");

    return 1;
}


// Functions exposed to Lua
static const luaL_reg Module_methods[] =
{
    {"rot13", Rot13},
    {"playLevel", PlayLevel},
    {"getStruct", GetStruct},
    {0, 0}
};

static void LuaInit(lua_State* L)
{
    int top = lua_gettop(L);

    // Register lua names
    luaL_register(L, MODULE_NAME, Module_methods);

    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}

dmExtension::Result AppInitializeBrickGameModel(dmExtension::AppParams* params)
{
    return dmExtension::RESULT_OK;
}

dmExtension::Result InitializeBrickGameModel(dmExtension::Params* params)
{
    // Init Lua
    LuaInit(params->m_L);
    printf("Registered %s Extension\n", MODULE_NAME);
    return dmExtension::RESULT_OK;
}

dmExtension::Result AppFinalizeBrickGameModel(dmExtension::AppParams* params)
{
    return dmExtension::RESULT_OK;
}

dmExtension::Result FinalizeBrickGameModel(dmExtension::Params* params)
{
    return dmExtension::RESULT_OK;
}


// Defold SDK uses a macro for setting up extension entry points:
//
// DM_DECLARE_EXTENSION(symbol, name, app_init, app_final, init, update, on_event, final)

DM_DECLARE_EXTENSION(BrickGameModel, LIB_NAME, AppInitializeBrickGameModel, AppFinalizeBrickGameModel, InitializeBrickGameModel, 0, 0, FinalizeBrickGameModel)

#undef LIB_NAME
#undef DLIB_LOG_DOMAIN LIB_NAME
#undef DLIB_LOG_DOMAIN
